from fabric.api import * 
def dev():
	env.virtualenv = "workon adcake.com"	
	env.project_directory = "/home/ctaloc/project/adcake.com"
	env.code_directory = "/home/ctaloc/project/adcake.com/adcake"
def prod():
	env.virtualenv = "workon adcake.com"	
	env.hosts = ["adcake.com"]
	env.user = "django"
	env.project_directory = "/home/django/project/adcake.com"
	env.code_directory = "/home/django/project/adcake.com/adcake"
def update_server(): 
	with cd(env.project_directory):
		run('git pull')
def update_packages():
	with prefix(env.virtualenv):
		with cd(env.project_directory):
			run('pip install -r requirements.txt')

def commit(message="..."):
	with cd(env.project_directory):
		local('git commit -a -m "%s"' % message)
		local('git push')
		
