from django.conf.urls.defaults import *

urlpatterns = patterns('',
    (r'^submit', 'rabidratings.views.record_vote'),
    (r'^test', 'rabidratings.views.testview')
    )
