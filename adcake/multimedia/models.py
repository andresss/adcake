from datetime import datetime, date
import os
from countries.countries.models import Country
from helpers.utils import check_clean_cache, clean_cache
import videotools.forms as videoforms
from django.utils.translation import ugettext as _
from videotools.utils import get_length, generate_thumb,store_video_in_s3,delete_video_s3, generate_thumb_web
from videotools.models import VideoField
from django.conf import settings
from django.db import models
from django.contrib.auth.models import User
from django.db.models import FileField
from videotools.signals import convert_video
from tagging.fields import TagField
from sorl.thumbnail.fields import ImageWithThumbnailsField
from videotools.utils import generate_thumb
import commands
from os.path import basename,exists,dirname
from os import makedirs,rename,unlink,chdir,listdir
from django.template.defaultfilters import slugify
from utils import removeDir,get_size
import oembed
oembed.autodiscover()

VIDEO_TYPE = (
    ('O', 'oembed'),
    ('F', 'file'),
)

class AdCategory(models.Model):
    name = models.CharField(max_length=50,null=False,blank=False)
    slug = models.SlugField()
    def __unicode__(self):
		return self.name
    class Meta:
        verbose_name_plural = 'Advertising Categories'

class Media(models.Model):
    title = models.CharField(max_length=50)
    owner = models.ForeignKey(User, related_name='files')
    author_set = models.ManyToManyField(User, through='AuthorRole')
    added = models.DateTimeField(default=datetime.now())
    creation = models.DateField('Creation Date', blank=True, null=True,
                                help_text='Format: YYYY-MM-DD')
    comment = models.TextField(blank=True, null=True)
    view_count = models.PositiveIntegerField(default=0)

    def __unicode__(self):
        return self.title
    class Meta:
        ordering = ['-view_count']

def upload_to_i(attributeu,attributeg):
    def upload_callback(instance, filename):
        u=getattr(instance, attributeu)
        g=getattr(instance, attributeg)
        gal=g.all()[0]
        return 'uploads/users/%s/galleries/%s/%s' % (u.id,gal.id,filename)
    return upload_callback

class Image(Media):
    image = ImageWithThumbnailsField(upload_to='uploads/tmp/images/',
                                     thumbnail={'size': (96, 96),},
                                     extra_thumbnails={
        'searchcrop':  {'size': (80,80), 'options': ['upscale'],},
        'small':  {'size': (64,64), 'options': ['upscale'],},
        'medium': {'size': (96,96), 'options': ['upscale'],},
        'large':  {'size': (128,128), 'options': ['upscale'],},
        'wide': {'size': (172,96),'options':['crop'],},
        'wider': {'size': (280,180),'options':['crop'],},
        'frame': {'size': (444,258),'options':['crop'],},
        'showcase': {'size': settings.IMAGE_SHOWCASE_SIZE, },
        'final': {'size': (2000,1500),'options': ['upscale',],},
        'gallerysize': {'size': (550,550),'options': ['upscale',],},
        })
    slug = models.SlugField(help_text='Unique URL-friendly title for the\
    article.', unique=False)
#    gallery = models.ForeignKey(Galleries)
    caption = models.CharField(max_length=150, blank=True, null=True,
                               help_text="A short text to represent the image")
    tags = TagField(help_text="Tags separated by commas, eg: professional photography, illustration")

    def save(self,gal_id=0):
        if self.id:
            super(Image,self).save()
        else:
            super(Image,self).save()
            tmp_path = self.image.path
            tmp_filename = basename(tmp_path)
            tmp_filename_ext = tmp_filename.split('.')[1]
            rel_final_path = 'uploads/users/%s/galleries/%s/%s.%s' % (self.owner.id,gal_id,self.id,tmp_filename_ext)
            final_path = settings.MEDIA_ROOT + rel_final_path
            folder = settings.MEDIA_ROOT + "uploads/users/%s/galleries/%s/" % (self.owner.id,gal_id)
            try:
                makedirs(folder,mode=0760)
            except OSError:
                pass
            if exists(final_path):
                unlink(final_path)
            rename(tmp_path,final_path)
            self.image = rel_final_path
            try:
                clean_cache("new_media_list")
            except:
                pass
            super(Image,self).save()
            

    def public_galleries(self):
        """
        Return the public galleries to which this image belongs.
        """
        return self.galleries.filter(publish=True)
    def main_gallery(self):
        return self.galleries.all()[0]

    def main_category(self):
        try:
            return self.galleries.all()[0].adcategory
        except:
            return ''

    def firstornone (q):
        try:
            return q[0]
        except IndexError:
            return None

    def get_previous_in_gallery(self, gallery):
        """
        Get previous by publication date filtering a given gallery
        """
        try:
            get_prev = Image.objects.order_by('id').filter(galleries__exact=gallery,id__gt=self.id)
            return get_prev[0]
        except:
                return None
    def return_size(self):
        return "%sx%s" % (self.image.width,self.image.height)
    
    def get_next_in_gallery(self, gallery):
        """
        Get next by publication date filtering a given gallery
        """
        try:
            get_next = Image.objects.order_by('-id').filter(galleries__exact=gallery,id__lt=self.id)
            return get_next[0]
        except:
                return None

    def get_previous_general(self,owner):
        """
        Get previous by publication date filtering a given gallery
        """
        try:
            get_prev = Image.objects.order_by('id').filter(id__gt=self.id,owner=owner)
            return get_prev[0]
        except:
                return None

    def get_next_general(self,owner):
        """
        Get next by publication date filtering a given gallery
        """
        try:
            get_next = Image.objects.order_by('-id').filter(id__lt=self.id,owner=owner)
            return get_next[0]
        except:
                return None


    @models.permalink
    def get_absolute_url(self):
        try:
            gal = self.main_gallery()
        except:
            return 'image_standalone',(),{'object_id':self.id}
        return 'image_detail', (), {'username': self.owner.username,'g_slug':gal.slug,'slug':self.slug,}

    def __unicode__(self):
        return self.title

    class Meta:
        ordering = ['-id']
        get_latest_by = 'added'

    def image_img(self):
        if self.image:
            print  self.image
            return u'<img src="%s" />' % self.image.extra_thumbnails['small']
        else:
            return '(No Image)'
    image_img.short_description = 'Thumb'
    image_img.allow_tags = True

class Playable(Media):
    length = models.CharField(max_length=12)
    class Meta:
        ordering = ['-view_count']



def upload_to(attributeu,attributev):
    def upload_callback(instance, filename):
        u=getattr(instance, attributeu)
        v=getattr(instance,attributev)
        return 'uploads/users/%s/videos/%s/o/%s' % (u.id,v,filename)
    return upload_callback


class Video(Playable):
    media = FileField(upload_to=upload_to('owner','id'),null=True,blank=True)
    slug = models.SlugField(unique=False)
    width = models.PositiveSmallIntegerField(null=True)
    height = models.PositiveSmallIntegerField(null=True)
    agency = models.CharField('Ad Agency',max_length=60,null=True,blank=True)
    client = models.CharField('Advertiser / Client',max_length=60,null=True,blank=True)
    region = models.ForeignKey(Country,null=False,blank=False)
    adcategory = models.ForeignKey(AdCategory,null=False,blank=False)
    original = models.CharField(max_length=200)
    type = models.CharField(max_length=1,choices=VIDEO_TYPE, blank=True)
    oembed = models.URLField(blank=True,null=True,verify_exists=False)
    converted = models.CharField(max_length=200)
    thumbnail = models.CharField(max_length=200)
    thumb_times = models.CharField(max_length=200)
    #flag
    tags = TagField()

    def delete(self):
        if not self.oembed:
            if settings.LOCALVIDEO:
                relative_video_dir = 'uploads/users/%s/videos/%s' % (self.owner.id,self.pk)
                absolute_video_dir = settings.MEDIA_ROOT + relative_video_dir
                try:
                    os.remove(absolute_video_dir)
                except:
                    pass
                try:
                    check_clean_cache(self)
                except:
                    pass
            else:
                try:
                    relative_video_dir = 'uploads/users/%s/videos/%s' % (self.owner.id,self.pk)
                    absolute_video_dir = settings.MEDIA_ROOT + relative_video_dir
                    os.remove(absolute_video_dir)
                except:
                    pass
                try:
                    filename_key = "u/%s/video/%s.mp4" % (self.owner.id,self.pk)
                    delete_video_s3(filename_key)
                except:
                    pass
                try:
                    check_clean_cache(self)
                except:
                    pass
        else:
            try:
                portfolio=self.portfolio_set.all()[0]
            except:
                portfolio=None
            try:
                cportfolio=self.companyportfolio_set.all()[0]
            except:
                cportfolio=None
            check_clean_cache(self)
        super(Video, self).delete()

    def save(self,update=False,webvideo=False,force_insert=False, force_update=False):
        #Note that thumbnails won't be generated if the video is
        #updated. To modify a video just insert a new one
        if self.id and not webvideo:
            super(Video,self).save()
        else:
            if webvideo:
                if not update:
                    resource = oembed.site.embed(self.oembed)
                    info_video = resource.get_data()
                    if info_video:
                        super(Video, self).save(force_insert, force_update)
                        VIDEO_UPLOAD_PATH='uploads/users/%s/videos/%s/w/' % (self.owner.id,self.pk)
                        folder = settings.MEDIA_ROOT + VIDEO_UPLOAD_PATH
                        # We clean the folder from previous files
                        removeDir(folder)
                        try:
                            makedirs(folder,mode=0760)
                        except:
                            pass
                        thumbnail_url = info_video.get('thumbnail_url',None)
                        if thumbnail_url:
                            self.thumbnail, self.thumb_times = generate_thumb_web(video_id=self.pk,url=thumbnail_url,destination=VIDEO_UPLOAD_PATH)
                        else:
                            self.thumbnail, self.thumb_times = generate_thumb_web()
                        self.slug = slugify(self.title)
                        if info_video.get('width',None) and info_video.get('height',None):
                            self.width,self.height = int(info_video.get('width',None)),int(info_video.get('height',None))
                        else:
                            self.width,self.height = 0,0
                    clean_cache('new_media_list')
                    super(Video, self).save(force_insert, force_update)
                else:
                    super(Video, self).save(force_insert, force_update)
            else:
                super(Video, self).save(force_insert, force_update)
                if not update:
                    media = self.media
                    media_ext = basename(media.path).split('.')[1]
                    media_path_clean = dirname(media.path)+'/'+ "video%s.%s" % (self.pk,media_ext)
                    try:
                        unlink(media_path_clean)
                    except:
                        pass
                    rename(media.path,media_path_clean)
                    self.original=media_path_clean
                    VIDEO_UPLOAD_PATH='uploads/users/%s/videos/%s/c/' % (self.owner.id,self.pk)
                    # Dynamic folder is created
                    folder = settings.MEDIA_ROOT + VIDEO_UPLOAD_PATH
                    # We clean the folder from previous files
                    removeDir(folder)
                    try:
                        makedirs(folder,mode=0760)
                    except:
                        pass
                    #We set the ffmpeg temp dir
                    ffmpeg_tmp_dir = settings.MEDIA_ROOT + VIDEO_UPLOAD_PATH + "ffmpeg/"
                    removeDir(ffmpeg_tmp_dir)
                    makedirs(ffmpeg_tmp_dir,mode=0760)
                    chdir(ffmpeg_tmp_dir)
                    # The path to the created video is saved
                    self.converted = '%svideo%s.mp4' % (VIDEO_UPLOAD_PATH, self.pk)
                    converted_path = settings.MEDIA_ROOT + self.converted
                    # The video is created
                    #command_line = "ffmpeg -i %s -sameq -f flv %s"% (media.path, original_path)
                    command_line = "ffmpeg -i %s -y -an -pass 1 -passlogfile ffmpegcodec -vcodec libx264 -threads 4 -b 1024k -flags +loop -cmp +chroma -partitions +parti4x4+partp8x8+partb8x8 -subq 1 -trellis 0 -refs 1 -bf 3 -b_strategy 1 -coder 1 -me_range 16 -g 250 -keyint_min 25 -sc_threshold 40 -i_qfactor 0.71 -rc_eq 'blurCplx^(1-qComp)' -qcomp 0.6 -qmin 10 -qmax 51 -qdiff 4 %s;ffmpeg -i %s -y -acodec libfaac -ab 128k -pass 2 -passlogfile ffmpegcodec -vcodec libx264 -threads 4 -b 1024k -flags +loop -cmp +chroma -partitions +parti4x4+partp8x8+partb8x8 -flags2 +mixed_refs -subq 5 -trellis 1 -refs 5 -bf 3 -b_strategy 1 -coder 1 -me_range 16 -g 250 -keyint_min 25 -sc_threshold 40 -i_qfactor 0.71 -rc_eq 'blurCplx^(1-qComp)' -qcomp 0.6 -qmin 10 -qmax 51 -qdiff 4 %s" % (media_path_clean, converted_path, media_path_clean, converted_path)
                    a = commands.getoutput(command_line)
                    quickstart_command = "qtfaststart.py %s" % (converted_path)
                    a = commands.getoutput(quickstart_command)
                    # We check if the video was succesfully created
                    if 'ERROR' in a:
                        raise Exception(_('Error converting the video. Please ' +
                                          'use one of the supported formats.'))
                    if self.media:
                        try:
                            self.length = get_length(self.converted)
                        except:
                            self.length = _('Not available')
                        self.thumbnail, self.thumb_times = generate_thumb(self.converted, self.length)
                    if not settings.DEBUG:
                        try:
                            filename_key = "u/%s/video/%s.mp4" %  (self.owner.id,self.pk)
                            store_video_in_s3(filename_key,converted_path)
                            self.converted = "https://s3.amazonaws.com/adcake/" + filename_key
                            os.remove(converted_path)
                        except:
                            pass
                    self.slug = slugify(self.title)
                    self.width,self.height = get_size(converted_path)
                    #Unset FFMPEG TMP DIRECTORY
                    chdir('..')
                    removeDir(ffmpeg_tmp_dir)
                    clean_cache('new_media_list')
                    super(Video, self).save(force_insert, force_update)

    def get_thumbnails(self):
        return [(self.thumbnail % i) for i in self.thumb_times.split(',')]
    def get_second_thumbnail(self):
        return (self.thumbnail % "0")
    @models.permalink
    def get_absolute_url(self):
        return ('video_detail', [str(self.pk),self.slug])

    def __unicode__(self):
        return self.title
    class Meta:
        ordering = ['-view_count']

class Audio(Playable):
    media = models.FileField(upload_to='uploads/audio/')

    def __unicode__(self):
        return self.title

class AuthorsManager(models.Manager):
    def authorized(self):
        return self.filter(authorized=True)
    
    def pending_roles(self,author):
        return self.filter(media_file__owner=author,authorized=False)
        
class AuthorRole(models.Model):
    media_file = models.ForeignKey(Media, related_name='authors')
    author = models.ForeignKey(User, related_name='roles')
    authorized  = models.BooleanField()
    role = models.CharField(max_length=200,blank=True)
    objects = AuthorsManager()
    def __unicode__(self):
        return "%s as %s" % (self.author, self.role)
