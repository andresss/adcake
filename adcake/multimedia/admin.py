from django.contrib import admin
from django import forms

from spotlight.admin import SpotlightInline

from models import Image, Playable, AuthorRole, Video, Audio



class VideoForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        super(VideoForm, self).__init__(*args, **kwargs)

    class Meta:
        model = Video

class AuthorRole(admin.TabularInline):
    model = AuthorRole
    extra = 3

class ImageAdmin(admin.ModelAdmin):
    list_display = ('title','image_img','caption', 'added', 'view_count')
    list_filter = ['added']
    search_fields = ['title']
    prepopulated_fields = {'slug': ('title',),}
    inlines = [AuthorRole,SpotlightInline]

class VideoAdmin(admin.ModelAdmin):
    exclude = ['media','converted','length', 'original', 'thumbnail', 'thumb_times']
    form = VideoForm
    search_fields = ['title']
    inlines = [AuthorRole, SpotlightInline]

class AudioAdmin(admin.ModelAdmin):
    inlines = [AuthorRole]

admin.site.register(Image, ImageAdmin)
admin.site.register(Video, VideoAdmin)
admin.site.register(Audio, AudioAdmin)

