from django import forms
from django.utils.translation import ugettext_lazy as _
from django.template.defaultfilters import slugify
from multimedia.models import Video
from django.conf import settings
from models import Image

class AuthorRoleForm(forms.Form):
    role = forms.CharField(max_length=200, label=
                           _('How did you contribute to the creation of this work?'),
                           help_text=_('Example:Art Director, Producer'),
                           min_length=2)
    class Media:
         js = ('js/roles/add_author.js',)


class ImageForm(forms.ModelForm):
    role = forms.CharField(max_length=200, label=_('How did you contribute to the creation of this work?'),help_text=_('Example: Producer, Art Director, Designer'),min_length=2)
    def __init__(self,*args, **kwargs):
        self.gallery= kwargs.pop('gallery', None)
        print "  a:", args
        print "  k:", kwargs
        super(ImageForm, self).__init__(*args, **kwargs)

    def clean_title(self):
        if self.gallery.title_used(slug=slugify(self.cleaned_data['title'])):
             raise forms.ValidationError(_('An image with this title in the gallery already\
            exists'))
        return self.cleaned_data['title']

    def clean_image(self):
        file = self.cleaned_data['image']
        if file:
            if len(file.name.split('.')) == 1:
                raise forms.ValidationError(_('File type is not supported'))
            file_split = file.name.split('.')
            extension = file_split[-1]
            if not extension in settings.IMAGE_UPLOAD_FILE_EXTENSIONS:
                raise forms.ValidationError(_('Image format not supported, use one of the following formats: png, jpeg'))
            else:
                file_type=file.content_type
                if not file_type in settings.IMAGE_UPLOAD_FILE_TYPES:
                    raise forms.ValidationError(_('File type is not supported, it must be a jpeg or png file'))
        return file

    class Meta:
        model = Image
        fields = ( 'image','title', 'creation','role','comment', 'caption', 'tags')

class ImageMetaForm(forms.ModelForm):
    def clean_title(self):
        gallery = self.instance.galleries.all()[0]
        if not self.instance.slug == slugify(self.cleaned_data['title']) and gallery.title_used(slug=slugify(self.cleaned_data['title'])):
             raise forms.ValidationError(_('An image with this title in the gallery already\
            exists'))
        return self.cleaned_data['title']

    class Meta:
        model = Image
        fields = ('title', 'creation', 'comment', 'caption', 'tags')

class VideoMetaForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        user = kwargs.pop('user', None)
        super(VideoMetaForm, self).__init__(*args, **kwargs)
        if user:
            video_checked = True if user.portfolio.contains(self.instance) else False
            self.fields['portfolio'] = forms.BooleanField(initial=video_checked,label="Include in your portfolio",required=False)
            qs = user.companies.all()
            if len(qs) > 0 and qs[0].cportfolio.contains(self.instance):
                company_initial=qs[0]
            else:
                company_initial=None
            if len(qs) > 0:
                self.fields['mcompany'] = forms.ModelChoiceField(label="Add this video to your company",queryset=qs,initial=company_initial,required=False,empty_label="No Company")
            else:
                self.fields['mcompany'] = forms.ModelMultipleChoiceField(label="Add this video to your company",queryset=qs,widget=forms.HiddenInput,required=False)

    def clean(self):
        cleaned_data = self.cleaned_data
        mcompany = cleaned_data.get("mcompany")
        portfolio = cleaned_data.get("portfolio")
        if not portfolio and not mcompany:
            raise forms.ValidationError("You have to include the video in your portfolio or in your company.")
        return cleaned_data

    class Meta:
        model = Video
        fields = ('title','adcategory','agency','client','region','creation','tags')