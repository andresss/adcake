from django.conf.urls.defaults import *
from views import add_author,change_role

urlpatterns = patterns('',
   url('^add_author/$', add_author, name='add_author'),
   url('^change_role/$', change_role, name='change_role'),

)
