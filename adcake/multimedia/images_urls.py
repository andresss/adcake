from django.conf.urls.defaults import *
from django.views.generic.simple import redirect_to
from models import Image
from tagging.views import tagged_object_list

urlpatterns = patterns('multimedia.views',
    url(r'^(?P<object_id>\d+)/$', 'image_detail', name='image_standalone'),
    url(r'^list/$', redirect_to, {'url': '/images/list/page1/'}, name='image_list'),
    url(r'^list/page(?P<page>[0-9]+)/$', 'image_list',name='image_list_page'),
    url(r'^tags/(?P<tag>[\w\-. ]+)/page(?P<page>[0-9]+)/$', tagged_object_list, dict(queryset_or_model=Image, paginate_by=25, template_object_name='image', related_tags=True), name='images_for_tag'),
)
