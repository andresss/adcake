from django.contrib.auth.models import User
from django.core.paginator import InvalidPage, EmptyPage, Paginator
from django.http import Http404, HttpResponseRedirect
from django.shortcuts import render_to_response
from django.template import RequestContext
from django.template.defaultfilters import slugify
from django.views.generic.list_detail import object_list
from django.contrib.auth.decorators import login_required
from django.views.decorators.http import require_POST
from helpers.utils import check_clean_cache
from multimedia.forms import VideoMetaForm
from multimedia.models import Video, Media, Image, AuthorRole
from django.core.urlresolvers import reverse
from portfolio.forms import VideoAuthorForm, VideoAuthorFormWeb
from contrib.json import JsonResponse
from django.utils.translation import ugettext as _
from django.http import Http404
from django.contrib.comments import *
from django.conf import settings
from multimedia.forms import ImageMetaForm
from django.contrib.contenttypes.models import ContentType
from endless_pagination.decorators import page_template
from django.shortcuts import get_object_or_404

def filter_videos(request, username, page, filter=None, role=None):
    if request.GET.get('style', None) == None:
        grid = settings.DEFAULT_SHOW_VIDEOS_IN == "grid"
    else:
        grid = request.GET.get('style', None) == "grid"
    user = get_object_or_404(User, username=username)
    pages = (grid and 35) or 7
    if filter is 'by':
        videos = Video.objects.filter(owner=user)
    if filter is 'with':
        videos = Video.objects.filter(authors__author=user).distinct()
    if filter is 'with_as':
        videos = Video.objects.distinct().filter(authors__role=role,
                                                 authors__author=user)
    if not filter:
        raise Exception("Wrong filter")
    
    return object_list(request, queryset=videos, template_object_name='video', 
                       paginate_by=pages,page=page,extra_context={'author':user,
                                                                  'role': role,
                                                                  'filter':filter,
                                                                  'grid': grid,
                                                                  'form': VideoAuthorForm(user=request.user),
                                                                  'formw': VideoAuthorFormWeb(user=request.user)})


@login_required
def videos_by(request, username, page):
    return filter_videos(request, username, page, filter='by')

@login_required
def videos_with(request, username, page):
    return filter_videos(request, username, page, filter='with')

def videos_with_author_as(request, username, role, page):
    return filter_videos(request, username, page, filter='with_as', role=role)


def all_videos(request, page):
    if request.GET.get('style', None) == None:
        grid = settings.DEFAULT_SHOW_VIDEOS_IN == "grid"
    else:
        grid = request.GET.get('style', None) == "grid"
    pages = 12
    if request.user.is_authenticated():
        formf = VideoAuthorForm(user=request.user)
        formw = VideoAuthorFormWeb(user=request.user)
    else:
        formf = None
        formw = None
    if request.method == "POST" and request.user.is_authenticated():
        if request.POST['type'] == "F":
            formf = VideoAuthorForm(request.POST, request.FILES,user=request.user)
            if formf.is_valid():
                video = formf.save(commit=False)
                video.owner = request.user
                video.type = request.POST['type']
                video.save()
                AuthorRole(media_file=video.media_ptr, author=request.user, authorized=True, role=formf.cleaned_data['role']).save()
                if formf.cleaned_data['mcompany']:
                    for comp in formf.cleaned_data['mcompany']:
                        comp.cportfolio.playable.add(video)
                if formf.cleaned_data['portfolio']:
                    request.user.portfolio.set_featured_playable(video)
                return HttpResponseRedirect(reverse('video_detail',
                                                    args=[video.pk,video.slug]))
        elif request.POST['type'] == "O":
            formw = VideoAuthorFormWeb(request.POST,user=request.user)
            if formw.is_valid():
                video = formw.save(commit=False)
                video.owner = request.user
                video.type = request.POST['type'] 
                video.save(webvideo=True)
                AuthorRole(media_file=video.media_ptr, author=request.user, authorized=True, role=formw.cleaned_data['role']).save()
                if formw.cleaned_data['mcompany']:
                    for comp in formw.cleaned_data['mcompany']:
                        comp.cportfolio.playable.add(video)
                if formw.cleaned_data['portfolio']:
                    request.user.portfolio.set_featured_playable(video)
                return HttpResponseRedirect(reverse('video_detail',
                                                    args=[video.pk,video.slug]))
    return object_list(request, queryset=Video.objects.all(), template_object_name='video', paginate_by=pages,page=page,extra_context={'grid': grid,'formw': formw,'form':formf})



def image_list(request,page=1):
    return object_list(request, queryset=Image.objects.all(), template_object_name='image', paginate_by=16,page=page,template_name='multimedia/image_list_format.html')

def get_comment_videos_by_page(page=1,video_id=1):
    from django.core.paginator import Paginator, InvalidPage, EmptyPage
    items = Comment.objects.filter(object_pk=video_id)
    paginator = Paginator(items, 2)
    try:
        p = int(page)
    except ValueError:
        p = 1
    try:
        items = paginator.page(p)
        print paginator.page_range
        print paginator.num_pages
    except (EmptyPage, InvalidPage):
        items = paginator.page(paginator.num_pages)
    return items

@page_template("multimedia/video_comment_pagination.html")
def vid_detail(request,template="multimedia/video_detail.html",video_id=1,*args, **kwargs):
    co = ContentType.objects.get(app_label="multimedia",model="video")
    comments = Comment.objects.filter(object_pk=video_id,content_type=co)
    video = get_object_or_404(Video,id=video_id)
    video.view_count +=1
    context = {
      'localvideo': settings.LOCALVIDEO,
      'comments':comments,
      'video':video,
      'page_template': page_template,
    }
    try:
        company = video.companyportfolio_set.all()[0].company
    except:
        company = None
    try:
        user_portfolio = video.portfolio_set.all()[0].owner
    except:
        user_portfolio = None
    context['company']=company
    context['user_portfolio']=user_portfolio
    if request.user.is_authenticated() and request.POST:
        form = VideoMetaForm(request.POST, instance=video,user=request.user)
        if form.is_valid():
            video = form.save(commit=False)
            video.slug = slugify(video.title)
            video.save(webvideo=True,update=True)
            if form.cleaned_data['mcompany']:
                comp = form.cleaned_data['mcompany']
                comp.cportfolio.playable.add(video)
            else:
                try:
                    comp = request.user.companies.all()[0]
                    comp.cportfolio.playable.remove(video)
                except:
                    pass
            if form.cleaned_data['portfolio']:
                request.user.portfolio.set_featured_playable(video)
            else:
                request.user.portfolio.unset_featured_playable(video)
            form = VideoMetaForm(instance=video,user=request.user)
            context['form']=form
            return HttpResponseRedirect(reverse('video_detail',args=[video.id,video.slug]))
        else:
            context['form']=form
    else:
        if request.user.is_authenticated():
            form = VideoMetaForm(instance=video,user=request.user)
            context['form']=form
        else:
            form = None
            context['form']=form
        if video.oembed:
            video.save(update=True,webvideo=True)
        else:
            video.save(update=True)
    return render_to_response(template,context,context_instance=RequestContext(request))
   
@login_required
@require_POST
def add_author(request):
    try:
        id = request.POST.get('id')
        user = request.user
        role = unicode(request.POST.get('role'))
        if len(role) < 2:
            raise Exception("Too small")
        m = Media.objects.get(pk=id)
        if m.owner.username != user.username:
            m.authors.create(author=user, role=role, authorized=False)
            m.save()
            return JsonResponse({'status':_('You have been successfully added as an author, wait for an authorization of the owner of this work')})
        else:
            m.authors.create(author=user, role=role, authorized=True)
            m.save()
            return JsonResponse({'status':_('You have added yourself to this work, you may refresh the page.')})
    except Exception, e:
        return JsonResponse({'errors': e.args[0]})
    
@login_required
@require_POST
def change_role(request):
    try:
        id = request.POST.get('role_id')
        action = request.POST.get('action')
        ar=AuthorRole.objects.get(id=id)
        print action
        if ar.media_file.owner == request.user:
            if action == 'accept':
                ar.authorized=True
                ar.save()
            elif action == 'decline':
                ar.delete()
            return JsonResponse({'status':_('You have been successfully accepted the request')})
        else:
            raise Exception("You don't have privileges to do this.")
    except Exception, e:
        return JsonResponse({'errors': e.args[0]})
    
# Image Views
@page_template("multimedia/standalone_image/image_detail_page.html")
def image_detail(request,template="multimedia/standalone_image/image_detail.html", *args, **kwargs):
    """
    A thin wrapper around object_detail that ensures the image increase
    its view count and calculate if the image has to be zoomed out to fit
    the page size
    """
    img_id = kwargs.get('object_id', '')
    co = ContentType.objects.get(app_label="multimedia",model="image")
    comments = Comment.objects.filter(object_pk=img_id,content_type=co)
    try:
        img = Image.objects.get(pk=img_id)
        img.view_count += 1
        img.save()
    except Image.DoesNotExist:
        raise Http404('Image Does Not Exist')
    try:
        g=img.galleries.all()[0]
        g.view_count+=1
        g.save()
    except:
        pass
    if request.method == "POST":
        form = ImageMetaForm(request.POST, instance=img)
        if form.is_valid():
            image = form.save(commit=False)
            image.owner = request.user
            image.slug = slugify(image.title)
            image.save()
            check_clean_cache(image)
            form.save_m2m()
            return HttpResponseRedirect(reverse('image_standalone',args=[image.pk]))
    else:
        form = ImageMetaForm(instance=img)

    if not kwargs.has_key('extra_context'):
        kwargs['extra_context'] = {}
    do_zoom=img.image.width > settings.IMAGE_SHOWCASE_SIZE[0] or img.image.height > settings.IMAGE_SHOWCASE_SIZE[1]
    zoom_dim=""
    if do_zoom:
        if img.image.width < 2000 or img.image.height < 1500:
            zoom_dim = "%sx%s" % (img.image.width,img.image.height)
    context = {
        'form':form,
        'image': img,
        'comments':comments,
        'objects': Image.objects.all(),
        'page_template': page_template,
        'do_zoom' : do_zoom,
        'zoom_dim' : zoom_dim,
       }
    kwargs['extra_context']['do_zoom'] = \
                        img.image.width > settings.IMAGE_SHOWCASE_SIZE[0] or \
                        img.image.height > settings.IMAGE_SHOWCASE_SIZE[1]
    return render_to_response(template,context,context_instance=RequestContext(request))

