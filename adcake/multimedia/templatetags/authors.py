from django import template
from django.template import Library, Context
from django.core.urlresolvers import reverse
from multimedia.forms import AuthorRoleForm
from multimedia.models import Media
from settings import MEDIA_URL
register = Library()

class AuthorsNode(template.Node):
    def __init__(self, ctype, object_id):
        self.ctype = ctype
        self.object_id = template.Variable(object_id)
    def render(self, context):
        user = context['request'].user
        m = Media.objects.get(pk=self.object_id.resolve(context))
        d = ({'media': m, 'ctype': self.ctype, 'form': AuthorRoleForm(),'MEDIA_URL':MEDIA_URL,'user':user})
        c = Context(d, autoescape=context.autoescape)
        t = template.loader.get_template('authors.html')
        return t.render(c)

@register.tag(name="authors")
def authors(parser, token):
    """
    Shows the authors for a given media file and provides the
    possibility of adding yourself as an author.
    Usage::

        {% authors for [media_type] with [media.id] %}

    Example::

        {% authors for videos with video.id %}
    
    """
    try:
        tag_name, for_s, ctype, with_s, object_id = token.split_contents()
    except ValueError:
        raise template.TemplateSyntaxError, "%r tag requires the following arguments: {% authors for 'type' with object_id %}" % token.contents.split()[0]
    if not (for_s == 'for' and with_s == 'with'):
        raise template.TemplateSyntaxError, "One of %r tag's keywords ('for', 'with') is not present" % tag_name
    return AuthorsNode(ctype, object_id)

@register.simple_tag
def author_url(utype, ctype, object_id, role=None):
    print utype, ctype, object_id, role
    args = [object_id, 1]
    if utype=='with_author_as':
        args.insert(1, role)
    return reverse(ctype+'_'+utype, args=args)
