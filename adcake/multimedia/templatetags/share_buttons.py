from django.template import Library
register = Library()

@register.inclusion_tag('add_this.html', takes_context=False)
def share_buttons(element_id=None):
    return {'element_id':element_id}
