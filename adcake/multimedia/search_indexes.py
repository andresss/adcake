from haystack import indexes
from haystack.sites import site
from multimedia.models import Video, Image

class VideoIndex(indexes.SearchIndex):
    text = indexes.CharField(document=True, use_template=True)
    rendered = indexes.CharField(use_template=True, indexed=False)

site.register(Video, VideoIndex)

class ImageIndex(indexes.SearchIndex):
    text = indexes.CharField(document=True, use_template=True)
    rendered = indexes.CharField(use_template=True, indexed=False)

site.register(Image, ImageIndex)
