from django.conf.urls.defaults import *
from views import videos_by, videos_with, videos_with_author_as, all_videos,vid_detail
from models import Video
from tagging.views import tagged_object_list
urlpatterns = patterns('django.views.generic',
   url('^page(?P<page>\d+)/$', all_videos, name='video_index'),
   url('^by_(?P<username>[\w.-]+)/page(?P<page>\d+)/$', videos_by, name='videos_by'),
   url('^with_(?P<username>[\w.-]+)/page(?P<page>\d+)/$', videos_with, name='videos_with'),
   url('^with_(?P<username>[\w.-]+)as_(?P<role>.+)/page(?P<page>\d+)/$', videos_with_author_as, name='videos_with_author_as'),
   url('^(?P<video_id>\d+)/$', vid_detail, name='video_detail'),
   url('^(?P<video_id>\d+)/(?P<slug>[\w-]+)/$', vid_detail, name='video_detail'),
   url(r'^tags/(?P<tag>[\w\-. ]+)/page(?P<page>[0-9]+)/$', tagged_object_list, dict(queryset_or_model=Video, paginate_by=25, template_object_name='video',template_name='multimedia/video_tag_list.html',related_tags=True), name='videos_for_tag'),

)
