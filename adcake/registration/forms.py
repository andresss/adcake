"""
Forms and validation code for user registration.

"""
import httplib, urllib, settings
from django import forms
from django.core.urlresolvers import reverse
from django.http import HttpResponseRedirect, Http404
from django.shortcuts import render_to_response
from django.utils.translation import ugettext_lazy as _
from django.contrib.auth.models import User
from django.contrib.formtools.wizard import FormWizard
from accounts.models import ReservedName
from directory.models import Company
from invitation.models import InvitationKey
from registration.models import RegistrationProfile
from accounts.utils import profile_creation

# I put this on all required fields, because it's easier to pick up
# on them with CSS or JavaScript if they have a class of "required"
# in the HTML. Your mileage may vary. If/when Django ticket #3515
# lands in trunk, this will no longer be necessary.
attrs_dict = { 'class': 'required' }

class RegistrationForm(forms.Form):
    """
    Form for registering a new user account.
    
    Validates that the requested username is not already in use, and
    requires the password to be entered twice to catch typos.
    
    Subclasses should feel free to add any additional validation they
    need, but should either preserve the base ``save()`` or implement
    a ``save()`` which accepts the ``profile_callback`` keyword
    argument and passes it through to
    ``RegistrationProfile.objects.create_inactive_user()``.
    
    """
    username = forms.RegexField(regex=r'^\w+$',
                                max_length=30,
                                widget=forms.TextInput(attrs=attrs_dict),
                                label=_(u'username'))
    email = forms.EmailField(widget=forms.TextInput(attrs=dict(attrs_dict,
                                                               maxlength=75)),
                             label=_(u'email address'))
    password1 = forms.CharField(widget=forms.PasswordInput(attrs=attrs_dict, render_value=False),
                                label=_(u'password'))
    password2 = forms.CharField(widget=forms.PasswordInput(attrs=attrs_dict, render_value=False),
                                label=_(u'password (again)'))
    
    def clean_username(self):
        """
        Validate that the username is alphanumeric and is not already
        in use.
        
        """
        print "registration-process"
        if User.objects.filter(username__iexact=self.cleaned_data['username']):
            raise forms.ValidationError(_(u'This username is already taken. Please choose another.'))
        else:
            return self.cleaned_data['username']

    def clean(self):
        """
        Verifiy that the values entered into the two password fields
        match. Note that an error here will end up in
        ``non_field_errors()`` because it doesn't apply to a single
        field.
        
        """
        if 'password1' in self.cleaned_data and 'password2' in self.cleaned_data:
            if self.cleaned_data['password1'] != self.cleaned_data['password2']:
                raise forms.ValidationError(_(u'You must type the same password each time'))
        return self.cleaned_data
    
    def save(self, profile_callback=None):
        """
        Create the new ``User`` and ``RegistrationProfile``, and
        returns the ``User``.
        
        This is essentially a light wrapper around
        ``RegistrationProfile.objects.create_inactive_user()``,
        feeding it the form data and a profile callback (see the
        documentation on ``create_inactive_user()`` for details) if
        supplied.
        
        """
        new_user = RegistrationProfile.objects.create_inactive_user(username=self.cleaned_data['username'],
                                                                    password=self.cleaned_data['password1'],
                                                                    email=self.cleaned_data['email'],
                                                                    profile_callback=profile_callback)
        return new_user


class RegistrationFormTermsOfService(RegistrationForm):
    """
    Subclass of ``RegistrationForm`` which adds a required checkbox
    for agreeing to a site's Terms of Service.
    
    """
    tos = forms.BooleanField(widget=forms.CheckboxInput(attrs=attrs_dict),
                             label=_(u'I have read and agree to the Terms of Service'),
                             error_messages={ 'required': u"You must agree to the terms to register" })


class RegistrationFormUniqueEmail(RegistrationForm):
    """
    Subclass of ``RegistrationForm`` which enforces uniqueness of
    email addresses.
    
    """
    def clean_email(self):
        """
        Validate that the supplied email address is unique for the
        site.
        
        """
        if User.objects.filter(email__iexact=self.cleaned_data['email']):
            raise forms.ValidationError(_(u'This email address is already in use. Please supply a different email address.'))
        return self.cleaned_data['email']


class RegistrationFormNoFreeEmail(RegistrationForm):
    """
    Subclass of ``RegistrationForm`` which disallows registration with
    email addresses from popular free webmail services; moderately
    useful for preventing automated spam registrations.
    
    To change the list of banned domains, subclass this form and
    override the attribute ``bad_domains``.
    
    """
    bad_domains = ['aim.com', 'aol.com', 'email.com', 'gmail.com',
                   'googlemail.com', 'hotmail.com', 'hushmail.com',
                   'msn.com', 'mail.ru', 'mailinator.com', 'live.com']
    
    def clean_email(self):
        """
        Check the supplied email address against a list of known free
        webmail domains.
        
        """
        email_domain = self.cleaned_data['email'].split('@')[1]
        if email_domain in self.bad_domains:
            raise forms.ValidationError(_(u'Registration using free email addresses is prohibited. Please supply a different email address.'))
        return self.cleaned_data['email']


class Registration1(forms.Form):
    """
    """
    username = forms.RegexField(label=_("Username"), max_length=30, regex=r'^[\w.-]+$',
        help_text = _("Required. 30 characters or fewer. Letters, digits and .-_ only."),
        error_message = _("This value may contain only letters, numbers and .-_ characters."))
    first_name = forms.CharField(label=_(u'first name'), widget=forms.TextInput(attrs=attrs_dict))
    last_name = forms.CharField(label=_(u'last name'), widget=forms.TextInput(attrs=attrs_dict))
    email = forms.EmailField(widget=forms.TextInput(attrs=dict(attrs_dict,
                                                               maxlength=75)),
                             label=_(u'email address'),
                             help_text=_(u'Please enter a valid email address, a confirmation will be sent'))
    password1 = forms.CharField(widget=forms.PasswordInput(attrs=attrs_dict, render_value=False),
                                label=_(u'password'))
    password2 = forms.CharField(widget=forms.PasswordInput(attrs=attrs_dict, render_value=False),
                                label=_(u'password (again)'))

    def clean_email(self):
        """
        Validate that the supplied email address is unique for the
        site.
        
        """
        if User.objects.filter(email__iexact=self.cleaned_data['email']):
            raise forms.ValidationError(_(u'This email address is already in use. Please supply a different email address.'))
        return self.cleaned_data['email']

    def clean_username(self):
        """
        Validate that the username is alphanumeric and is not already
        in use.
        
        """
        if User.objects.filter(username__iexact=self.cleaned_data['username']) or ReservedName.objects.filter(reserved__iexact=self.cleaned_data['username']) or Company.objects.filter(uniquename__iexact=self.cleaned_data['username']):
            raise forms.ValidationError(_(u'This username is already taken. Please choose another.'))
        else:
            return self.cleaned_data['username'].lower()

    def clean(self):
        """
        Verifiy that the values entered into the two password fields
        match. Note that an error here will end up in
        ``non_field_errors()`` because it doesn't apply to a single
        field.
        
        """
        if 'password1' in self.cleaned_data and 'password2' in self.cleaned_data:
            if self.cleaned_data['password1'] != self.cleaned_data['password2']:
                raise forms.ValidationError(_(u'You must type the same password each time'))
        return self.cleaned_data


class Registration2(forms.Form):
    """
    """
    tos = forms.BooleanField(widget=forms.CheckboxInput(attrs=attrs_dict),
                             label=_(u'I have read and agree to the Terms and Conditions'),
                             error_messages={ 'required': u"You must agree to the terms to register" })
    # captcha


class RegistrationWizard(FormWizard):
    """
    """
    extra_context = {'recaptcha_public_key': settings.RECAPTCHA_PUBLIC_KEY,}
    def get_template(self, step):
        """
        Hook for specifying the name of the template to use for a given step.

        Note that this can return a tuple of template names if you'd like to
        use the template system's select_template() hook.
        """
        return 'registration/sign_up/form%s.html' % step

    def done(self, request, form_list):
        """
        Create the new ``User`` and ``RegistrationProfile``, and
        returns the ``User``.
        
        This is essentially a light wrapper around
        ``RegistrationProfile.objects.create_inactive_user()``,
        feeding it the form data and a profile callback (see the
        documentation on ``create_inactive_user()`` for details) if
        supplied.
        
        """
        form = form_list[0]
        
        new_user = RegistrationProfile.objects.create_inactive_user(username=form.cleaned_data['username'],
                                                                    password=form.cleaned_data['password1'],
                                                                    email=form.cleaned_data['email'])
        new_user.first_name = form.cleaned_data['first_name']
        new_user.last_name = form.cleaned_data['last_name']
        new_user.save()
        profile_creation(new_user)
        reservedname = ReservedName(reserved = new_user.username)
        reservedname.save()
        invitation_key = request.REQUEST.get('invitation_key', False)
        key = InvitationKey.objects.get_key(invitation_key)
        key.registrant=new_user
        key.save()
        return HttpResponseRedirect(reverse('registration_complete'))
    def __call__(self, *args, **kwargs):
        request = args[0]
        if request.POST:
                challenge_f = request.POST.get('recaptcha_challenge_field', False)
                response_f = request.POST.get('recaptcha_response_field', False)
                ip = request.META.get('REMOTE_ADDR')
                print ip
                if challenge_f:
                    errors = {}
                    if not challenge_f:
                        errors['captcha_c'] = 'An error occured with the CAPTCHA service. Please try again.'
                    if not response_f:
                        errors['captcha_s'] = 'Please enter the CAPTCHA solution.'

                    captcha_ok = False
                    if not errors:
                        value = validate_recaptcha(ip, challenge_f, response_f)
                        captcha_ok = value.get('result')
                        if settings.DEBUG:
                            captcha_ok=True
                    if not captcha_ok and not errors:
                        errors['captcha_s'] = 'An incorrect CAPTCHA solution was entered.'
                    if errors:
                        current_step = self.determine_step(request, *args, **kwargs)
                        form = self.get_form(current_step, request.POST)
                        form.errors.update(errors)
                        return self.render(form, request, current_step)

        return super(RegistrationWizard, self).__call__(*args, **kwargs)

def validate_recaptcha(remote_ip, challenge, response):
    # Request validation from recaptcha.net
    if challenge:
        params = urllib.urlencode(dict(privatekey=settings.RECAPTCHA_PRIVATE_KEY,
                                       remoteip=remote_ip,
                                       challenge=challenge,
                                       response=response))
        headers = {"Content-type": "application/x-www-form-urlencoded", "Accept": "text/plain"}
        conn = httplib.HTTPConnection("api-verify.recaptcha.net")
        conn.request("POST", "/verify", params, headers)
        response = conn.getresponse()
        if response.status == 200:
            data = response.read()
        else:
            data = ''
        conn.close()
    # Validate based on response data
    result = data.startswith('true')
    error_code = ''
    if not result:
        bits = data.split('\n', 2)
        if len(bits) > 1:
            error_code = bits[1]
    # Return dictionary
    return dict(result=result,
                error_code=error_code)


