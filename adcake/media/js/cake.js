var CakeTriggers = new Class ({

    Implements: [Options, Events],

    options: {
	className: 'add-cake-box-block',
	duration: 500,
	offsets: {x: 8, y: 8}
    },

    initialize: function(selector, cbHandler, options) {
	this.setOptions(options);
	this._build();

	this.triggers = $$(selector);
	this.triggers.each(this.parseTrigger.bind(this));
	this.cbHandler = cbHandler;
    },

    parseTrigger: function(trigger) {
	trigger.addEvent('click', (function(event) {
	    this.showBlock(event, trigger);
	}).bind(this));
    },

    showBlock: function(event, trigger) {
	new Event(event).stop();

	size = window.getSize(); scroll = window.getScroll();
	block = this.block.getSize();
	props = {x: 'left', y: 'top'};
	for (var z in props){
	    var pos = event.page[z] + this.options.offsets[z];
	    if ((pos + block[z] - scroll[z]) > size[z]) pos = event.page[z] - this.options.offsets[z] - block[z];
	    this.block.setStyle(props[z], pos);
	}

	this.block.fade('hide');
	this.overMessage.setStyle('visibility', 'visible').getElement('span').set('text', 'Loading');
	this.block.inject(document.body);
	this.block.fade('in');

	this.load(trigger);
    },

    _build: function() {
	if (this.block)
	    this.block.destroy();

	this.block = new Element('div', {'class': this.options.className});

	var title = new Element('h3', {'text': 'Add '}).inject(this.block);
	this.elementName = new Element('span').inject(title);
	title.appendText(' to your Cake Box');

	var form = new Element('form', {events: {'submit': this.save.bind(this)}}).inject(this.block);
	this.label = new Element('label', {'for': 'label', 'text': 'Select Label:'}).inject(form);
	this.label.store('input', new Element('select', {'name': 'label'}).inject(form));
	var switchHolder = new Element('div', {'class': 'switch-holder'}).inject(form);
	this.label.store('switch', new Element('span', {
	    'text': 'or add a New Label',
	    events: {'click': this.switchLabelInterface.bind(this)}
	}).inject(switchHolder));

	var inputContainer = new Element('div', {'class': 'input-container'}).inject(form);
	this.add = new Element('input', {
	    'type': 'button',
	    'value': 'Add',
	    events: {'click': this.save.bind(this)}
	}).inject(inputContainer);
	this.cancel = new Element('input', {
	    'type': 'button',
	    'value': 'Cancel',
	    events: {'click': this._unbuild.bind(this)}
	}).inject(inputContainer);

	// Set the loading state
	this.overMessage = new Element('div', {
	    'class': 'overall loading',
	    'styles': {'visibility': 'hidden'}
	}).inject(this.block);
	new Element('span').inject(this.overMessage);
    },

    _unbuild: function() {
	this.block.fade('out');
    },

    load: function(trigger) {
	var obj = trigger.get('rel').split(':');
	new Request.JSON({
	    url: '/cake/box/load_button/',
	    onComplete: this.charge.bind(this)
	}).post({'ctype': obj[0], 'obj_id': obj[1]});
	this.block.store('ctype', obj[0]);
	this.block.store('obj_id', obj[1]);
    },

    charge: function(response) {
	if (!$defined(response)) {
	    this.die('Oops, an error has ocurred');
	    return false;
	}
	if (response.succeed) {
	    this.elementName.set('text', response.item);

	    if (response.boxes.length > 0) {
		if (this.label.retrieve('input').get('tag') != 'select')
		    this.switchLabelInterface();

		// Destroy all the other options
		this.label.retrieve('input').getChildren('option').each(function(opt) {opt.destroy();});

		response.boxes.each(function(box) {
		    new Element('option', {'value': box.label, 'text': box.label}).inject(this.label.retrieve('input'));
		}, this);
	    } else {
		this.setLabelInterface('input');
		this.label.retrieve('switch').set('text', '');
	    }

	    // Unset the loading state
	    this.overMessage.setStyle('visibility', 'hidden');
	}
	else
	    this.die('Oops, an error has ocurred...');
    },

    save: function(submit) {
	if ($defined(submit))
	    new Event(submit).stop();
	this.overMessage.removeClass('error').addClass('Loading').setStyle('visibility', 'visible').getElement('span').set('text', 'Loading');
	var request_dict = {
	    'box_label': this.label.retrieve('input').get('value'),
	    'content_type': this.block.retrieve('ctype'),
	    'object_id': this.block.retrieve('obj_id')
	}
	new Request.JSON({
	    url: '/cake/box/save_item/',
	    onComplete: (function(response) {
		if (!$defined(response)) {
		    this.die('Oops, an error ocurred...');
		    return false;
		}
		if (response.box.created)
		    this.cbHandler.addBox(response.box.label, true);
		if (response.item.created)
		    this.cbHandler.addItem(response.item, response.box.label, true);
	    }).bind(this)
	}).post(request_dict);
	this._unbuild();
    },

    switchLabelInterface: function() {
	var input = this.label.retrieve('input');
	var toUse = this.unUsedLabel || new Element('input', {'type': 'text', 'name': 'label'});
	this.unUsedLabel = input.dispose();
	if (input.get('tag') == 'select') {
	    this.label.retrieve('switch').set('text', 'close').getParent().addClass('close-new-label');
	    this.label.set('text', 'New Label:');
	} else {
	    this.label.retrieve('switch').set('text', 'or add a New Label').getParent().removeClass('close-new-label');
	    this.label.set('text', 'Select Label:');
	}
	this.label.store('input', toUse.inject(this.label,'after'));
    },

    setLabelInterface: function(mode) {
	this.unUsedLabel = this.label.retrieve('input').dispose();
	if (mode == 'select') {
	    this.label.retrieve('switch').set('text', 'or add a New Label').getParent().removeClass('close-new-label');
	    this.label.set('text', 'Select Label:');
	    this.label.store('input', new Element('select', {'name': 'label'}).inject(this.label, 'after'));
	} else {
	    this.label.retrieve('switch').set('text', 'close').getParent().addClass('close-new-label');
	    this.label.set('text', 'New Label:');
	    this.label.store('input', new Element('input', {'type': 'text', 'name': 'label'}).inject(this.label, 'after'));
	}
    },

    die: function(errMsg) {
	this.overMessage.removeClass('loading').addClass('error').setStyle('visibility', 'visible').getElement('span').set('text', errMsg);
	this.block.fade.pass('out',this.block).delay(4000);
    }
});

var CakeBox = new Class ({
    initialize: function() {
	this._body = $('cake-box');
	this.load();
	new CakeTriggers('a.add-cake-box', this, {'offsets': {x:0,y:0}});
    },

    load: function() {
	new Request.JSON({
	    url: '/cake/box/load/',
	    onComplete: (function(rboxes) {
		rboxes.each(
		    (function(rbox) {
			var box_node = this.addBox(rbox.label, false);
			rbox.items.each(
			    (function(rbox_item) {
				this.addItem(rbox_item, box_node, false);
			    }).bind(this)
			);
			if (rbox.items.length == 0)
			    this.emptyBox(box_node);
		    }).bind(this)
		);
		if (rboxes.length == 0)
		    this.emptyCake();
	    }).bind(this)
	}).send();
    },

    resize: function() {
	this._body.getElement('br').set('styles', {'height': 0})
	if (this._body.getParent().getSize().y != 0)
	    this._body.getParent().setStyle('height', this._body.getSize().y + 30);
    },

    addBox: function(label, dynamic) {
	if (this._empty)
	    this._body.getChildren('li').destroy();

	var box = new Element('li');
	var box_label = new Element('h4', {'text': (label.length > 25 ? label.substr(0,25) + ' ...' : label)}).inject(box);
	var box_options = new Element('span', {'class': 'box-options'}).inject(box);

	box.label = box_label;
	box.label_text = label;
	box.rename_button = new Element('a', {'class': 'box-rename box-button edit-simple', 'href': '#', 'title': 'Rename this box'}).inject(box_options);
	box.delete_button = new Element('a', {'class': 'box-delete box-button delete-simple', 'href': '#', 'title': 'Delete this box'}).inject(box_options);
	new Element('br', {'class': 'clear'}).inject(box);

	// we add an empty list (ul) of items for this box.
	box.items = new Element('ul');
	box.items.set('styles', {'display': 'none'});
	box.items.open = false;

	// Finally, setting up the events
	box.rename_button.addEvent('click', (function(e) { new Event(e).stop(); this.renameBox(box); }).bind(this));
	box.delete_button.addEvent('click', (function(e) { new Event(e).stop(); this.deleteBox(box); }).bind(this));
	var bgpos;
	box.label.addEvents({
	    'mouseover': function() {
		bgpos = box.label.getStyle('background-position').split(" ");
		box.label.set('styles', { 'background-position': '0 ' + (bgpos[1].substring(0,bgpos[1].length-2) - 20) + 'px' });
	    },
	    'mouseleave': function() {
		bgpos = box.label.getStyle('background-position').split(" ");
		box.label.set('styles', { 'background-position': '0 ' + (parseInt(bgpos[1].substring(0,bgpos[1].length-2)) + 20) + 'px' });
	    },
	    'click': (function() {
		if (box.items.open) {
		    box.items.set('styles', {'display': 'none'});
		    box.label.set('styles', {'background-position': '0 -20px'});
		}
		else {
		    box.items.set('styles', {'display': 'block'});
		    box.label.set('styles', {'background-position': '0 -60px'});
		}
		box.items.open = !box.items.open;
		box.toggleClass('opened');
		this.resize();
	    }).bind(this)
	});

	box.inject(this._body);
	box.items.inject(box, 'after');

	if (dynamic)
	    this.resize();

	return box;
    },

    deleteBox: function(box) {
	Sexy.confirm("<h1>Are you sure?</h1><p>Are you sure you want to delete: " + box.label_text + "</p>", {
	    onComplete: function(response) {
		if (!response) { return false; }
		box.addClass('loading');
		new Request.JSON({
		    url: '/cake/box/delete/',
		    onComplete: (function(response) {
			if (response.succeed) {
			    box.items.destroy();
			    box.destroy();
			    
			    if (this._body.getChildren('li').length == 0)
				this.emptyCake();
			    this.resize();
			} else
			    box.removeClass('loading');
		    }).bind(this)
		}).post({'label': box.label_text});
	    }
	});
    },

    renameBox: function(box) {
	if ($('rename-box'))
	    $('rename-box').destroy();
	var renameBlock = new Element('div', {'id': 'rename-box'});
	var form = new Element('form').inject(renameBlock);
	var newLabel = new Element('input', {'type': 'text', 'value': box.label_text, 'class': 'rename-input'}).inject(form);
	var save = new Element('input', {'type': 'button', 'value': 'Save'}).inject(form);
	var cancel = new Element('input', {'type': 'button', 'value': 'Cancel'}).inject(form);

	save.addEvent('click', function() {
	    newLabelText = newLabel.get('value');
	    if (newLabelText == "") {
		newLabel.focus();
		return false;
	    }
	    renameBlock.addClass('loading');
	    new Request.JSON({
		url: '/cake/box/update/',
		onComplete: function(response) {
		    if (response.succeed) {
			box.label_text = newLabelText;
			box.label.set('text', (newLabelText.length > 25 ? newLabelText.substr(0,25) + ' ...' : newLabelText));
		    } else {
			if (response.error != "") alert(response.error);
		    }
		    renameBlock.destroy();
		}
	    }).post({'new_label': newLabelText, 'old_label': box.label_text});
	});
	cancel.addEvent('click', function() { renameBlock.destroy(); });

	var boxPosition = box.rename_button.getPosition();
	renameBlock.set('styles', {
	    'position': 'absolute',
	    'top': boxPosition.y,
	    'left': boxPosition.x - 165,
	    'z-index': 9
	});
	newLabel.focus();
	renameBlock.inject(document.body);
    },

    addItem: function(item_dict, box, dynamic) {
	if ($type(box) == 'string')
	    box = this.lookUpBox(box);
	if (!$defined(box))
	    return false;

	if (box._empty)
	    box.items.getChildren('li').destroy();

	var item = new Element('li');
	new Element('a', {'class': 'box-item', 'href': item_dict.url, 'title': item_dict.name, 'text': item_dict.name}).inject(item);
	var item_options = new Element('span', {'class': 'box-item-options'}).inject(item);

	//item.watch_button  = new Element('a', {'class': 'box-item-watch box-button', 'href': '#', 'title': (item_dict.watch ? 'un' : '') + 'watch this item'}).inject(item_options);
	item.delete_button = new Element('a', {'class': 'box-item-delete box-button delete-simple', 'href': '#', 'title': 'delete this item'}).inject(item_options);

	new Element('br', {'class': 'clear'}).inject(item);

	item.i_id   = item_dict.id;
	item.i_name = item_dict.name;
	//item.watch  = item_dict.watch;
	item.box    = box;
	item.inject(box.items);

	// event setting
	//item.watch_button.addEvent('click', (function(e) { new Event(e).stop(); this.watchItem(item); }).bind(this));
	item.delete_button.addEvent('click', (function(e) { new Event(e).stop(); this.deleteItem(item); }).bind(this));

	if (dynamic)
	    this.resize();

	return item;
    },

    deleteItem: function(item) {
	item.addClass('loading');
	var box = item.box;
	new Request.JSON({
	    url: '/cake/box/delete_item/',
	    onComplete: (function(response) {
		if (response.succeed) {
		    item.destroy();
		}
		else
		    item.removeClass('loading');

		if (box.items.getChildren('li').length == 0)
		    this.emptyBox(box);
		this.resize();
	    }).bind(this)
	}).post({'id': item.i_id});
    },

    watchItem: function(item) {
	item.watch = !item.watch;
	item.addClass('loading');
	new Request.JSON({
	    url: '/cake/box/update_item/',
	    onComplete: function(response) {
		if (response.succeed) {
		    // set the apropiate class
		    if (item.watch) {
			item.watch_button.removeClass('dont-watch');
			item.watch_button.addClass('do-watch');
		    }
		    else {
			item.watch_button.removeClass('do-watch');
			item.watch_button.addClass('dont-watch');
		    }
		    item.watch_button.set('title', (item.watch ? 'un' : '') + 'watch this item');
		}
	    }
	}).post({'id': item.i_id, 'watch': item.watch});
	item.removeClass('loading');
    },

    lookUpBox: function(box_label) {
	var the_box = this._body.getChildren('li').filter(function(box) {
	    return box.label_text == box_label;
	});
	if (the_box.length != 1) return null;
	return the_box[0];
    },

    emptyCake: function() {
	new Element('li', {'class': 'no-children', 'text': 'No boxes for you... =('}).inject(this._body);
	this._empty = true;
    },

    emptyBox: function(box) {
	new Element('li', {'class': 'no-children', 'text': 'This box has no items'}).inject(box.items);
	box._empty = true;
    }
});