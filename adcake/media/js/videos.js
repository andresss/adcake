window.addEvent('domready', function() {
    if (!$('add-video'))
	return false;
    // Stablish a uuid for the posible connection with nginx
    var uuid = ""
    for (i = 0; i < 32; i++) {
	uuid += Math.floor(Math.random() * 16).toString(16);
    }

    var form = $('add-video').addEvent('submit', function(e) {
	form.getElement('div.buttonHolder').empty()
	form.getElement('div.buttonHolder').set('styles', {'text-align': 'left'});
	var bar = new Element('div', {'id': 'upload-progress-bar'}).inject(form.getElement('div.buttonHolder'));
// 	var box = new Element('div', {'id': 'box'}).inject(bar);
// 	new Element('div', {'id': 'perc'}).inject(box);
	new Element('span',  {'id': 'upload-text', 'text': 'Starting upload...'}).inject(form.getElement('div.buttonHolder'));

	filename = $("id_media").get('value').split(/[\/\\]/).pop();
	form.set('action', '?X-Progress-ID=' + uuid);

	// Creates the dwProgressBar
	var pb = new dwProgressBar({
	    container: $('upload-progress-bar'),
	    startPercentage: 1,
	    speed:1000,
	    boxID: 'box',
	    percentageID: 'perc'
	});

// 	// Create the request
// 	var req = new Request({
// 	    method: 'get',
// 	    headers: {'X-Progress-ID': uuid},
// 	    url: '/upload/progress',
// 	    initialDelay: 500,
// 	    delay: 1000,
// 	    limit: 10000,
// 	    onSuccess: function(reply) {
// 		test = JSON.decode(reply);
// 		switch(test.state) {
// 		case "uploading":
// 		    percent = 0.00 + parseFloat(Math.floor((test.received / test.size)*1000)/10);
// 		    $('upload-text').set('html','Uploading ' + filename + ' ... ' + percent + '%');
// 		    pb.set(percent);
// 		    break;
// 		case "starting":
// 		    $('upload-text').set('html','Starting Upload... ');
// 		    break;
// 		case "error":
// 		    $('upload-text').set('html','Upload Error... ' + test.status);
// 		    break;
// 		case "done":
// 		    $('upload-text').set('html','Upload Finished...');
// 		    req.stopTimer();
// 		    break;
// 		default:
// 		    console.debug("Oooops!");
// 		    break;
// 		}
// 	    },
// 	});
	filename = $("id_media").get('value').split(/[\/\\]/).pop();
	form.set('action', '?X-Progress-ID=' + uuid);
	//e.stop();

// 	req.startTimer('X-Progress-ID=' + uuid);
// 	//e.stop()
    });
});
