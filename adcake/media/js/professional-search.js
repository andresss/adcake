var Professionals = {
    start: function() {
	$$('#add-search span').addEvent('click', function(event){
	    var another = $('search').getElement('fieldset').clone();
	    another.getElements('input').each(function(el){ el.set('value', ''); });
            another.inject($('search').getElement('.buttonHolder'), 'before');
            another.getElement('.remove').addEvent('click',function(event){
		el = another.getElement('.remove');
		if (el.getParent().getParent().getChildren().length > 4){
		    el.getParent().destroy();
		}else{
		    Site.notifications.send('Removing the last search'
					    + 'box is not allowed.');
		}
            });
        });

	$$('.remove').each(function(el){
            el.addEvent('click', function(event){
		if (el.getParent().getParent().getChildren().length > 4){
		    el.getParent().destroy();
		}else{
		    Site.notifications.send('Removing the last search'
					    + 'box is not allowed.');
		}
            });
	});
    }
};

window.addEvent('domready', Professionals.start);