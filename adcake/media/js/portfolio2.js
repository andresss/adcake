var PortfolioE = {
 start: function() {
	if ($('video-list'))
	    PortfolioE.Details.start();
 }
};
PortfolioE.Details = {
    start: function(){
	PortfolioE.Details.userAdministration();
    },

    userAdministration: function(){
	$$('a.delete-video').each(function(trigger) {
	    trigger.addEvent('click', function(e) {
		new Event(e).stop();
		if (!confirm('Are you sure you want to delete this video?'))
		    return false;
		var pieces = trigger.get('href').split('#');
		var video_id = pieces[1].toInt();
		new Request.JSON({
		    url:'/portfolio/video/delete/',
		    onComplete: function(jsonResponse) {
			if (jsonResponse.succeed) {
			    trigger.getParent('li').destroy();
			} else {
			    alert('Oops! there was an error deleting this video');
			}
		    }
		}).post({'video_id': video_id});
	    });
	});
	$$('a.delete-gallery').each(function(trigger) {
	    trigger.addEvent('click', function(e) {
		new Event(e).stop();
		if (!confirm('Are you sure you want to delete this gallery and all his images?'))
		    return false;
		var pieces = trigger.get('href').split('#');
		var gallery_id = pieces[1].toInt();
		new Request.JSON({
		    url:'/portfolio/gallery/delete/',
		    onComplete: function(jsonResponse) {
			if (jsonResponse.succeed) {
			    trigger.getParent('li').destroy();
			} else {
			    alert('Oops! there was an error deleting this video');
			}
		    }
		}).post({'gallery_id': gallery_id});
	    });
	});
    }
};
window.addEvent('load', PortfolioE.start);