Element.implement({
	fadeAndDestroy: function(duration) {
		duration = duration || 600;
		this.set('tween', {
			duration: duration,
			onComplete: this.destroy.bind(this)
		});
		this.fade('out');
	}
});
function change_role(username,role_id,action){
    var request_dict = {
        'role_id':role_id,
        'action':action
    };
    new Request.JSON({
        url:'/multimedia/change_role/',
        onComplete:(function(response){
                   if ( $defined(response.errors) ){
                     return false;
                   }
                   $(username+'-'+action+'-'+role_id).getParent('tr').fadeAndDestroy();
                   if (action == 'accept')
                      not.alert(username+" has been added to your work.", {'className': 'success','highlight': false,'hideAfter': 2000});
                   else
                      not.alert(username+" has been declined to be part of your work.", {'className': 'error','highlight': false,'hideAfter': 2000});
                   var prt=$('pending-roles-t');
                   if (prt.getElements('.pending-role-e').length == 2){
                       (function(){
                        prt.getParent().fadeAndDestroy()
                       }).delay(600, this);
                   }
                   return true;
        }).bind(this)
    }).post(request_dict)
}
var PendingRole = {
    start: function() {
    not = new Purr({'mode':'top','position': 'center'});
    $$(".pending-role-e").each(function (e) {
        e.addEvent("click", function () {
            e_id=e.get('id').split('-');
            username = e_id[0];
            action = e_id[1];
            id = e_id[2];
            change_role(username,id,action);
        });
    });
    }
}
window.addEvent('domready', PendingRole.start);