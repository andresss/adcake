window.addEvent('domready', function() {
    var togglers, elements;

    if ($('company-list')) {
	togglers = $$('li.company-toggler');
	elements = $$('div.company-detail');

	var accordion = new Accordion(togglers, elements, { alwaysHide: true, display: -1 });
    }

    if ($('category-list')) {
	togglers = $$('span.view-companies');
	elements = $$('ul.category-company-list');

	var accordion = new Accordion(togglers, elements, {
	    alwaysHide: true,
	    display: -1,
	    onActive: function(toggler, element) {
		toggler.addClass('viewer-selected');
		element.set('styles', {
		    'overflow': 'auto',
		    'border-top': '1px solid #E6E6E6',
		    'border-bottom': '1px solid #E6E6E6'
		});
	    },
	    onBackground: function(toggler, element) {
		toggler.removeClass('viewer-selected');
		element.set('styles', {
		    'overflow': 'hidden',
		    'border-top': 'medium none',
		    'border-bottom': 'medium none'
		});
	    }
	});
    }

    if ($('company-admin')) {
	/*
	 * Collapsable Fieldset
	 */
	$$('fieldset.collapsible').each(function(ele){
	    legend = ele.getElement('legend');
	    
	    // The Fieldset must have a legend for this to work
	    if(!legend) { return; }

	    if (ele.getElements('li.error').length != 0)
		ele.removeClass('collapsed');
	    
	    legend.addEvent('click', function(e){
		e.stop(); // stop the click event
		// The fieldset is collapsed
		ele.toggleClass('collapsed');
	    });

	});
    }
});
