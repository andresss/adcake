var Profile = {
    start: function() {
	if ($('edit-profile')) {
	    var edit = new EditProfileManager();
	    $('edit-image').addEvent('click', function(e) {
		e.stop();
		edit.load_tab('picture');
	    });
	    $$('#company-ad span.link').addEvent('click', function() { edit.load_tab('companies'); });
	}
    }
};

var EditProfileManager = new Class({
    initialize: function() {
	this._body = $('form-holder');
	this._tabs_names = ['account', 'password', 'picture','companies'];

	this.set_tabs();
	if ($('profile-tabs').get('rel') && this._tabs_names.contains($('profile-tabs').get('rel')))
	    this.load_tab($('profile-tabs').get('rel'));
	else
	    this.load_tab('account');
    },

    set_loading: function() {
	if ($defined(this._adcakeForm))
	    this._adcakeForm = null;
	this._body.empty();
	this._body.addClass('loading');
    },

    unset_loading: function() {
	this._body.removeClass('loading');
    },

    set_tabs: function() {
	this._tabs = {};
	this._tabs_names.each((function(name) {
	    this._tabs[name] = {'url': '/accounts/edit/xhr/' + name + '/', 'element': $(name)};
	    this._tabs[name].element.addEvent('click', (function(e) {
		e.stop();
		if (this._tabs[name].element.hasClass('selected'))
		    return false;
		this.load_tab(name);
		// Does whatever you want when a tab is clicked
	    }).bind(this));
	}).bind(this));
    },

    load_tab: function(name) {
	this.set_loading();

	this._actual_tab = this._tabs[name];
	for (key in this._tabs) {this._tabs[key].element.removeClass('selected')};
	this._actual_tab.element.addClass('selected');
	new Request.HTML({
	    url: this._actual_tab.url,
	    evalScripts: true,
	    onSuccess: (function(html, elements, strHTML, strJS) {
		this._body.adopt(html);
		this._actual_form = this._body.getElement('form');
        if ( name == 'picture' && $('profile_picture') ){
          new MooCrop('profile_picture', {constrainRatio : false,
                                          maskOpacity: '.8',
                                          handleColor: '#D00A14',
                                          handleWidth: '7px',
 	                                      handleHeight: '7px'});
        }
	    this.set_form();
	    }).bind(this)
	}).get();
	this.unset_loading();
    },

    set_form: function() {
	this._adcakeForm = new AdcakeForm({forms: $$(this._actual_form)});
    if (this._actual_form.get('name') == 'skip-ajax') return true;
    this._actual_form.addEvent('submit', (
	    function(e) {
		e.stop();
		new Request.HTML({
		    url: this._actual_tab.url,
		    onSuccess: (function(html, form) {
			this._body.empty();
			this._adcakeForm = null;
			this._body.adopt(html);
			this._actual_form = this._body.getElement('form');
			// Checks if the form is complete.
			if (form[0].hasClass('done')) // complete...
			    Registration.form_save_complete();
			this.set_form();
			this.unset_loading();
		    }).bind(this)
		}).post(this._actual_form);
		this.set_loading();
	    }).bind(this));
    },

    form_save_complete: function() {}
});

window.addEvent('domready', Profile.start);