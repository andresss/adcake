Element.implement({
	injectHTML: function(content, where){
      (new Element('div')).set('html', content).getChildren().inject(this, where);
	  return this;
	}

});

var $elem = function(content){
  return (new Element('span')).set('html', content);
};

var $elems = function(content){
  return $elem(content).getChildren();
};

var AddTriggers = new Class(
  {

    Implements: [Options, Events],

    options: {
	  className: 'add-resume-property-block',
      duration: 500,
      offsets: {x: 8, y: 8},
      type: 'add',
      edit: false
    },

    initialize: function(selector, options) {
	  this.setOptions(options);

	  this.triggers = $$(selector);
	  this.triggers.each(this.parseTrigger.bind(this));
      this.injection = new Element('div', {'class': this.options.className+'-wrapper'});
      this.block_top = new Element('div', {'class': this.options.className+'-top', html:''});
      this.block_bottom = new Element('div', {'class': this.options.className+'-bottom', html:''});
      this._build();
    },

    parseTrigger: function(trigger) {
	  trigger.addEvent('click', (function(event) {
	                               this.showBlock(event, trigger);
	                             }).bind(this));
    },

    showBlock: function(event, trigger) {
      $$('.'+this.options.className+'-wrapper').each(
        function(e){
          e.destroy();
        });
      new Event(event).stop();
	  size = window.getSize(); scroll = window.getScroll();
	  block = this.block.getSize();
	  props = {x: 'left', y: 'top'};
      this.injection.setStyle('position', 'absolute');
	  for (var z in props){
        var pos = event.page[z] + this.options.offsets[z];
	    if ((pos + block[z] - scroll[z]) > size[z]) pos = event.page[z] - this.options.offsets[z] - block[z];
	    this.injection.setStyle(props[z], pos);
	  }

      if (this.options.edit){
        var type = this.options.type;
        var build = function(type){
          var property = function(response) {
            if (!$defined(response)) {
              this.die('Oops, an error ocurred...');
              return false;
            }

            form = this.form('paired');
            block = $$('.add-resume-property-block')[0];
            this.insertSaveCancel(form, 'property');
            form.replaces(block);
	        this.block.trigger = trigger;

            this.property.retrieve('input').set('value',
                                                response.skill__name);
            if ( !response.deletable ){
              this.property.retrieve('input').set('disabled',
                                                  'true');
            }else{
              this.property.retrieve('input').erase('disabled');
            }

            this.value.retrieve('input').set('value',
                                             response.value);
            this.comment.retrieve('textarea').set('value',
                                                  response.comment);
            form.getElements('option').each(function(el){
                                              if (el.get('value')==response.level){
                                                el.set('selected', 'true');
                                              }
                                            });

            return true;
          };
          return property;
        };

        var request = new Request.JSON({
                                         url: '/resume/'+type+'/get/',
	                                     onComplete: build(type).bind(this)
                                       });
        var request_dict = {
          'id': trigger.get('rel')
	    };
        request.post(request_dict);

      }

	  this.injection.fade('hide');
      this.injection.adopt([
                             this.block_top,
                             this.block,
                             this.block_bottom
                           ]);
	  this.injection.inject(document.body);
      if ( $defined($('add-paired-selected')) ){
        $('add-paired-selected').addEvent('click',
        (function(){
          form = this.form('paired');
          block = $$('.add-resume-property-block')[0];
          this.insertSaveCancel(form, 'property');
          form.replaces(block);
	      this.block.trigger = trigger;
        }).bind(this));
      }
      if ( $defined($('add-skill-selected')) ){
        $('add-skill-selected').addEvent('click', (function(){
          form = this.form('unpaired');
          block = $$('.add-resume-property-block')[0];
          this.insertSaveCancel(form, 'skill');
          form.replaces(block);
          this.block.trigger = trigger;
          }).bind(this));
      }
      if ( !$defined($('add-skill-selected')) && !$defined($('add-paired-selected')) ){
        form = this.form('subcategory');
        block = $$('.add-resume-property-block')[0];
        this.insertSaveCancel(form, 'subcategory');
        form.replaces(block);
        this.block.trigger = trigger;
      }

      this.block.fade('in');
 	  this.injection.fade('in');
    },

    insertCancel: function(){
      var inputContainer = new Element('div', {'class': 'input-container'}).inject(this.block);
	  new Element('input', {
                    'type': 'button',
                    'value': 'Cancel',
	                events: {'click': this._unbuild.bind(this)}
	                }).inject(inputContainer);

      new Element('br', {'class':'clear'}).inject(this.block);
    },

    insertSaveCancel: function(form, type){
      var inputContainer = new Element('div', {'class': 'input-container'}).inject(form);
	  new Element('input', {
                    'type': 'button',
	                'value': 'Save',
	                events: {'click': this.add(type).bind(this)}
	                }).inject(inputContainer);
	  new Element('input', {
                    'type': 'button',
                    'value': 'Cancel',
	                events: {'click': this._unbuild.bind(this)}
	                }).inject(inputContainer);

      new Element('br', {'class':'clear'}).inject(form);
    },

    form: function(type){
      var el = false;
      if (type == 'paired'){
        el = $elems('<div class="add-resume-property-block">'
         + '<h2>paired</h2>'
         + '<form>'
         + '<label for="property">Name:</label>'
         + '<input name="name"/>'
         + '<label for="value">Value:</label>'
         + '<input name="value"/>'
         + '<label for="level">Level:</label>'
         + '<select name="level">'
         + '<option value="0">N/A</option>'
         + '<option value="1">minimal</option>'
         + '<option value="2">basic</option>'
         + '<option value="3">intermediate</option>'
         + '<option value="4">advanced</option>'
         + '<option value="5">expert</option>'
         + '</select>'
         + '<br class="clear"/>'
         + '<label for="comment">Comment:</label>'
         + '<textarea name="comment"></textarea>'
         + '</form>'
         + '<br class="clear"/>'
         + '</div>')[0];
      }else if (type == 'unpaired'){
        el = $elems('<div class="add-resume-property-block">'
         + '<h2>unpaired</h2>'
         + '<form>'
         + '<label for="property">Skill:</label>'
         + '<input name="skill"/>'
         + '<label for="level">Level:</label>'
         + '<select name="level">'
         + '<option value="0">N/A</option>'
         + '<option value="1">minimal</option>'
         + '<option value="2">basic</option>'
         + '<option value="3">intermediate</option>'
         + '<option value="4">advanced</option>'
         + '<option value="5">expert</option>'
         + '</select>'
         + '<br class="clear"/>'
         + '<label for="comment">Comment:</label>'
         + '<textarea name="comment"></textarea>'
         + '</form>'
         + '<br class="clear"/>'
         + '</div>')[0];
      }else if(type=='subcategory'){
        el = $elems('<div class="add-resume-property-block">'
                    + '<h3>Add a Subcategory</h3>'
                    + '<form>'
                    + '<label for="property">Title:</label>'
                    + '<input name="title"/>'
                    + '</form>'
                    + '</div>')[0];
      }
      var inputs = el.getElements('input');
      var labels = el.getElements('label');
      this.property = labels[0];
      this.property.store('input', inputs[0]);
      var i = 1;
      if (type == 'paired'){
        this.value = labels[1];
        this.value.store('input', inputs[1]);
        i += 1;
      }
      if (type!='subcategory'){
        this.level = labels[i];
        this.level.store('select', el.getElement('select'));
        this.comment = labels[i+1];
        this.comment.store('textarea', el.getElement('textarea'));
      }

      return el;
    },

    _build: function() {
      $$('.'+this.options.className+'-wrapper').each(
        function(e){
          e.destroy();
        });

      this.block = new Element('div', {'class': this.options.className});
      this.block.setStyle('padding-bottom', '10px');

      if ( this.options.type == 'add' ){
        this.block.injectHTML('<div class="add-resume-property-block">'
                              + '<h2>Choose a Style</h2>'
                              + '<a id="add-paired-selected">uno</a>'
                              + '<a id="add-skill-selected>el otro</a>'
                              + '</div>');
        this.insertCancel();
	    // Set the loading state
	    this.overMessage = new Element('div', {
                                         'class': 'overall loading',
	                                     'styles': {'visibility': 'hidden'}
	                                   }).inject(this.block);
	    new Element('span').inject(this.overMessage);
        return false;
      }
        this.block.injectHTML();
    },

    _unbuild: function() {
	  this.block.fade('out');
	  this.injection.fade('out');
    },

    add: function(type){
      return function(submit){
        if ($defined(submit))
          new Event(submit).stop();
          if (type == 'property' || type == 'skill'){
	        var request_dict = {
	          'name': this.property.retrieve('input').get('value'),
	          'parent': this.block.trigger.get('rel'),
              'level': this.level.retrieve('select').get('value'),
              'comment': this.comment.retrieve('textarea').get('value')
	        };
            if (type=='property'){
              request_dict['value'] = this.value.retrieve('input').get('value');
            }
          }else if (type=='description'){
          	var request_dict = {
	          'description': this.property.retrieve('textarea').get('value'),
	          'parent': this.block.trigger.get('rel')
	        };
          }else if (type=='subcategory'){
          	var request_dict = {
	          'title': this.property.retrieve('input').get('value'),
	          'parent': this.block.trigger.get('rel'),
	          'public': true
	        };
          }
          if (!this.options.edit){
            var method = 'add';
          }else{
            var method = 'edit';
            request_dict['id'] = this.block.trigger.get('rel');
          }

	      new Request.JSON({
	        url: '/resume/'+type+'/'+method+'/',
	        onComplete: (function(response) {
		                   if (!$defined(response)) {
		                     this.die('Oops, an error ocurred...');
		                     return false;
		                   }
                           if ( type == 'description' ){
                             this.addDescription(response.id,
                                                 response.description);
                             return true;
                           }
                           if ( type == 'subcategory' ){
                             this.addSubcategory(response.id,
                                                 response.parent,
                                                 response.title);
                             new nicEditor().panelInstance("id_description_"+response.id);
                             var not = new Purr({'mode':'top','position': 'center'});
                             not.alert('You have successfully created a new Resume Category', {'className': 'success','highlight': false,'hideAfter': 2000});
                             return true;
                           }

                           if (!this.options.edit){
                             this.addCreated(this.block.trigger.get('rel'),
                                             response);
                           }else{
                             this.editProperty(this.block.trigger.get('rel'),
                                               response);
                           }
                           return true;
	                     }).bind(this)
	        }).post(request_dict);
	      this._unbuild();
      };
    },

    addCreated: function(rel, response){
      if ($defined(response.value)){
        var elem = new Element('li', {'class':'property',
                                     'id': 'skill-'+response.id,
                                     'html':'<b>'+response.name+': </b>'+response.value})
        new Element('a', {'class':'delete-property',
                          'rel': response.id,
                          'html':'(delete)'}).inject(elem);
        elem.inject($('category-' + rel).getElement('.paired-values'));
      }else{
        var elem = new Element('li', {'class':'skill-list', 'id': 'skill-'+response.id, 'html':response.name+' &nbsp;'})
        new Element('span', {'class':'delete-skill',
                          'rel': response.id,
                          'html':'(delete)'}).inject(elem);
        elem.inject($('category-' + rel).getElement('.skill'));
      };

      var tipz = new Element('div', {'class':'tipz', 'html':''});
      var ul = new Element('ul', {'class':'tipz'});
      for (i in response){
        if (['id'].contains(i))
          continue;
        if(i=='level' && response[i]==0)
          continue;
        new Element('li', {html:(i+': '+response[i])}).inject(ul);
      }
      ul.inject(tipz);
      new Element('div', {'id':'skill-'+response.id+'-tooltip'}).adopt(
        [
          new Element('div', {'class':'tipz-top', 'html':''}),
          tipz,
          new Element('div', {'class':'tipz-bottom', 'html':''})
        ]
      ).inject($('all-tips'));
      new Tips('skill-' + response.id, 'skill-'+response.id+'-tooltip', {className: 'tipz'});
      window.Site.resume();
    },

    addDescription: function(rel, description){
      new Element('div', {'class':'description', 'html': description}).replaces(
        $('category' + rel).getElement('.description')
      );
    },

    addSubcategory: function(rel, parent, title){
      if ( parent == 'none' ){
        var elem = new Element('div', {'id':'category-'+rel,'class':'resume-categories'}).adopt(
          new Element('div', {'class': 'category-header'}).adopt(
            new Element('h2', {'html': title, 'class': 'category-title','style':'border-bottom:0'}),
            new Element('span', {'class': 'sortable-handler'}),
            new Element('br', {'class': 'clear'})
          ),
          new Element('div',{'class':'category-description bulk-edit','id':'category-description-'+rel}).adopt(
                  new Element('textarea',{'class':'simple edit','id':'id_description_'+rel})
          ),
          new Element('div',{'class':'category-content'}).adopt(
                  new Element('div',{'class':'buttons-holder buttons'}).adopt(
                          new Element('span',{'html':'Delete','class':'delete-category','rel':rel})
                  )
          )
        );
      }else{
        var elem = new Element('li', {'id':'category-'+rel, 'class': 'buttons-holder buttons'}).adopt(
          new Element('div', {'class': 'subcategory-header'}).adopt(
            new Element('h3', {'html': title, 'class':'category-title'}),
            new Element('span', {'class': 'subcategory-edit-description collapsable-trigger', 'rel':'subcategory-description-'+rel, 'html': '(edit description)'}),
            new Element('br', {'class': 'clear'})
          ),
          $elem('<div class="subcategory-buttons">'
          + '<span class="subcategory-delete" rel="'+rel+'" title="Delete this category">Delete this category</span>'
          + '<span class="add-property" rel="'+rel+'" title="Add new info">Add new info</span>'
          + '<br class="clear" />'
          + '</div>'),
          $elems('<div class="hide subcategory-description-edit" id="subcategory-description-'+rel+'">'
          + '<textarea class="simple empty"></textarea>'
          + '</div>'),
          $elems('<ul class="paired-values paired"></ul>'),
          $elems('<ul class="skills"></ul>')
        );
      }


      if (parent == 'none')
        $('categories').adopt(elem);
      else
        $('category-' + parent).getElement('.category-content').adopt(elem);

	  this._unbuild();
      window.Site.resume();
    },

    editProperty: function(rel, response){
      new Element('strong', {'html': response.name+':'}).replaces(
        $('skill-'+rel).getElement('strong')
      );
      $('skill-'+rel+'-tooltip').getElement('span.name').set('html', response.name);

      new Element('span', {'class': 'value', 'html': response.value}).replaces(
        $('skill-'+rel).getElement('.value')
      );
      $('skill-'+rel+'-tooltip').getElement('span.value').set('html', response.value);
      $('skill-'+rel+'-tooltip').getElement('span.comment').set('html', response.comment);

      //Optional property
      if (response.level != 0 && response.had_level)
        $('skill-'+rel+'-tooltip').getElement('span.level').set('html', response.level);
      else if (response.level != 0){
        $('skill-'+rel+'-tooltip').getElement('ul').adopt(
          new Element('li', {'html':'Level: '}).adopt(
            new Element('span', {'html': response.level, 'class': 'level'})));
      }else if (response.level == 0 && response.had_level){
        $('skill-'+rel+'-tooltip').getElement('span.level').getParent().destroy();
      }

    },

    die: function(errMsg) {
	this.overMessage.removeClass('loading').addClass('error').setStyle('visibility', 'visible').getElement('span').set('text', errMsg);
	this.injection.fade.pass('out',this.injection).delay(4000);
    }


  });

var DeleteTriggers = new Class(
  {

    Implements: [Options, Events],

    options: {
	  className: 'delete-property',
      duration: 500,
      offsets: {x: 8, y: 8},
      type: 'property'
    },

    initialize: function(selector, options) {
	  this.setOptions(options);

	  this.triggers = $$(selector);
	  this.triggers.each(this.parseTrigger.bind(this));
    },

    parseTrigger: function(trigger) {
	  trigger.addEvent('click', (function(event) {
	                               this.remove(event, trigger);
	                             }).bind(this));
    },

    remove: function(event, trigger){
      var request_dict = {
	    'id': trigger.get('rel')
	  };

	  new Request.JSON({
	    url: '/resume/'+this.options.type+'/delete/',
	    onComplete: (function(response) {
		               if (!$defined(response)) {
		                 this.die('Oops, an error ocurred...');
		                 return false;
		               }
                       this.removeDeleted(trigger.get('rel'), response.status);
                       return true;
                     }).bind(this)
	        }).post(request_dict);
    },

    removeDeleted: function(rel, status){
      if (status == 'not deleted')
        alert('the element you tried to delete is not deletable'); //This should never occour
      else
        if (this.options.type == 'property' || this.options.type == 'skill'){
          $('skill-'+rel).destroy();
          $('skill-'+rel+'-tooltip').destroy();
        }else if (this.options.type == 'category'){
          $('category-'+rel).destroy();
        }
    },

    die: function(errMsg) {}

  });

var Tips = new Class({

  Implements: [Events, Options],

  options: {
    onShow: function(tip){
      tip.setStyle('visibility', 'visible');
    },
    onHide: function(tip){
      tip.setStyle('visibility', 'hidden');
    },

    showDelay: 100,
    hideDelay: 100,
    className: null,
    offsets: {x: 8, y: 8},
    fixed: false
  },

  initialize: function(trigger, selector, options){
	this.setOptions(options);
    this.tip = $(selector);
    this.trigger = $(trigger);
    this.tip.setStyles({position: 'absolute', top: 0, left: 0, visibility: 'hidden'});

    var enter = this.elementEnter.bindWithEvent(this, trigger);
    var leave = this.elementLeave.bindWithEvent(this, trigger);
    var move = this.elementMove.bindWithEvent(this, trigger);
    this.trigger.addEvents({mouseenter: enter, mouseleave: leave,
                            mousemove: move});

  },

  elementEnter: function(event, element){

    this.timer = $clear(this.timer);
    this.timer = this.show.delay(this.options.showDelay, this);

    this.position((!this.options.fixed) ? event : {page: element.getPosition()});
  },

  elementLeave: function(event){
    $clear(this.timer);
    this.timer = this.hide.delay(this.options.hideDelay, this);
  },

  elementMove: function(event){
    this.position(event);
  },

  position: function(event){
	size = window.getSize(); scroll = window.getScroll();
	var tip = this.tip.getSize();
	props = {x: 'left', y: 'top'};
	for (var z in props){
        //alert(''+event.page[z]+''+' '+this.options.offsets[z]+' '+tip[z]);
        var pos = event.page[z] + this.options.offsets[z];
	    if ((pos + tip[z] - scroll[z]) > size[z]) pos = event.page[z] - this.options.offsets[z] - tip[z];
	    this.tip.setStyle(props[z], pos);
	}

//     var size = window.getSize(), scroll = window.getScroll();
//     var tip = {x: this.tip.offsetWidth, y: this.tip.offsetHeight};
//     var props = {x: 'left', y: 'top'};
//     for (var z in props){
//       //alert(''+event.page[z]+''+' '+this.options.offsets[z]+' '+tip[z]);
//       var pos = event.page[z] + this.options.offsets[z] - tip[z];
//       if ((pos + tip[z] - scroll[z]) > size[z]) pos = event.page[z] - this.options.offsets[z] - tip[z];
//       this.tip.setStyle(props[z], event.page[z] - scroll[z]);
//     }
  },

  show: function(){
    this.fireEvent('show', this.tip);
  },

  hide: function(){
    this.fireEvent('hide', this.tip);
  }

  });

var ResumeBoxes = new Class ({
  initialize: function() {
    this._body = $('resume');
	new AddTriggers('span.add-property', {'offsets': {x:0,y:0}});

	new AddTriggers('.add-subcategory',{'offsets': {x:0,y:0},
                                             'type':'subcategory'});

	new AddTriggers('.edit', {'offsets': {x:0,y:0},
                                           'type':'property',
                                           'edit':true});

	new DeleteTriggers('.delete',{'offsets': {x:0,y:0},
                                  'type':'property'});
	new DeleteTriggers('a.delete-skill',{'offsets': {x:0,y:0},
                                         'type':'skill'});
	new DeleteTriggers('span.delete-category',{'offsets': {x:0,y:0},
                                               'type':'category'});
	new DeleteTriggers('span.subcategory-delete',{'offsets': {x:0,y:0},
                                               'type':'category'});

    $$('.paired-value').each( function(skill){
      var id = skill.get('id');
      new Tips(id, id+'-tooltip', {className: 'tipz'});
    });

    $$('.skill').each( function(skill){
      var id = skill.get('id');
      new Tips(id, id+'-tooltip');
    });

    var save = function(el){
      var area = el.getElement('textarea');
      var request = new Request.JSON({
          url: '/resume/description/add/'
      });
      var request_dict = {
          'parent': el.get('id').split('-')[2],
          'description': nicEditors.findEditor('id_description_'+el.get('id').split('-')[2]).getContent()
        };
      request.post(request_dict);
    };

   $('save-resume').addEvent('click',function(){
       $$('.category-description').each(save);
       var not = new Purr({'mode':'top','position': 'center'});
       not.alert('Your Resume have been successfully saved.', {'className': 'success','highlight': false,'hideAfter': 2000});
   });
//    $$('.category-description').each(save);
    $$('.subcategory-description-edit').each(save);
    $$('.profile-edit').each(function (elem){
                               elem.addEvent('click', function(e){
                                               e.stop();
                                               window.location = '/profile/?';
                                             });
                             });
  }

});
