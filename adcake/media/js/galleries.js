var Gallery = {
    start: function() {
	if ($('gallery-thumbs'))
	    Gallery.Details.start();
	if ($('image-content'))
	    Gallery.Image.start();
	if ($('confirm-delete'))
	    Gallery.Delete.set();
	// If there's a message, delete it after 8 seconds
	if ($('gallery-deleted'))
	    (function() { $('gallery-deleted').destroy() }).delay(8000);
    }
};

Gallery.Details = {
    start: function() {
	// Set up the user administration panel
	Gallery.Details.userAdministration();
	// Set up the image list preview
    },

    userAdministration: function() {
	/*
	if (!$('user-administration'))
	    return false;
	Gallery.Details.administration = $('user-administration');
	Gallery.Details.id = Gallery.Details.administration.getElement('form').get('id').replace('gallery-', '').toInt();
	
	var properties = Gallery.Details.administration.getElements('input')
	var publish = properties[0];
	var featured = properties[1]; 
	publish.addEvent('click', function() {
	    request_dict = {'gallery_id': Gallery.Details.id, 'publish': publish.checked };

	    if (!publish.checked && featured.checked) {
		featured.checked = false;
		request_dict[featured.name] = featured.checked;
	    }
	    
	    new Request.JSON({
		url:'/portfolio/gallery_detail/administration/',
	    }).post(request_dict);
	});

	featured.addEvent('click', function() {
	    request_dict = {'gallery_id': Gallery.Details.id, 'featured': featured.checked };

	    if (!publish.checked && featured.checked) {
		publish.checked = true;
		request_dict[publish.name] = publish.checked;
	    }
	    
	    new Request.JSON({
		url:'/portfolio/gallery_detail/administration/',
	    }).post(request_dict);
	});
*/
	// Delete images.
	$$('a.delete-image').each(function(trigger) {
	    trigger.addEvent('click', function(e) {
		new Event(e).stop();
		if (!confirm('Are you sure you want to delete this image?'))
		    return false;
		var pieces = trigger.get('href').split('#');
		var image_id = pieces[1].toInt();
		new Request.JSON({
		    url:'/portfolio/gallery_detail/delete_image/',
		    onComplete: function(jsonResponse) {
			if (jsonResponse.succeed) {
			    trigger.getParent('li').destroy();
			} else {
			    alert('Oops! there was an error deleting this image');
			}
		    }
		}).post({'image_id': image_id});
	    });
	});
    }
};

Gallery.Image = {
    start: function() {
	if ($('image-zoom'))
	    Gallery.Image.zoomController($('image-zoom'));
    },

    zoomController: function(controller) {
	Gallery.Image.image_holder = controller.getNext('div#image-border').set('morph', {duration: 1000, transition: 'bounce:out'});
	Gallery.Image.zoomout = controller.retrieve('zoomout', Gallery.Image.image_holder.getElement('img'));
	Gallery.Image.original = controller.retrieve('original', Asset.image(Gallery.Image.zoomout.get('longdesc'), {
	    'alt': Gallery.Image.zoomout.get('alt')
	}));
	Gallery.Image.controller = controller.addEvent('click', function(e) {
	    new Event(e).stop();
	    var open = controller.retrieve('open', false);
	    Gallery.Image.toggleImage(open)
	});
    },

    toggleImage: function(open) {
	if (open) {
	    Gallery.Image.loadImage(Gallery.Image.zoomout);
	    Gallery.Image.controller.set('text', 'Zoom in');
	    Gallery.Image.image_holder.addEvent('click', Gallery.Image.toggleImage.pass(false)).setStyle('cursor', 'pointer');
	    Gallery.Image.controller.store('open', false);
	}
	else {
	    Gallery.Image.loadImage(Gallery.Image.original);
	    Gallery.Image.controller.set('text', 'Zoom out');
	    Gallery.Image.image_holder.removeEvent('click').setStyle('cursor', 'default');
	    Gallery.Image.controller.store('open', true);
	}
    },

    loadImage: function(image) {
	Gallery.Image.image_holder.get('morph').start({height: image.getSize().y, width: image.getSize().x}).chain(
	    function() {
		Gallery.Image.image_holder.getElement('img').dispose();
		image.setStyle('opacity', 0);
		image.inject(Gallery.Image.controller,'after');
		image.fade(1);
	    }
	);
	
    }
};

Gallery.Delete = {
    set: function() {
	$('no').addEvent('click', function(e) {
	    new Event(e).stop();
	    history.go(-1);
	});
    }
};

window.addEvent('load', Gallery.start);