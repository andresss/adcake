var Message = {
    start: function() {
	// Pages with messages-header: inbox, outbox, trash
	if ($('messages-header')) Message.messageList();
	// Pages with forms: compose message
	if ($('compose-message')) {
	    new Autocompleter.Request.JSON($('id_recipient'), '/messages/live_search/', {
		'postVar': 'recipient',
		'multiple': true,
		'minLength': 1,
		'selectFirst': false,
	    });
	}
    },

    messageList: function() {
	Message.messages = $$('table.messages tr');

	// Setting 'selecting events'
	Message.messages.each(function(message) {
	    message.input = $(message.id + '-selector');
	    message.input.addEvent('click', function() {
		if (message.input.checked) message.addClass('selected');
		else message.removeClass('selected');
	    });

	    if (message.input.checked) message.addClass('selected');
	});

	// Setting 'select buttons'
	$('select-all').addEvent('click', function(e) { new Event(e).stop(); Message.select('all'); });
	$('select-none').addEvent('click', function(e) { new Event(e).stop(); Message.select('none'); });
	$('select-read').addEvent('click', function(e) { new Event(e).stop(); Message.select('read'); });
	$('select-unread').addEvent('click', function(e) { new Event(e).stop(); Message.select('unread'); });

	// Setting 'action buttons'
	if ($('mark-read'))
	    $('mark-read').addEvent('click', function(e) { new Event(e).stop(); Message.mark('read'); });
	if ($('mark-unread'))
	    $('mark-unread').addEvent('click', function(e) { new Event(e).stop(); Message.mark('unread'); });
	if ($('mark-delete'))
	    $('mark-delete').addEvent('click', function(e) { new Event(e).stop(); Message.mark('delete'); });
	if ($('mark-inbox'))
	    $('mark-inbox').addEvent('click', function(e) { new Event(e).stop(); Message.mark('inbox'); });
    },

    select: function(match) {
	Message.messages.each(function(message) {
	    if (match == 'all')
		Message.selectMessage(message);
	    else if (match == 'read' && !message.hasClass('new'))
		Message.selectMessage(message);
	    else if (match == 'unread' && message.hasClass('new'))
		Message.selectMessage(message);
	    else
		Message.unselectMessage(message);
	});
    },

    selectMessage: function(message) {
	message.addClass('selected');
	message.input.checked = true;
    },

    unselectMessage: function(message) {
	message.removeClass('selected');
	message.input.checked = false;
    },

    mark: function(match) {
	// Collect the messages to mark
	var message_ids = '';
	Message.messages.each(function(message) {
	    if (message.input.checked)
		message_ids += '+' + message.id;
	});
	if (message_ids == '') return false;
	message_ids = message_ids.substring(1);

	var action_url = '/messages/action/' + match + '/';
	// Request to do the action
	new Request.JSON({ url: action_url }).post({'message_ids': message_ids});

	Message.messages.each(function(message) {
	    if (message.input.checked) {
		if (match == 'read') message.removeClass('new');
		else if (match == 'unread') message.addClass('new');
		else message.destroy();
		Message.unselectMessage(message);
	    }
	});
    },
};

window.addEvent('domready', Message.start);