Element.implement({
	fadeAndDestroy: function(duration) {
		duration = duration || 600;
		this.set('tween', {
			duration: duration,
			onComplete: this.destroy.bind(this)
		});
		this.fade('out');
	}
});
function change_role(username,role_id,action){
    var request_dict = {
        'role_id':role_id,
        'action':action
    };
    new Request.JSON({
        url:'/multimedia/change_role/',
        onComplete:(function(response){
                   if ( $defined(response.errors) ){
                     return false;
                   }
                   $(username+'-'+action+'-'+role_id).getParent('tr').fadeAndDestroy();
                   if (action == 'decline')
                      not.alert(username+" has been removed from this work.", {'className': 'error','highlight': false,'hideAfter': 2000});
                   var prt=$('role-list');
                   if (prt.getElements('tr').length == 1){
                       (function(){
                        var emptyelem  = new Element("td", {id:"empty-roles",text: "No credits added yet.."});
                        prt.getElement('tbody').adopt(emptyelem);
                       }).delay(600, this);
                   }
                   return true;
        }).bind(this)
    }).post(request_dict)
}
var ManageRoles = {
    start: function() {
    not = new Purr({'mode':'top','position': 'center'});
    $$(".delete-role").each(function (e) {
        e.addEvent("click", function () {
            e_id=e.get('id').split('-');
            username = e_id[0];
            action = e_id[1];
            id = e_id[2];
            change_role(username,id,action);
        });
    });
    }
}
window.addEvent('domready', ManageRoles.start);