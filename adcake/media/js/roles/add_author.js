var selector = 'add_author_form';

function cleanForm(form) {
	form.getElements('input[type!=hidden], select, textarea').each(function(input) {
	    input.set('value', '');
	});
	form.getElements('li.error').removeClass('error');
	form.getElements('li.errorField').destroy();
	form.getElement('input[type=submit]').set('value', 'Add').disabled = false;
}
function submit_role(id, role, form){
  var request_dict = {
    'id': id,
    'role': role
  };
  new Request.JSON({
	url: '/multimedia/add_author/',
	onComplete: (function(response) {
                   if ( $defined(response.errors) ){
                     var not = new Purr({'mode':'top','position': 'center'});
                     not.alert("The role must have at least 2 or more characters.", {'className': 'error','highlight': false,'hideAfter': 2000});
                     return false;
                   };
                   Site.notifications.send(response.status);
//                    var tr = new Element('tr');
//                    tr.grab(new Element('td', {'html':response.role}));
//                    tr.grab(new Element('td', {'html':response.by}));
//                    tr.grab(new Element('td', {'html':'All videos with '+response.with+' as an author'}));
//                    tr.grab(new Element('td', {'html':'All videos with _ as '+ response.with_as}));
//                   $('author-list').grab(tr);
                   var form = $('add_author_form');
                   cleanForm(form);
                   slider.slide('toggle');
                   return true;

	             }).bind(this)
    }).post(request_dict);
}

function add_trigger(){
  var form = $(selector);
  var id = form.get('rel');
  form.addEvent('submit', function(e){
                    var role = form.getElement('input').get('value');
                    new Event(e).stop();
                    submit_role(id, role, form);
                });
}

var slider = $(selector).set('slide', {});
slider.slide('hide');
$('add_author_slide').addEvent('click', function(e){
	e = new Event(e);
    slider.slide('toggle');
	e.stop();
});
add_trigger();

