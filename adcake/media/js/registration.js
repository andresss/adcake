var Registration = {
    start: function() {
	Registration._body = $('form-holder');
	Registration._url  = '/registration/sign_up/wizard/';
	Registration.set_loading();
	// Loads the initial state of the form
	new Request.HTML({
	    url: Registration._url,
	    evalScripts: true,
	    onSuccess: function(html, elements, strHtml, strJS) {
		Registration._body.adopt(html);
		//Set the size and everything...
		// ...
		Registration.set_form();
	    },
	}).get();
	Registration.unset_loading();
    },

    set_loading: function() {
	var loading = new Element('div', {
	    'id': 'registration-loading',
	    'text': 'Loading...',
	}).setStyles({
	    'height': Registration._body.getSize().y-32,
	    'width': '100%',
	}).inject(Registration._body);
	//Registration._body.setStyle('display', 'none');
    },

    unset_loading: function() {
	$('registration-loading').destroy();
	//Registration._body.setStyle('display', 'block');
    },

    set_form: function() {
	Registration._actual_form = Registration._body.getElement('form');
	Registration._actual_form.addEvent('submit', function(e) {
	    e.stop();
	    Registration.set_loading();

	    new Request.HTML({
		url: Registration._url,
		onSuccess: function(html, form) {
		    Registration._body.empty();
		    Registration._body.adopt(html);
		    // Checks if the form is complete.
		    if (form[0].hasClass('done')) { // complete...
			Registration.complete();
		    } else { // not yet..
			//Set the size and everything...
			// ...
			Registration.set_form();
		    }
		    //Registration.unset_loading();
		},
	    }).post(Registration._actual_form);	    
	});
    },

    complete: function() {},
}

//window.addEvent('domready', Registration.start);