Element.implement({
	fadeAndDestroy: function(duration) {
		duration = duration || 600;
		this.set('tween', {
			duration: duration,
			onComplete: this.destroy.bind(this)
		});
		this.fade('out');
	}
});
var JobE = {
 start: function() {
	if ($('job-list'))
	    JobE.Details.start();
    if ($('apply-job'))
        JobE.Details.applyJob();
 }
};
JobE.Details = {
    start: function(){
	JobE.Details.userAdministration();
    },
    userAdministration: function(){
        $$('span.delete-job').each(function(trigger) {
            trigger.addEvent('click', function(e) {
                new Event(e).stop();
                Sexy.confirm("<h1>Are you sure?</h1><p>Are you sure you want to delete this post?</p>", {
                    onComplete: function(response) {
                        if (!response) { return false;}
                        var pieces = trigger.get('rel').split('#');
                        var job_id = pieces[1].toInt();
                        new Request.JSON({
                            url:'/jobs/delete/',
                            onComplete: function(jsonResponse) {
                            if (jsonResponse.succeed) {
                                trigger.getParent('a.job-list-element').fadeAndDestroy();
                            } else {
                                alert('Oops! there was an error deleting this video');
                            }
                            }
                        }).post({'job_id': job_id});
                    }
                })
            })
        });
    },
    applyJob: function(){
        var apply_job=$('apply-job');
        var apply_fun=function(){
            var job_id = apply_job.get('rel').split('#')[1].toInt();
            new Request.JSON({
                url:'/jobs/apply/',
                onComplete: function(jsonResponse) {
                    if (jsonResponse.succeed) {
                        apply_job.set('text', 'You have applied to this job!');
                        apply_job.set('id', 'applied');
                        apply_job.removeEvent("click",apply_fun);
                    } else {
                        alert("Sorry, something weird happened, try again later.")
                    }
                }
            }).post({'job_id':job_id})
        };
        apply_job.addEvent('click',apply_fun);
    }
};
window.addEvent('load', JobE.start);