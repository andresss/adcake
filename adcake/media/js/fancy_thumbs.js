var fancyThumbs = new Class({
    Implements: [Options, Events],
    options: {
        width: 172,
        height: 96,
        transition: "slideUp",
        shift: 16,
        titleOpacity: 1,
        tween: {
            // transition: "linear:out",
            delay: 300
        }
    },
    initialize: function(selector, options) {
        this.setOptions(options);
        this.elements = document.getElements(selector);
        if (!this.elements.length)
            return; // or throw an exception 
        
        this.elements.each(function(el) {
            this.attachEvents(el);
        }, this);
    },
    attachEvents: function(el) {
        el.getFirst().set("morph", this.options.tween);
        el.getElement("div.fancy-description").setStyle("opacity", this.options.titleOpacity);
        el.addEvents({
            mouseenter: function() {
                this[this.options.transition](el, "open");
            }.bind(this),
            mouseleave: function() {
                this[this.options.transition](el, "close");
            }.bind(this)
        });
    },
    slideUp: function(el, event) {
        var elChild = el.getFirst(), currentMargin = elChild.getStyle("margin-top").toInt();
        elChild.morph({
            "margin-top": event == "open" ? [currentMargin, -this.options.shift] : [currentMargin, 0]
        });
    },
    slideOver: function(el, event) {
        
        var elChild = el.getElement("div.fancy-description"), currentMargin = elChild.getStyle("margin-top").toInt();
        var size = elChild.getSize();
        //elChild.setStyle("position", "absolute");
        elChild.morph({
            "margin-top": event == "open" ? [currentMargin, -size.y] : [currentMargin, 0],
            "opacity": event == "open" ? [0, this.options.titleOpacity] : [0]
        });
    }
});

window.addEvent('load', function() {
new fancyThumbs("div.media-thumb", {
    transition: "slideOver",
    titleOpacity: .9,
    width:172,
    height:96,
    shift: 46
});
});