function startGallery() {
  var mygal = $('myGallery');
  if (mygal){
      $('news-spotlight').set({styles: {visibility:'visible'}});
      var myGallery = new gallery(mygal, {
          timed: true,
          delay: 10000,
          textShowCarousel: 'Spotlight'
      });
  }
}
window.addEvent('domready', startGallery);


