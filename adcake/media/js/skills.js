window.addEvent('domready', function() {
    var togglers, elements;

    if ($('skill-list')) {
	togglers = $$('li.skill-toggler');
	elements = $$('div.skill-detail');
	var accordion = new Accordion(togglers, elements, { alwaysHide: true, display: -1 });
    }
	var accordion = new Accordion(togglers, elements, {
	    alwaysHide: true,
	    display: -1,
	    onActive: function(toggler, element) {
		toggler.addClass('viewer-selected');
		element.set('styles', {
		    'overflow': 'auto'
		});
	    },
	    onBackground: function(toggler, element) {
		toggler.removeClass('viewer-selected');
		element.set('styles', {
		    'overflow': 'hidden',
		    'border-top': 'medium none',
		    'border-bottom': 'medium none'
		});
	    }
	});
    })
