var Portfolio = {
    start: function() {
	Portfolio._body = $('portfolio');
	Portfolio._branches = $('media-branches');

	if (Portfolio._body.hasClass('edit')) Portfolio.startEdit();
	// Collapsable branches
	$$('ul#media-branches li h3').each(function(branch) {
	    var content = branch.getParent().getNext('div.media-content');
	    branch.addEvent('click', function() {
		content.toggleClass('hide');
	    });
	});
    },

    startEdit: function() {

	Portfolio.sortable_branches = new Sortables(Portfolio._branches, {
	    clone: true, opacity: 0.5, handle: 'span.sortable-handler',
	    onComplete: Portfolio.sendBranchesOrder,
	});


	['gallery','video'].each(function(name) {
	    Portfolio.initializeEditBranch(name + '-branch');
	});
	Portfolio.PersonalInformation.start();
	Portfolio.About.start();
    },
        sendBranchesOrder: function() {
	var new_order = Portfolio.sortable_branches.serialize().map(function(id, index) {
	    return {'name': id.replace('-branch', ''), 'index': (index + 1)}
	});

	new Request.JSON({url: '/portfolio/xhr/reorder_branches/',}).post(
	    {'new_order': JSON.encode(new_order)}
	);
    },
    initializeEditBranch: function(branch) {
	var branch_name = branch.replace('-branch','');
	var branch_plural = '';
	if (branch_name == 'video') branch_plural = 'videos';
	else if (branch_name == 'audio') branch_plural = 'audio tracks';
	else branch_plural = 'galleries';

	branch = $(branch).store('name', branch_name);
	branch.store('work-list', $(branch_name + '-list'));
    branch.store('horizontal-media-feed-content','horizontal-media-feed-content');
	branch.store('plural_name', branch_plural);
    branch.store('form', branch.getElement('form'));
/*	branch.store('sortable', new Sortables($(branch_name + '-list'), {
	    clone: true, opacity: 0.5,
	    onComplete: function() {
		var new_order = this.serialize().map(function(id, index) {
		    return {'id': id.replace(branch_name + '-id-', '').toInt(), 'index': (index + 1)}
		});
		new Request.JSON({url: '/portfolio/xhr/' + branch_name + '/reorder/'}).post(
		    {'new_order': JSON.encode(new_order)}
		);
	    }
	}));*/
    /*
	branch.store('search-container', branch.getElement('div.search-and-add-list').set('slide').slide('hide'));
	branch.store('search-list', branch.getElement('ul.search-list'));
	branch.store('added-work-list', branch.getElement('div.add-selected ul'));
	branch.store('added-work-count', branch.getElement('span.select-counter span'));
	branch.getElement('a.search-and-add').addEvent('click', function(e) {
	    new Event(e).stop();
	    var trigger = branch.getElement('a.search-and-add').toggleClass('clicked');
	    if (!branch.retrieve('search-container').get('slide').open) {
		trigger.set('text', 'Close');
		branch.retrieve('search-container').slide('show');
	    }
	    else {
		trigger.set('text', 'Search for ' + branch_plural);
		branch.retrieve('search-container').slide('hide');
	    }
	    //branch.retrieve('search-list').slide('toggle');
	});
    */
    /*
	// Gallery's and video's table delete button
	Portfolio.Helpers.tableDeleteButtons(branch, branch.retrieve('work-list').getElements('table'));

	Portfolio.setSearchList(branch);
	*/
	Portfolio.setForm(branch);
    },
    /*
    setSearchList: function(branch) {
	var search_container = branch.retrieve('search-container');
	var input_list = search_container.getElements('ul.search-list li input');

	if (input_list.length == 0) {
	    return false;
	}
	input_list.each(function(input) {
	    if (input.checked) Portfolio.toggleWork(branch, input.getParent(), 'select');
	    input.getParent().addEvent('click', function() {
		if (input.checked) Portfolio.toggleWork(branch, input.getParent(), 'select');
		else Portfolio.toggleWork(branch, input.getParent(), 'unselect');
	    });
	});

	search_container.getElement('input.select-button').addEvent('click', Portfolio.massSelectWork.pass([branch, 'all']));
	search_container.getElement('input.unselect-button').addEvent('click', Portfolio.massSelectWork.pass([branch, 'none']));

	search_container.getElement('input.add-work').addEvent('click', Portfolio.addToPortfolio.pass(branch));
    },

    massSelectWork: function(branch, mode) {
	branch.retrieve('search-list').getElements('li').each(function(work) {
	    if (work.hasClass('empty'))
		return false;
	    if (mode == 'all')
		Portfolio.toggleWork(branch, work, 'select');
	    else
		Portfolio.toggleWork(branch, work, 'unselect');
	});
    },

    toggleWork: function(branch, work, mode) {
	var work_count = branch.retrieve('added-work-count');
	var num_works = work_count.get('text').split(' ')[0].toInt();
	if (mode == 'select') {
	    work.addClass('selected');
	    work.getElement('input').checked = true;
	    // clone from unfeatured-list and add it to add-selected list
	    if (!branch.retrieve('added-work-list').getElement('li#'+work.getElement('input').value)) {
		new Element('li', {'text': work.getElement('a').get('text'), 'id': work.getElement('input').value}).inject(branch.retrieve('added-work-list'));
		num_works++;
	    }
	} else {
	    work.removeClass('selected');
	    work.getElement('input').checked = false;
	    // delete from added-work-list
	    if (branch.retrieve('added-work-list').getElement('li#'+work.getElement('input').value)) {
		branch.retrieve('added-work-list').getElement('li#'+work.getElement('input').value).destroy();
		num_works--;
	    }
	}
	work_count.set('text', num_works + ' work' + (num_works == 1 ? '' : 's'));
    },

    addToPortfolio: function(branch) {
	// LOAD STAGE
	var add_list = branch.retrieve('added-work-list', branch.getElement('div.add-selected ul'));
	if (add_list.getElements('li').length == 0)
	    return false;
	works = [];
	add_list.getElements('li').each(function(work) {
	    works.include(work.get('id').toInt());
	    // Delete this items
	    work.destroy();
	});
	branch.retrieve('added-work-count').set('text', '0 works');
	new Request.HTML({
	    url: '/portfolio/xhr/' + branch.retrieve('name') + '/add_items/',
	    onSuccess: function(html, new_works) {
		var work_list = branch.retrieve('work-list');
		if (work_list.getElement('li.empty-branch'))
		    work_list.getElement('li.empty-branch').destroy();
		work_list.adopt(html);
		// Add all the new works to their sortable
		new_works = new_works.filter(function(el) { return el.get('tag') == 'li'; });
		branch.retrieve('sortable').addItems(new_works);
		// Set its delete buttons
		Portfolio.Helpers.tableDeleteButtons(branch, new_works.getElements('table'));
	    },
	}).post({'works': JSON.encode(works)});
	var search_list = branch.retrieve('search-list');
	// Delete all the selected (before)unfeatured-work
	search_list.getElements('li').each(function(work) {
	    if (work.getElement('input').checked)
		work.destroy();
	});
	if (search_list.getElements('li').length == 0)
	    new Element('li', {'text': 'You don\'t have any work left on this branch', 'class': 'empty'}).inject(search_list);
	// UNSET LOAD STAGE
    },

    deleteWork: function(branch, work) {
	var branch_list = work.getParent('ul');
	work_id = work.get('id').split('-id-');
	new Request.JSON({
	    url: '/portfolio/xhr/' + work_id[0] + '/delete_item/',
	    onComplete: function(response) {
		if ($defined(response) && response.succeed) {
		    // Re-add the work to search list
		    var search_list = branch.retrieve('search-list');
		    if (search_list.getElements('li').length == 1 && search_list.getElement('li').hasClass('empty'))
			search_list.getElement('li').destroy();
		    var readd = new Element('li').inject(search_list);
		    var input = new Element('input', {'type': 'checkbox', 'value': work_id[1]}).inject(readd).addEvent('click', function() {
			if (input.checked) Portfolio.toggleWork(branch, input.getParent(), 'select');
			else Portfolio.toggleWork(branch, input.getParent(), 'unselect');
		    });
		    readd.adopt(new Element('a', {'href': response.work_url, 'title': response.work_name, 'text': response.work_name}));

		    work.destroy();
		    // If the list gets empty
		    if (branch_list.getElements('li').length == 0) {
			var empty_branch = new Element('li', {'class': 'empty-branch', 'text': 'The ' + work_id[0].capitalize() + ' List is '});
			new Element('strong', {'text': 'empty'}).inject(empty_branch);
			empty_branch.appendText(', this branch ');
			new Element('strong', {'text': 'will be not shown'}).inject(empty_branch);
			empty_branch.appendText(' in your Portfolio View.').inject(branch_list);
		    }
		}
		else {} // Error message. TO DO
	    }
	}).post({'id': work_id[1]});
    },
    */
    setForm: function(branch) {
    branch.store('form', branch.getElement('form'));
	var form = branch.retrieve('form');

	form.addEvent('submit', function(e) {
	    form.getElement('input[type=submit]').disabled = true;
	});
    },

    recieveFromForm: function(response, branch) {
	response = new Element(response);
    var hcontent = response.innerHTML;
    var helem = Elements.from(hcontent);
    branch = $(branch);
	if (helem.get('tag') == 'li') {
	    var work_list = branch.retrieve('work-list');
        var horizontal_media = branch.retrieve('horizontal-media-feed-content');
	    if (work_list.getElement('li.empty-branch')) work_list.getElement('li.empty-branch').destroy();
	    //response.inject(work_list);
        //$('gallery-list').innerHTML = response;
        work_list.adopt(helem);
	    // Add all the new works to their sortable
	    branch.retrieve('sortable').addItems(response);
	    // Set its delete buttons
	    Portfolio.Helpers.tableDeleteButtons(branch, response.getElements('table'));
	    $(branch.retrieve('name') + '-collapse').fireEvent('click');
	    Portfolio.cleanForm(branch.retrieve('form'));
	} else {
	    var form = branch.retrieve('form');
	    var new_form = Elements.from(hcontent);
	    new_form.set('id', form.get('id'));
	    branch.store('form', new_form);
	    new_form.inject(form, 'after');
	    form.destroy();
	    Portfolio.setForm(branch);
	}
    },

    cleanForm: function(form) {
	form.getElements('input[type!=hidden], select, textarea').each(function(input) {
	    input.set('value', '');
	});
	form.getElements('li.error').removeClass('error');
	form.getElements('li.errorField').destroy();
	form.getElement('input[type=submit]').set('value', 'Add').disabled = false;
    }
};

Portfolio.PersonalInformation = {
    start: function() {
    },
};

Portfolio.About = {
    start: function() {
	Portfolio.About._text = $('portfolio-information').getElement('textarea');
	Portfolio.About._text.addEvents({
	    'click': function() {
		if (Portfolio.About._text.hasClass('empty'))
		    Portfolio.About._text.set('text', '');
	    },
	    'keyup': Portfolio.About.keyHandler
	});
	Portfolio.About._loading = new Element('span', {'class': 'loading', 'text': 'saving...'});
    },

    keyHandler: function(event) {
	// Remove this event, we only want to listen to it the
	// first time
	Portfolio.About._text.removeEvents('keyup');

	// Build the save button and inject it.
	if (Portfolio.About._save)
	    Portfolio.About._save.setStyle('display', 'inline');
	else {
	    Portfolio.About._save = new Element('input', {
		'type': 'button',
		'value': 'Save',
		'class': 'save-portfolio-information'
	    }).inject(Portfolio.About._text, 'after');
	}

	Portfolio.About._save.addEvent('click', function() {
	    Portfolio.About._loading.inject(Portfolio.About._save, 'after');
	    new Request.JSON({
		url: '/portfolio/xhr/save_description/',
		onComplete: function() {
		    Portfolio.About._text.addEvent('keyup', Portfolio.About.keyHandler);
		    Portfolio.About._save.setStyle('display', 'none');
		}
	    }).post({'description': Portfolio.About._text.value,});
	    Portfolio.About._loading = Portfolio.About._loading.dispose();
	});
    },
},

Portfolio.Helpers = {
    tableDeleteButtons: function(branch, tables) {
	$$(tables).each(function(table) {
	    var trigger = table.getElement('thead th.delete-table a').setStyle('display', 'none');
	    table.getElement('thead').addEvents({
		'mouseenter': function() { trigger.setStyle('display', 'inline'); },
		'mouseleave': function() { trigger.setStyle('display', 'none'); },
	    });
	    trigger.addEvent('click', function(e) {
		new Event(e).stop();
		// Delete the instance..
		Portfolio.deleteWork(branch, table.getParent('li'));
	    });
	});
    },
}

window.addEvent('domready', Portfolio.start);