(function()
{
	String.implement(
	{
		slugify: function( replace )
		{
			if( !replace ) replace = '-';
			var str = this.toString().tidy().standardize().replace(/[\s\.]+/g,replace).toLowerCase().replace(new RegExp('[^a-z0-9'+replace+']','g'),replace).replace(new RegExp(replace+'+','g'),replace);
			if( str.charAt(str.length-1) == replace ) str = str.substring(0,str.length-1);
			return str;
		}
	});
})();
var Site = {
    start: function() {
	if ($('directory-search')) Site.directorySearch();
	if ($('login-form')) Site.loginForm();
	if ($('personal-bin')){
        Site.personalBin();
    }else{
       if ($$('a.toggle-contact').length > 0){
           ContactList.quickstart();
       }
    }
	if ($('content-menu')) Site.contentMenu();
	if ($('resume')) Site.resume();
	Site.notifications = new Notifications({size: {height: '70px', width: '800px'}});
	Site.forms = new AdcakeForm({forms: $$('form.adcake-uni-form')});
	Site.collapsables = new Collapsable({triggers: $$('.collapsable-trigger')});
	Site.collapsables.start();
    },

    directorySearch: function() {
	Site.directory_search = $('directory-search-query');
	Site.directory_search.set('value', Site.directory_search.get('rel'));
	Site.directory_search.addEvents({
	    'focus': function() {
		if (Site.directory_search.get('value') == Site.directory_search.get('rel'))
		    Site.directory_search.set('value','');
		// Background-changing
		$('advertisement-directory').addClass('focus');
		// Advanced search
	    },
	    'blur': function() {
		if (Site.directory_search.get('value') == '')
		    Site.directory_search.set('value', Site.directory_search.get('rel'));
		// Background-restoring
		$('advertisement-directory').removeClass('focus');
		// Advanced search
	    }
	});
    },

    loginForm: function() {
	if ($('registration-form')) {
	    var registrationForm =  $('registration-form').set('slide', {duration: 500});
	    registrationForm.slide('hide');

	    $('open-registration').addEvent('click', function() { registrationForm.slide('in'); });
	    $('close-registration').addEvent('click', function() { registrationForm.slide('out'); });
	}
	else {
	    $('open-registration').addEvent('click', function(e) { e.stop(); window.location = '/accounts/request/' });
	}

	$$('div.post-form-buttons input').each(function (button) {
	    button.addEvent('mouseover', function() { button.getParent().set('styles', {'border-color': '#DD0F16'}); });
	    button.addEvent('mouseleave', function() { button.getParent().set('styles', {'border-color': '#999'}); });
	});
    },

    personalBin: function() {
	var togglers = $$('h3.pb-togg');
	var elements = $$('div.pb-element');
	Site.pb_accordion = new Accordion(togglers, elements, {
	    display: 3,
	    alwaysHide: true,
	    onActive: function(toggler) {
		toggler.setStyle('background-position', '-330px 0');
        toggler.setStyle('background-color', '#AAAAAA');
        toggler.setStyle('color', '#FFFFFF');
        toggler.setStyle('font-style', 'italic');
        toggler.setStyle('font-weight', 'bold');
	    },
	    onBackground: function(toggler) {
		toggler.setStyle('background-position', '0 0');
        toggler.setStyle('background-color', '#EBEBEB');
        toggler.setStyle('color', '#6a6a6a');
        toggler.setStyle('font-style', 'normal');
        toggler.setStyle('font-weight', 'normal');
	    }
	});
	// Implement history, display the last item opened

	// for each pb application...
	ContactList.start();
	Skills.start();
	new CakeBox();
    },

    resume: function() {
      Array.implement({
 	    makeSortable: function(options){
 	      var Sortable = new Sortables(this, options);
 	                       this.each(function(el){
                             el.getElements('.dragable').each(function(el){
                               el.setStyle('cursor', 'move');
                             });
 			                 //el.extend(Sortable);
 		                   });
          return Sortable;
 	    }
       });

      this.resume_obj = new ResumeBoxes();
      this.resume_obj._cat = $('categories');
      this.resume_obj._cat.getElements('category-header').each(function(el){
        el.setStyle('cursor', 'move');
      });
      this.resume_obj.catsortable = new Sortables(this.resume_obj._cat, {
	    clone: true, opacity: 0.5, handle: '.category-header',
	    onComplete: (function(){
                       this.sendNewCatOrder(this.resume_obj.catsortable);
                     }).bind(this)/*send order*/
	  });

      this.resume_obj.subs = $$('.subcategory').makeSortable({
	    clone: true, opacity: 0.5, handle: '.subcategory-header',
	    onComplete: (function(){
                       this.sendNewSubOrder(this.resume_obj.subs);
                     }).bind(this)/*send order*/
	  });

      this.resume_obj.skills = $$('.paired').makeSortable({
	    clone: true, opacity: 0.5, handle: 'div.subcategory h3',
        constrain: true,
	    onComplete: (function(){
                       this.sendNewSkillOrder(this.resume_obj.skills);
                     }).bind(this)/*send order*/
	  });


    },

    sendNewCatOrder: function(sortable) {
	    var new_order = sortable.serialize().map(function(id, index) {
	      return {'id': id, 'index': (index + 1)};
	    });

	    new Request.JSON({url: '/resume/categories/reorder/'}).post(
          {'new_order': JSON.encode(new_order)}
	    );
    },

    sendNewSubOrder: function(sortable) {
	    var new_order = sortable.serialize().flatten().map(function(id, index) {
	      return {'id': id, 'index': (index + 1)};
	    });

	    new Request.JSON({url: '/resume/categories/reorder/'}).post(
          {'new_order': JSON.encode(new_order)}
	    );
    },

    sendNewSkillOrder: function(sortable) {
        var new_order = sortable.serialize().flatten().map(function(id, index) {
          if(id!='skills-category'){
	        return {'id': id.replace('skill-', ''), 'index': (index + 1)};
          }else{
            return null;
          }
	    });

	    new Request.JSON({url: '/resume/skills/reorder/'}).post(
          {'new_order': JSON.encode(new_order)}
	    );
    },

    contentMenu: function() {
	$$('#content-menu a').each(function(alink) {
	    var fx = new Fx.Morph(alink,{ duration:150, link:'cancel' });
	    alink.addEvents({
		'mouseenter': function() { fx.start({ 'padding-left': 10 }); },
		'mouseleave': function() { fx.start({ 'padding-left': 0 }); }
	    });
	});
    }
};

var Collapsable = new Class({
    Implements: [Options],

    options: {
	triggers: $$('.collapsable-trigger')
    },

    initialize: function(options) {
	this.setOptions(options);
    },

    start: function() {
	this.options.triggers.each(function(trigger) {
	    var id_target = trigger.get('rel');
            var classes = trigger.getProperty("class").split(' ');
	    if (id_target != "" && $defined($(id_target))) {
		trigger.store('original-text', trigger.get('text'));
		if (classes.some(function(item, index) { return item == "open" })) {
		    trigger.set('text', 'Close');
		    trigger.addClass('close');
		    trigger.store('target-is-open', true);
		} else {
		    trigger.addClass('plus');
		    trigger.store('target-is-open', false);
		}
		trigger.addEvent('click', function(e) {
		    if (e)
			new Event(e).stop();
            var myHorizontalSlide = new Fx.Slide(id_target);
            myHorizontalSlide.toggle();
            if (trigger.retrieve('target-is-open')) {
			trigger.store('target-is-open', false); // Now it's close
			trigger.set('text', trigger.retrieve('original-text'));
			trigger.removeClass('close');
			trigger.addClass('plus');
		    }
		    else {
			trigger.store('target-is-open', true); // Now it's open
			trigger.set('text', 'Close');
			trigger.removeClass('plus');
			trigger.addClass('close');
		    }
		});
	    }
	});
    }
});

var AdcakeForm = new Class({
    //implements
    Implements: [Options],

    //options
    options: {
        forms: $$('form')
    },

    //initialization
    initialize: function(options) {
        //set options
        this.setOptions(options);
	this.setInputHelpers();
        this.setInputStyle();
    },

    //a method that does whatever you want
    setInputStyle: function() {
        this.options.forms.each(function(form) {
	    form.getElements('input, select, textarea').each(function(el) {
		if (el.getParent('li') != null) {
		    var parent = el.getParent('li');
		    el.addEvents({
			'focus': function() { parent.addClass('focused'); },
			'blur':  function() { parent.removeClass('focused'); }
		    });
		}
	    });
        });
    },

    setInputHelpers: function() {
	// Calendar date picker
	$$(this.options.forms).getElements('input.DateField').flatten().each(function(input) {
	    var toggler = new Element('span', {'class': 'dateToggler', 'text': 'Show calendar', 'title': 'Show calendar'});
	    toggler.inject(input, 'after');
	    new DatePicker(input, {
		toggleElements: toggler, 
		pickerClass: 'datepicker_jqui', 
		allowEmpty: true,
		inputOutputFormat: 'Y-m-d',
		format: 'Y-m-d'
	    });
	});
    }
});
var ContactList = {
    start: function() {
	ContactList._body = $('contact-list');
	ContactList._isEmpty = ContactList._body.getChildren('li').length == 0;

	ContactList.sortable = new Sortables(ContactList._body, {
	    clone: true, opacity: 0.5,
	    onComplete: ContactList.sendNewOrder
	});


	$$('#contact-list li').each(function(contact) {
	    contact.store('user', contact.get('title').replace('user-', ''));
	    var rc_button = contact.getElement('a.remove-contact').setStyle('display', 'none');
// 	    var watch_button = contact.getElement('a.watch-contact');
	    rc_button.addEvent('click', function(e) {
		new Event(e).stop();
		ContactList.remove(rc_button.getParent('li'));
	    });
// 	    watch_button.addEvent('click', function(e) {
// 		new Event(e).stop();
// 		ContactList.toggleWatch(watch_button, contact.get('id').replace('contact-list-', ''));
// 	    });

	    contact.addEvents({
		'mouseover': function(e) { rc_button.setStyle('display', 'block') },
		'mouseout': function(e) { rc_button.setStyle('display', 'none') }
	    });
	});
	$$('a.toggle-contact').each(function(tc_button) {
	    // add click funcionality
	    tc_button.addEvent('click', function(event) {
            new Event(event).stop();
            // Removing contact
            if (tc_button.hasClass('contact-added'))
                ContactList.remove(tc_button.get('rel').split(':')[1]);
            // Adding contact
            else
                ContactList.add(tc_button.get('rel').split(':')[0]);
	    });
	});

	if (ContactList._isEmpty)
	    ContactList.setEmpty();
    },
    quickstart: function() {

	$$('a.toggle-contact').each(function(tc_button) {
	    // add click funcionality
	    tc_button.addEvent('click', function(event) {
            new Event(event).stop();
            // Removing contact
            if (tc_button.hasClass('contact-added'))
                ContactList.quickremove(tc_button.get('rel').split(':')[0],tc_button.get('rel').split(':')[1]);
            // Adding contact
            else
                ContactList.quickadd(tc_button.get('rel').split(':')[0]);
	    });
	});
    },

    sendNewOrder: function() {
	var new_order = ContactList.sortable.serialize().map(function(id, index) {
	    return {'id': id.replace('contact-list-', '').toInt(), 'index': (index + 1)}
	});

	new Request.JSON({url: '/cake/contacts/reorder/'}).post(
	    {'new_order': JSON.encode(new_order)}
	);
    },

    toggleLinks: function(elements, contactId, action) {
	var find = ['Unfollow'];
	var replace = ['Follow'];

	if (action == 'add') {
	    var tmp = find;
	    find = replace; replace = tmp;
	}

	var el = $$(elements)[0];
	if (!$defined(el))
	    return false;
	var ntitle = el.get('title').replace(find[0], replace[0]).replace(find[1], replace[1]);
	var ntext = el.get('text').replace(find[0], replace[0]).replace(find[1], replace[1]);

	$$(elements).set({
	    'title': ntitle,
	    'text': ntext,
	    'rel': el.get('rel').split(':')[0] + ':' + contactId
	});
	$$(elements).toggleClass('contact-added');
    },

    add: function(user_id) {
	if (ContactList._isEmpty)
	    ContactList.unsetEmpty();

	// Set loading stage
	new Element('span', {'class': 'loading'}).inject($('pb-contact-list'));

	var contact;
	new Request.JSON({
	    url: '/cake/contacts/add/',
	    onComplete: function(response) {
		if ($defined(response) && response.succeed) {
		    contact = response.contact;
		    // Add the element to the sortable list
		    var new_contact = new Element('li', {'id': 'contact-list-' + contact.id});
		    var contact_name = new Element('span', {'class': 'contact-name'}).inject(new_contact);
		    new Element('a', {'href': contact.profile_url, 'class': 'contact-profile', 'title': contact.name + '\'s Profile', 'text': contact.name}).inject(contact_name);
//		    if (contact.messages != 0)
//			new Element('a', {''}) Number of messages from the contact
		    var contact_buttons = new Element('span', {'class': 'contact-buttons'}).inject(new_contact);
		    new Element('a', {'href': '/messages/compose/' + contact.uid + '/', 'title': 'Send a message to ' + contact.name, 'text': 'Write a message'}).inject(contact_buttons);
// 		    var watch_button = new Element('a', {'href': '#', 'title': 'Watch this contact', 'text': 'Watch this contact'}).inject(contact_buttons);
		    var rc_button = new Element('a', {'href': '#', 'title': 'Remove this contact', 'class': 'remove-contact', 'text': 'Delete this contact'}).setStyle('display', 'none').inject(new_contact);
		    new Element('br', {'class': 'clear'}).inject(new_contact);

// 		    watch_button.addEvent('click', function(e) {
// 			new Event(e).stop();
// 			ContactList.toggleWatch(watch_button, contact.id);
// 		    });
		    rc_button.addEvent('click', function(e) {
			new Event(e).stop();
			ContactList.remove(rc_button.getParent('li'));
		    });

		    new_contact.addEvents({
			'mouseover': function(e) { rc_button.setStyle('display', 'block') },
			'mouseout': function(e) { rc_button.setStyle('display', 'none') }
		    });

		    new_contact.store('user', user_id);
		    ContactList._body.adopt(new_contact);
		    ContactList.sortable.addItems(new_contact);
		    ContactList.resize();

		    // Toggle all the 'toggle-contacts' links on the page
		    ContactList.toggleLinks('a.toggle-user-' + user_id, contact.id, 'add');
		}
		// Something went wrong...
		// else { Site.die(0); }   Maybe pop some alert...

		// Unset loading stage
		$('pb-contact-list').getElement('span').destroy();
	    }
	}).post({'user_id': user_id});
    },


    quickadd: function(user_id) {
	var contact;
	new Request.JSON({
	    url: '/cake/contacts/add/',
	    onComplete: function(response) {
		if ($defined(response) && response.succeed) {
		    contact = response.contact;
		    // Toggle all the 'toggle-contacts' links on the page
		    ContactList.toggleLinks('a.toggle-user-' + user_id, contact.id, 'add');
		}
        }
	}).post({'user_id': user_id});
    },

    remove: function(contact) {
	if ($type(contact) == 'string')
	    contact = ContactList.lookUp(contact);
	var user_id = contact.retrieve('user');
	new Request.JSON({url: '/cake/contacts/delete/'}).post(
	    {'contact_id': contact.id.replace('contact-list-', '').toInt()}
	);
	ContactList.sortable.removeItems(contact).destroy();
	if (ContactList._body.getChildren('li').length == 0)
	    ContactList.setEmpty();
	ContactList.resize();

	// Toggle all the 'toggle-contacts' links on the page
	ContactList.toggleLinks('a.toggle-user-' + user_id, contact.id.replace('contact-list-', ''), 'remove');
    },

    quickremove: function(contact,rfrom) {
        new Request.JSON({url: '/cake/contacts/delete/'}).post({'contact_id': rfrom});
        ContactList.toggleLinks('a.toggle-user-' + contact, rfrom, 'remove');
    },

    toggleWatch: function(watch_button, contact_id) {
	var xpos = watch_button.getStyle('background-position').split(' ')[0].toInt();
	new Request.JSON({
	    url: '/cake/contacts/update/',
	    onComplete: function() {
		watch_button.setStyle('background-position', (xpos == 0 ? '-16px 0' : '0 0'));
		var new_text = (xpos == 0 ? 'Unw' : 'W') + 'atch this contact';
		watch_button.set({
		    'text': new_text,
		    'title': new_text
		});
	    }
	}).post({'contact_id': contact_id});
    },

    resize: function() {
	if (ContactList._body.getParent().getSize().y != 0)
	    ContactList._body.getParent().setStyle('height', ContactList._body.getSize().y + 30);
    },

    lookUp: function(contact_id) {
	var the_contact = ContactList._body.getChildren('li').filter(function(c) {
	    return c.id == 'contact-list-' + contact_id;
	});
	if (the_contact.length != 1) return null;
	return the_contact[0];
    },

    setEmpty: function() {
	new Element('li', {'class': 'no-contacts', 'text': 'You dont have any contacts yet.'}).inject(ContactList._body);
	ContactList._isEmpty = true;
    },

    unsetEmpty: function() {
	ContactList._body.getChildren('li').destroy();
	ContactList._isEmpty = false;
    }
};

var Skills = {
    start: function() {
	Skills.list_holder = $('my-skills');
	Skills.form = $('pb-skills').getPrevious('div').getElement('form');
	Skills.inputs = Skills.form.getElements('input');

	Skills.parse();

	Skills.form.addEvent('submit', function(e) {
	    new Event(e).stop();
	    if (Skills.inputs[0].value != "")
		Skills.addSkill()
	});

	new Autocompleter.Request.JSON($('skills-search'), '/skills/live_search/', {
	    'postVar': 'skill',
	    'multiple': true,
	    'minLength': 1,
	    'selectFirst': true
	});
    },

    parse: function() {
	Skills.list_holder.getElements('li').each(function(item) {
	    Skills.parseItem(item);
	});
    },

    parseItem: function(item) {
	var delete_skill = item.getElement('a.delete-skill');
	delete_skill.setStyle('display', 'none');
	delete_skill.addEvent('click', function(e) {
	    new Event(e).stop();
	    Skills.removeSkill(item);
	});

	item.addEvent('mouseover', function() { delete_skill.setStyle('display', 'block'); });
	item.addEvent('mouseout', function() { delete_skill.setStyle('display', 'none'); });
    },

    createItem: function(name) {
	// Create the new element and insert it into the list.
	var new_skill = new Element('li');
	new Element('a', {'href': '/skills/'+ name.slugify() +'/', 'title': name, 'text': name}).inject(new_skill);
	new Element('a', {'href': '#', 'title': 'Delete this skill', 'class': 'delete-skill'}).inject(new_skill);
	new Element('br', {'class': 'clear'}).inject(new_skill);
	new_skill.inject(Skills.list_holder);

	Skills.parseItem(new_skill);
	Skills.list_holder.getParent().setStyle('height', Skills.list_holder.getParent().y + new_skill.getSize().y);
    },

    addSkill: function() {
	Skills.form.addClass('loading');
	// Send the xhr request to save the skill.
	new Request.JSON({
	    url: '/skills/add/',
	    onComplete: function(response) {
		if (response.complete)
		    response.skills.each(function(skill) {
			Skills.createItem(skill);
		    });
		Skills.inputs[1].value = "";
	    }
	}).post({'skill': Skills.inputs[1].value});
	Skills.form.removeClass('loading');
    },

    removeSkill: function(item) {
	Skills.form.addClass('loading');
	// Send the xhr request to delete the skill
	new Request.JSON({
	    url: '/skills/remove/',
	    onComplete: function(response) {
		if (response.complete) {
		    Skills.list_holder.getParent().setStyle('height', Skills.list_holder.getParent().y - item.getSize().y);
		    item.destroy();
		}
	    }
	}).post({'skill': item.getElement('a').get('text')});
	Skills.form.removeClass('loading');
    }
};

var Notifications = new Class(
  {
  Implements: [Options, Events],

  options: {
	container_name: 'notifications',
    duration: 3000,
    size: { height: '70px', width: '100%'},
    on_top: true
  },

  initialize: function(options) {
	this.setOptions(options);

    this.container = $(this.options.container_name);
    if( !this.options.on_top ){
      this.container.set('slide', {});
      this.container.slide('hide');
      this.container.setStyle('top', '0px');
    }
    this.container.setStyle('z-index', '9000');
    this.container.setStyle('position', 'fixed');
    this.container.setStyle('height', this.options.size.height);
    this.container.setStyle('width', this.options.size.width);
    this.container.setStyle('left', (window.getWidth() - this.container.getWidth())/2);
    if( this.options.on_top ){
      this.container.set('slide', {});
      this.container.slide('hide');
      this.container.setStyle('top', '0px');
    }
    this.container.slide('hide');
  },

  slide: function(type){
    this.container.slide(type);
  },

  close: function(){
    this.slide('out');
    this.container.getChildren().destroy();
  },

  insertElement: function(message, parent){
    var elem = new Element('div', {id:'message'}).adopt(
      [
        new Element('h3', {'html':'Notification'}),
        new Element('p', {'html':message})
      ]
    );

    if ($defined(span = parent.getElement('div#message'))){
      elem.replaces(span);
    }else{
      parent.grab(elem);
    }
  },

  send: function(message){
    this.stop = false;
    this.insertElement(message, this.container);
    this.slide('in');
    this.animate(this.options.duration);
    var close_timer = (function(){
                         this.close();
                       }).bind(this).delay(this.options.duration);
    var close = new Element('div', {id: 'close', html: 'close'});
    close.addEvent('click', (function(){this.slide('hide');}).bind(this));
    this.container.addEvent('click', (function(){
                                        this.stop = true;
                                        $clear(close_timer);
                                        this.timer.fx.start('opacity', '100%');
                                        this.timer.set('html',
                                                       '<span id="stopped">Stopped</span>');
                                      }).bind(this));
    this.container.getElement('div#message').grab(close);
  },

  animate: function(time) {
    if ($defined(timer = this.container.getElement('div#timer'))){
      timer.set('html', time/1000);
    }else{
      this.container.getElement('div#message').adopt(
        timer = new Element('div',{id: 'timer',
                                   html: time/1000 })
      );
      this.timer = timer;
    }
    var fx = new Fx.Tween(timer,{
                             duration: 1000,
                             onComplete: function() {
                               if ( time > 0 && !this.stop)
                                 this.animate(time-1000);
                             }.bind(this)
    }).start('opacity','100%','10%');
    this.timer.fx = fx;
  }

  }
);

window.addEvent('domready', Site.start);