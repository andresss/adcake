# -*- coding: utf-8 -*-
#settings that should not be shared through version control
import os
import private_settings
TESTING_MODE = True
LOCALVIDEO = True
#INTERNAL_IPS = ('127.0.0.1',)
#import whoosh as whoosh_backend

from django.conf.global_settings import \
     TEMPLATE_CONTEXT_PROCESSORS, AUTHENTICATION_BACKENDS

# Django settings for adcake project.

DEBUG = True
TEMPLATE_DEBUG = DEBUG

ADMINS = (
    ('AES', 'ctaloc@gmail.com'),
    ('MAS', 'advertti@gmail.com'),
)

MANAGERS = ADMINS

DEFAULT_FROM_EMAIL = 'adcake@adcake.com'

DATABASE_ENGINE = private_settings.DATABASE_ENGINE
DATABASE_NAME = private_settings.DATABASE_NAME
DATABASE_USER = private_settings.DATABASE_USER
DATABASE_PASSWORD = private_settings.DATABASE_PASSWORD
DATABASE_HOST = private_settings.DATABASE_HOST
DATABASE_PORT = private_settings.DATABASE_PORT


########### SORL CONFIGURE ##############
THUMBNAIL_EXTENSION = 'jpg'


# Local time zone for this installation. Choices can be found here:
# http://en.wikipedia.org/wiki/List_of_tz_zones_by_name
# although not all choices may be available on all operating systems.
# If running in a Windows environment this must be set to the same as your
# system time zone.
TIME_ZONE = 'America/Panama'

# Language code for this installation. All choices can be found here:
# http://www.i18nguy.com/unicode/language-identifiers.html
LANGUAGE_CODE = 'en-us'

SITE_ID = 1

# If you set this to False, Django will make some optimizations so as not
# to load the internationalization machinery.
USE_I18N = True

# Absolute path to the directory that holds media.
# Example: "/home/media/media.lawrence.com/"
MEDIA_ROOT = private_settings.MEDIA_ROOT

# URL that handles the media served from MEDIA_ROOT. Make sure to use a
# trailing slash if there is a path component (optional in other cases).
# Examples: "http://media.lawrence.com", "http://example.com/media/"
MEDIA_URL = '/site-media/'

# URL prefix for admin media -- CSS, JavaScript and images. Make sure to use a
# trailing slash.
# Examples: "http://foo.com/media/", "/media/".

# Make this unique, and don't share it with anybody.
SECRET_KEY = '-vd49y$1n(sj(!gi7e@tv1z-jli49$o8c-4witc)&k(m@+$yq1'


# List of callables that know how to import templates from various sources.
TEMPLATE_LOADERS = (
    'django.template.loaders.filesystem.load_template_source',
    'django.template.loaders.app_directories.load_template_source',
    'django.template.loaders.eggs.load_template_source',

)
TEMPLATE_CONTEXT_PROCESSORS += (
    'django.core.context_processors.request',
)
#

# S3 SETUP
#
ACCESS_KEY='AKIAIDXP3OEUDOQXQSHA'
PASS_KEY='49h8er17qDnNvUwu6ThzsFWritDtdna3b0c2D6Uf'

MIDDLEWARE_CLASSES = (
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.flatpages.middleware.FlatpageFallbackMiddleware',
    'debug_toolbar.middleware.DebugToolbarMiddleware',
#    'axes.middleware.FailedLoginMiddleware',
)


ROOT_URLCONF = 'urls'

TEMPLATE_DIRS = (
    # Put strings here, like "/home/html/django_templates" or "C:/www/django/templates".
    # Always use forward slashes, even on Windows.nic
    # Don't forget to use absolute paths, not relative paths.
) + private_settings.TEMPLATE_DIRS 

#App specific settings
#

# ADS settings
ADS_DISPLAY_QUANTITY = 4


AUTH_PROFILE_MODULE = 'accounts.UserProfile'

# Adding the email-auth backend
AUTHENTICATION_BACKENDS += (
    'adcake.accounts.email_auth.EmailBackend',
)

# ACCOUNTS settings
LOGIN_REDIRECT_URL = '/'

# RESUME settings
from django.utils.translation import ugettext_lazy as _
PERSONAL_INFO_NAME = _('Personal Information')
# HAYSTACK settings
HAYSTACK_SITECONF = 'search_sites'
HAYSTACK_SEARCH_ENGINE = 'xapian'
HAYSTACK_INCLUDE_SPELLING = True
HAYSTACK_XAPIAN_PATH = './.xapian_index'
# IMAGE settings
IMAGE_SHOWCASE_SIZE = (580, 700)

# NEWS settings
NEWS_UPLOAD_DIR = 'uploads/news' # no trailing slash..

# REGISTRATION settings
ACCOUNT_ACTIVATION_DAYS = 80

# SKILL settings
SKILLS_FORCE_LOWERCASE = True
SKILLS_LEVEL_INTERVAL = range(1,6)

# SPOTLIGHT settings
SPOTLIGHT_UPLOAD_DIR = 'uploads/spotlight'

# DJANGO COMPRESSOR
CACHE_BACKEND = 'memcached://127.0.0.1:11211/'
COMPRESS_CSS_FILTERS = ['compressor.filters.css_default.CssAbsoluteFilter','compressor.filters.yui.YUICSSFilter']
COMPRESS_YUI_BINARY = 'yui-compressor'
COMPRESS_OFFLINE=True

# TAGGING settings
FORCE_LOWERCASE_TAGS = True

# TINYMCE settings
TINYMCE_DEFAULT_CONFIG = {
    'plugins': "table, paste, style, searchreplace",
    'theme_advanced_buttons3_add' : "tablecontrols,styleprops,search,replace",
    'table_styles' : "Header 1=header1;Header 2=header2;Header 3=header3",
    'table_cell_styles' : "Header 1=header1;Header 2=header2;Header 3=header3;Table Cell=tableCel1",
    'table_row_styles' : "Header 1=header1;Header 2=header2;Header 3=header3;Table Row=tableRow1",
    'table_cell_limit' : 100,
    'table_row_limit' : 5,
    'table_col_limit' : 5,
    'theme': "advanced",
}

#THUMBNAIL settings
THUMBNAIL_DEBUG = DEBUG

#Video list 
DEFAULT_SHOW_VIDEOS_IN = 'grid' # grid or list

# Twitter Refresh time
TWITTER_TIMEOUT = 3600

TINYMCE_DEFAULT_CONFIG = {
    'mode' : "textareas",
    'theme' : "advanced",
}

#Filebrowser settimgs
FILEBROWSER_URL_FILEBROWSER_MEDIA = '/site-media/filebrowser/'

# Resume Search
RESUME_NUM_PER_PAGE = 2


# Re-Captcha
RECAPTCHA_PRIVATE_KEY = '6LeOBAYAAAAAAEUAdddc6zprhOfJKm9ZpyoH3UTg '
RECAPTCHA_PUBLIC_KEY = '6LeOBAYAAAAAAG4hkgFRLaC77ZcoFnzzCVvxT3T7'

# UPLOAD SETTINGS
VIDEO_UPLOAD_FILE_TYPES = ['video',]
VIDEO_UPLOAD_FILE_MAX_SIZE = 838860800
VIDEO_UPLOAD_FILE_EXTENSIONS = ['mov','avi','mpeg','flv','mp4','MOV','AVI','MPEG','FLV','MP4']
IMAGE_UPLOAD_FILE_TYPES = ['image/jpeg','image/png']
IMAGE_UPLOAD_FILE_EXTENSIONS = ['png','jpg']
COUNTRIES_FLAG_PATH = 'images/flags/gif/%s.gif'


ENDLESS_PAGINATION_ORPHANS=1

# DJANGO INVITATION
INVITE_MODE = True
ACCOUNT_INVITATION_DAYS = 80
INVITATIONS_PER_USER = 0

INSTALLED_APPS = (
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.sites',
    'django.contrib.flatpages',
    'django.contrib.admin',
    'django.contrib.admindocs',
    'accounts',
    'ads',
    'cake',
    'contrib',
    'countries.countries',
    'directory',
    'django-uni-forms.uni_form',
    'portfolio',
    'helpers',
    'messages',
    'multimedia',
    'news',
    'registration',
    'resume',
    'skills',
    'spotlight',
    'sorl.thumbnail',
    'tagging',
    'professionals',
    'jobs',
    # 'haystack',
    'django.contrib.comments',
    'django_extensions',
    'endless_pagination',
    'tinymce',
    'invitation',
    'oembed',
    'contact',
    'email-retriever',
    'compressor',
    'media_comments',
    'debug_toolbar',
#    'axes',
)

try:
    from server_settings import *
except:
    pass

