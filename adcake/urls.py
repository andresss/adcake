# -*- coding: utf-8 -*-
from django.conf.urls.defaults import *
from django.views.generic.simple import direct_to_template
from directory.urls import company_detail
from portfolio.views import view_portfolio
from django.views.generic import list_detail
import settings
from django.contrib import admin
import oembed
# Uncomment the next two lines to enable the admin:
oembed.autodiscover()
admin.autodiscover()
#import haystack
#haystack.autodiscover()
from contact.views import contact
from views import homepage, view_port_for
#from views import SearchView

urlpatterns = patterns('',
    # Example:
    # (r'^adcake/', include('foo.urls')),
    # Uncomment the admin/doc line below and add 'django.contrib.admindocs' 
    # to INSTALLED_APPS to enable admin documentation:
    (r'^estramboticaanastacia/doc/', include('django.contrib.admindocs.urls')),
    # Uncomment the next line to enable the admin:
    # (r'^estramboticaanastacia/filebrowser/', include('filebrowser.urls')),
    (r'^estramboticaanastacia/eretriever/', include('email-retriever.urls')),
    (r'^estramboticaanastacia/', include(admin.site.urls)),
    # Haystack
    #(r'^search/', SearchView(), {}, 'search'),
    (r'^$', homepage, {}, 'homepage'),
    (r'^accounts/', include('accounts.urls')),
    (r'^cake/', include('cake.urls')),
    (r'^directory/', include('directory.urls')),
    (r'^galleries/', include('portfolio.galleries_urls')),
    (r'^images/', include('multimedia.images_urls')),
    (r'^messages/', include('messages.urls')),
    (r'^news/', include('news.urls')),
    (r'^portfolio/', include('portfolio.urls')),
    (r'^professionals/', include('professionals.urls')),
    (r'^accounts/', include('invitation.urls')),
    (r'^accounts/', include('registration.urls')),    
#    (r'^registration/', include('registration.urls')),
    (r'^skills/', include('skills.urls')),
    (r'^videos/', include('multimedia.videourls')),
    (r'^multimedia/', include('multimedia.urls')),
    (r'^resume/', include('resume.urls')),
    (r'^profile/', include('profiles.urls')),
    (r'^jobs/', include('jobs.urls')),
    (r'^comments/', include('django.contrib.comments.urls')),
    (r'^tinymce/', include('tinymce.urls')),
    url(r'^terms', direct_to_template, {'template':'terms_of_use.html'}, name='terms_of_use'),
    url(r'^privacy', direct_to_template, {'template':'privacy_policy.html'}, name='privacy_policy'),
    (r'^contact/', contact, {}, 'contact_us'),

)

if settings.DEBUG:
    urlpatterns += patterns('',
    (r'^site-media/(?P<path>.*)$', 'django.views.static.serve', {'document_root': settings.MEDIA_ROOT}),
    )

urlpatterns += patterns('',
    url(r'(?P<username>[\w.-]+)/',view_port_for,name="portfolio_for_shortcut"),
    url(r'(?P<slug>[\w.-]+)/',list_detail.object_detail, company_detail,name="cportfolio_for_shortcut"),
)
