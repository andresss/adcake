# -*- coding: utf-8 -*-
from dajax.core.Dajax import Dajax
from multimedia.views import get_comment_videos_by_page
from django.template.loader import render_to_string
def multiply(request, a, b):
    dajax = Dajax()
    result = int(a) * int(b)
    dajax.assign('#result','value',str(result))
    return dajax.json()
    
def pagination(request, p,video_id):
    try:
        page = int(p)
    except:
        page = 1
    comments = get_comment_videos_by_page(page,video_id)
    render = render_to_string('multimedia/video_comment_pagination.html', { 'comments': comments,'video_id':video_id})
    dajax = Dajax()
    dajax.assign('#pag','innerHTML',render)
    return dajax.json()