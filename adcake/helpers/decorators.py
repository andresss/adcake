from functools import wraps
from django.http import HttpResponseBadRequest

def require_XHR(func):
    """
    Decorator to make a view only accept XML Requests, Usage::

        @require_XHR
        def some_view(request, *args, **kwargs):
            # Here we can assume that the request is ajax
            pass

    """
    @wraps(func)
    def decorator(request, *args, **kwargs):
        if not request.is_ajax():
            return HttpResponseBadRequest("XMLHttpRequest expected")
        return func(request, *args, **kwargs)
    return decorator
