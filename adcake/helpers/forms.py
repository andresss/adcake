#mapping all widget types to TextInput, CheckboxInput, Select, FileInput, RadioInput, Textarea
mapping = { 
    'CheckboxInput' : 'checkboxInput',
    'TextInput' : 'textInput',
    'DateTimeInput' : 'textInput',
    'Select' : 'selectInput',
    'FileInput' : 'fileUpload',
    'SelectMultiple' : 'selectInput',
    'NullBooleanSelect' : 'radioInput',
    'PasswordInput': 'textInput',
    'HiddenInput' : 'textInput',
    'Textarea' : 'Textarea',
    'RadioSelect' : 'radioInput',
    'CheckboxSelectMultiple' : 'checkboxInput',
    }

def add_helpful_classes(form):
    for name,field in form.fields.iteritems():
        new_classes = (type(field).__name__,
                       type(field.widget).__name__, 
                       field.required and "Required" or "Optional")
        try:
            mapped = mapping[type(field.widget).__name__]
        except KeyError:
            mapped = 'defaultInput'
        if not mapped in new_classes:
            new_classes += (mapped,)
        if 'class' in field.widget.attrs:
            classes = field.widget.attrs['class'].split()
            classes.extend(new_classes)
            field.widget.attrs['class'] = " ".join(classes)
        else:
            field.widget.attrs['class'] = " ".join(new_classes)
