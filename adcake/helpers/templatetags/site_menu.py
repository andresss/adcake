from django.template import Library

register = Library()

@register.inclusion_tag('main_menu.html',takes_context=True)
def main_menu(context,module=None):
    user = context.get('user')
    return { 'user': user}

@register.inclusion_tag('sub_menu.html')
def sub_menu(module=None):
    return locals()
