from django.utils.html import conditional_escape
from django.utils.safestring import mark_safe
from django.template import Library

register = Library()

@register.filter(name='reverse')
def reverse(list):
    """
    Returns a list reverted
    """
    list.reverse()
    return list

@register.filter(name='attr_list')
def attr_list(list, attr):
    """
    Given a list of objects and an attribute in those objects, returns a new
    list formed by 'object.attribute' for every object in the original list.
    """
    return [getattr(obj, attr) for obj in list]

@register.filter(name='truncatechars')
def truncatechars(string, num_chars):
    """
    Same as truncatewords but working on the charactes of the string
    """
    try:
        length = int(num_chars)
    except ValueError:
        return string
    output = string
    if len(output) > length:
        return output[:length] + ' ...'
    return output

@register.filter(name='keeptags')
def keeptags(value, tags):
    """
    Strips all [X]HTML tags except the space seperated list of tags 
    from the output.
    
    Usage: keeptags:"strong em ul li"
    """
    import re
    from django.utils.html import strip_tags, escape
    tags = [re.escape(tag) for tag in tags.split()]
    tags_re = '(%s)' % '|'.join(tags)
    singletag_re = re.compile(r'<(%s\s*/?)>' % tags_re)
    starttag_re = re.compile(r'<(%s)(\s+[^>]+)>' % tags_re)
    endtag_re = re.compile(r'<(/%s)>' % tags_re)
    value = singletag_re.sub('##~~~\g<1>~~~##', value)
    value = starttag_re.sub('##~~~\g<1>\g<3>~~~##', value)
    value = endtag_re.sub('##~~~\g<1>~~~##', value)
    value = strip_tags(value)
#    value = escape(value)
    recreate_re = re.compile('##~~~([^~]+)~~~##')
    value = recreate_re.sub('<\g<1>>', value)
    return value

@register.filter(name='highlightpattern')
def highlightpattern(string, pattern, points=None, opentag='<b>', 
                     closetag='</b>', autoescape=None):
    """
    Highlights the pattern in the text.
    """
    def findall(s, x):
        s = s.replace("<b>", "<->").replace("</b>", "</->")
        start, end = 0, len(s)
        while start<end:
            start = s.find(x, start, end)
            if start != -1:
                yield start
                start += len(x)
            else:
                start = end

    def esc(x, n=0):
        if n!=0:
            return x
        if autoescape:
            return conditional_escape(x)
        return x

    if string == None:
        return ''
    try:
        str(pattern)
    except:
        print 'Pattern must be a string. Failing silently.'
        return string 
 
    try:
        str(opentag)
        str(closetag)
    except:
        print ('Both `opentag` and `closetag` must be a string. '+
               ' Failing silently.')
        return string 
   
    if not points:
        points = list(findall(string, pattern))

    l = len(pattern)
    extra = len(opentag)+len(closetag)
    o = string
    for n, i in enumerate(points):
        o = (esc(o[:i+7*n], n) + opentag + esc(o[i+7*n:i+l+7*n]) + 
             closetag + esc(o[i+l+7*n:]))

    return mark_safe(o)
highlightpattern.needs_autoescape = True
highlightpattern.mark_safe = True

def autotruncate(string, length, points):
    """
    Truncates a string around the best match of some points of interest.

    >>> autotruncate('this is a large string to test', 9, [8, 11])
    'a large s'
    >>> autotruncate('this aa a large string to test', 9, [5, 6, 8, 11])
    'aa a larg'
    >>> autotruncate('this aa a lerga string to test', 5, [5, 6, 8, 14])
    'aa a '
    """
    lower, p_index = 0, 0
    max = len(string)
    from collections import defaultdict
    d = defaultdict(int)
    while lower + length < max and p_index<len(points):
        upper = lower + length
        i = p_index
        while i<len(points) and points[i] < upper:
            d[lower] += 1
            i += 1
        lower = points[p_index]
        p_index += 1


    if not d:
        d[0] = 1

    final = sorted([(v,k) for k,v in d.items()], reverse=True)[0]
    lower = final[1]

    return string[lower:lower+length]


@register.filter(name='truncatesmart')
def truncatesmart(value, limit=80):
    """
    Truncates a string after a given number of chars keeping whole words.

    Usage:
        {{ string|truncatesmart }}
        {{ string|truncatesmart:50 }}
    """

    try:
        limit = int(limit)
    # invalid literal for int()
    except ValueError:
        # Fail silently.
        return value

    # Make sure it's unicode
    value = unicode(value)

    # Return the string itself if length is smaller or equal to the limit
    if len(value) <= limit:
        return value

    # Cut the string
    value = value[:limit]

    # Break into words and remove the last
    words = value.split(' ')[:-1]

    # Join the words and return
    return ' '.join(words) + '...'