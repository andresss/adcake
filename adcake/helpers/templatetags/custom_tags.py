import operator
from django.template import Library, Node, VariableDoesNotExist, TemplateSyntaxError
from custom_filters import highlightpattern, autotruncate
from django.utils.safestring import mark_safe
     
register = Library()

class SplitListNode(Node):
    """
    taken from http://www.djangosnippets.org/snippets/660/
    with some modifications in render.
    """
    def __init__(self, sequence, chunk_size, new_list_name):
        self.list = sequence
        self.chunk_size = chunk_size
        self.new_list_name = new_list_name

    def split_seq(self, seq, size):
        """ Split up seq in pieces of size, from
        http://aspn.activestate.com/ASPN/Cookbook/Python/Recipe/425044"""
        return [seq[i:i+size] for i in range(0, len(seq), size)]

    def render(self, context):
        try:
            values = self.list.resolve(context, True)
        except VariableDoesNotExist:
            values = []
        if values is None:
            values = []
        if not hasattr(values, '__len__'):
            values = list(values)
        context[self.new_list_name] = self.split_seq(values, int(self.chunk_size))
        return ''

def split_list(parser, token):
    """<% split_list list as new_list 5 %>"""
    bits = token.contents.split()
    if len(bits) != 5:
        raise TemplateSyntaxError, "split_list list as new_list 5"
    sequence = parser.compile_filter(bits[1])
    return SplitListNode(sequence, bits[4], bits[3])
    
split_list = register.tag(split_list)


def findall(s, x):
    start, end = 0, len(s)
    s = s.replace("<b>", "<->").replace("</b>", "</->")
    while start<end:
        start = s.find(x, start, end)
        if start != -1:
            yield start
            start += len(x)
        else:
            start = end

class MatchStringNode(Node):
    def __init__(self, old, new, length):
        self.old = old
        self.new = new
        if int(length) == -1:
            self.truncate = False
        else:
            self.truncate = True
        self.length = min(int(length), len(old)-1)

    def render(self, context):
        try:
            self.old_val = context[self.old]
        except VariableDoesNotExist:
            raise TemplateSyntaxError ("'%s' is not defined in context" % 
                                       self.old)
        try:
            patterns = context['patterns']
        except VariableDoesNotExist:
            raise TemplateSyntaxError, "'patterns' is not defined in context"

        context[self.new] = []
        for match in self.old_val:
            mcv = []
            for old in match:
                if old is None:
                    mcv.append(mark_safe(''))
                    continue
                #print patterns
                all = [list(findall(old, i)) for i in patterns]
                #print all
                points = sorted(reduce(operator.__add__, all))
                if not self.truncate:
                    self.length = len(old)
                new = autotruncate(old, self.length, points)
                for pattern in patterns:
                    new = highlightpattern(new, pattern)
                mcv.append(mark_safe(new))
                
            context[self.new].append(dict(zip(['match', 'value', 'comment'], 
                                              mcv)))
        return ''

def matchstring(parser, token):
    """
    <% matchstring string as new_string with maxlength%>
    maxlength == -1 deactivates truncating.
    """
    bits = token.contents.split()
    if len(bits) != 6:
        raise TemplateSyntaxError, "matchstring string as new_string with maxlength"
    return MatchStringNode(bits[1], bits[3], bits[5])
    
matchstring = register.tag(matchstring)
