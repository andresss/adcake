import re
from django.conf import settings
from django.core.serializers import serialize
from django.db.models.query import QuerySet
from django.core.mail import SMTPConnection, EmailMessage
from django.http import HttpResponse
from django.utils import simplejson
from django.forms import ModelForm, Form
import memcache
# from djangosearch.results import SearchResults
from django.core.mail import EmailMultiAlternatives
from django.template.loader import render_to_string
from django.utils.html import strip_tags

if settings.DEBUG:
    mime = 'application/javascript'
    level = 2
else:
    mime = 'application/json'
    level = None

class JsonResponse(HttpResponse):
    """
    Retorna un response con el objeto serializado como JSON.
    En caso de que el objeto sea un Form, no se retorna la serializacion
    del form sino de sus errores.
    
    (Puede que no maneje 'out of the box' algunos tipos de objetos)

    El Parametro restricted_to solo se utiliza en el caso de los forms y
    es una lista o tupla de los campos que se van a retornar. 
    """
    def __init__(self, object, restricted_to=None):
        if isinstance(object, QuerySet) or isinstance(object, list):
            content = serialize('json', object, indent=level)
        elif isinstance(object, (ModelForm, Form)):
            content = errors_to_json(object, restricted_to)
        else:
            content = simplejson.dumps(object, indent=level)
        super(JsonResponse, self).__init__(content, mimetype=mime)


def errors_to_json(form, restricted_to=None):
    errors = {}
    if not restricted_to:
        for field in form:
            errors[field.auto_id] = {'name': field.name,
                                     'errors': [unicode(e) for e in
                                                field.errors]}
    else:
        for field in form:
            if field.name in restricted_to and field.errors:
                errors[field.auto_id] = {'name': field.name,
                                         'errors': [unicode(e) for e in
                                                    field.errors]}
    if form.errors.has_key('__all__'):
        errors['__all__'] = form.errors['__all__']
        
    return simplejson.dumps(errors, indent=level)


# ----------------------- EMAILS HELPERS --------------------------
def send_html_mail(subject, message, from_email, recipient_list,
                   fail_silently=False, auth_user=None, auth_password=None):
    connection = SMTPConnection(username=auth_user, password=auth_password,
                                fail_silently=fail_silently)
    mail = EmailMessage(subject, message, from_email, recipient_list,
                        connection=connection)
    return mail.send(fail_silently=fail_silently)


def send_multi_alternative_mail(subject,text_content,html_content,from_email,recipient_list):
    from django.core.mail import EmailMultiAlternatives
    msg = EmailMultiAlternatives(subject, text_content, from_email, recipient_list)
    msg.attach_alternative(html_content, "text/html")
    msg.send()

def send_comment_to_mail(comment,media,to):
    subject, from_email, to = "Someone commented in one of your works", 'adcake@adcake.com', to

    html_content = render_to_string('html_letters/comment_mail.html', {'comment':comment,'media':media})
    text_content = strip_tags(html_content) # this strips the html, so people will have the text as well.
    # create the email, and attach the HTML version as well.
    msg = EmailMultiAlternatives(subject, text_content, from_email, [to])
    msg.attach_alternative(html_content, "text/html")
    msg.send()

def send_html_to_mail(template,context,subject,to):
    subject, from_email, to = subject, 'adcake@adcake.com', to

    html_content = render_to_string(template, context)
    text_content = strip_tags(html_content) # this strips the html, so people will have the text as well.
    # create the email, and attach the HTML version as well.
    msg = EmailMultiAlternatives(subject, text_content, from_email, [to])
    msg.attach_alternative(html_content, "text/html")
    msg.send()

#------------------------ END EMAILS HELPERS ----------------------

#------------------------ CACHE HELPERS ---------------------------
def clean_cache(key):
    mc = memcache.Client(['127.0.0.1:11211'], debug=0)
    try:
        mc.delete(key)
    except:
        pass

def check_clean_cache(element):
    mc = memcache.Client(['127.0.0.1:11211'], debug=0)
    mvl=mc.get("most_view_list")
    if mvl:
        if mvl.__contains__(element):
            mc.delete("most_view_list")

    nml=mc.get("new_media_list")
    if nml:
        if nml.__contains__(element):
            mc.delete("new_media_list")
    rvl=mc.get("random_view_list")
    if rvl:
        if rvl.__contains__(element):
            mc.delete("random_view_list")

def clean_all_media_cache():
    mc = memcache.Client(['127.0.0.1:11211'], debug=0)
    mc.delete("most_view_list")
    mc.delete("random_view_list")
    mc.delete("new_media_list")

#------------------------ END CACHE HELPERS ---------------------------

#------------------------- STRING HELPERS -----------------------------

def special_match(strg, search=re.compile(r'[^a-zA-Z0-9 ]').search):
    return not bool(search(strg))

def clean_spacing(string):
    return re.sub(' +',' ',string)

def clean_skill(string):
    if special_match(string):
        return clean_spacing(string)
    else:
        return ''
