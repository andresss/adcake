from django.contrib import admin
from django import forms
from django.utils.translation import ugettext_lazy as _
from django.contrib.flatpages.models import FlatPage
from django.contrib.flatpages.admin import FlatPageAdmin as FlatPageAdminOld
from tinymce.widgets import TinyMCE

class FlatPageAdminForm(forms.ModelForm):
    url = forms.RegexField(label=_("URL"), max_length=100, regex=r'^[-\w/]+$',
        help_text = _("Example: '/about/contact/'. Make sure to have leading"
                      " and trailing slashes."),
        error_message = _("This value must contain only letters, numbers,"
                          " underscores, dashes or slashes."))
    content = forms.CharField(widget=TinyMCE(attrs={'cols': 100, 'rows': 35}))

    class Meta:
        model = FlatPage

class FlatPageAdmin(FlatPageAdminOld):
    form = FlatPageAdminForm

## # We have to unregister it, and then reregister
admin.site.unregister(FlatPage)
admin.site.register(FlatPage, FlatPageAdmin)
