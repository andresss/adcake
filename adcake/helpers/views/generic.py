from django.shortcuts import render_to_response
from django.http import Http404, HttpResponseRedirect
from django.db.models import get_model
from django.db.models.fields.related import ForeignKey
from helpers.utils import JsonResponse
from django.conf import settings

def ajax_validate(request, formclass, restricted_to=None, split_char='+'):
    """
    Realiza la validacion del formulario y retorna un objeto en json.

    formclass
      La clase de python que representa el formulario

    restricted_to
      Una lista de los nombres de los fields que se van a validar.
      Para usar en templates la lista puede venir en la forma: field1+field2+...

    split_char
      Caracter usado para separar la lista. 
    """
    form = formclass(request.POST)
    if isinstance(restricted_to, basestring):
        restricted_to = restricted_to.split(split_char)
        
    assert restricted_to is None or isinstance(restricted_to, list) 
    return JsonResponse(form, restricted_to=restricted_to)


def generic_json(request, formclass, modelclass, include, success_redirect=None, fail_redirect="/", allow_get=None,
                 crud='update', restrict_errors_to=None, template=None, mappings={}):
    """    
    Vista generica con respuesta en JSON para los errores.

    formclass
      El formulario (newform).

    modelclass
      El modelo que se va a alterar en caso de que sea los datos sean validos

    include
      Lista de los fields que se deben incluir en la creacion de las clases

    success_redirect
      URL al cual se redirige luego de postear exitosamente
      Default: <model_name>_posted/

    success_redirect
      URL al que se redirige en caso de un error
      (i.e. not allow_get and not request.META.has_key('HTTP_REFERER') and request.GET)
      Default: /

    allow_get
      if True, permite que se renderice el template con el formulario en caso
      de que se haga un request GET. De lo contrario se redirige a la pagina anterior

    crud
      El metodo crud que se quiere usar: create, update or delete

    restrict_errors_to
      Restringe los errores que se van a mostrar en la respuesta de JSON.
      De no proveerse se utiliza include

    template
      El template que se renderiza para mostrar los errores en caso de que el
      request no sea xhr.
      Default: <model_name>_error_review.html

    mappings
      En caso de que el nombre del field en el form no corresponda al del modelo
      se indica la correspondencia en el diccionario mappings de la sig forma:
      {'nombre_en_el_modelo':'nombre_en_el_form'} form.cleaned_data
    """
    if settings.DEBUG:
        is_ajax = request.GET.has_key('xhr') or request.is_ajax
    else:
        is_ajax = request.is_ajax
    
    classname = modelclass._meta.module_name
    app_label = modelclass._meta.app_label

    if not success_redirect:
        success_redirect = '%s_posted/' % classname
    if not template:
        template = '%s_error_review.html' % classname

    if request.method == 'POST':
        form_data = request.POST
        form = formclass(form_data)

        if form.is_valid():
            print 'form_data', form.cleaned_data
            form_data = form.cleaned_data
            restr = {}
            
            for i in include:
                print 'restr', restr
                
                if mappings.has_key(i):
                    restr[i] = form_data[mappings[i]]
                else:
                    restr[i] = form_data[i]

            print restr

            if crud == 'create':
                print 'hola'
                modelclass.objects.create(**restr)
                print 'created'
            elif crud == 'update':
                if not restr.has_key('id'):
                    #Doesn't work with primary keys different than id.
                    raise Exception, "id must be present to update"
                modelclass(**restr).save()
            elif crud == 'delete':
                modelclass.objects.get(**restr).delete()
            else:
                raise Exception, "Not a valid crud type"
            return HttpResponseRedirect(success_redirect)
        else:
            if is_ajax:
                if not restrict_errors_to:
                    restrict_errors_to = include
                return JsonResponse(form, restricted_to=restrict_errors_to)
            else:
                return render_to_response(template, {'form' : form})
            
    else:
        if allow_get:
            return render_to_response(template, {'form' : form})
        else:
            if request.META.has_key('HTTP_REFERER'):
                return HttpResponseRedirect(request.META['HTTP_REFERER'])
            else:
                return HttpResponseRedirect(fail_redirect)

