from django.db import models
class ERetriever(models.Model):
    full_name = models.CharField(max_length=60,blank=True,null=True,verbose_name="Full Name")
    email = models.EmailField(blank=False,null=False,unique=True)
    contacted = models.BooleanField(default=False,blank=True)
    contacted_time = models.DateField(blank=True)
    registered = models.BooleanField(default=False,blank=True)
    blocked = models.BooleanField(default=False,blank=True)
