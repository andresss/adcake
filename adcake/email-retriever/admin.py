from invitation.models import InvitationKey
from models import ERetriever
from django.contrib import admin
import datetime
def make_contacted(modeladmin, request, queryset):
    queryset.update(contacted=True)
make_contacted.short_description = "Mark selected emails as contacted"

def email_sender(modeladmin,request,queryset):
    for q in queryset:
        if not q.blocked and not q.registered:
            invitation = InvitationKey.objects.create_invitation(request.user)
            invitation.send_to(q.email)
            q.contacted_time = datetime.date.today()
            q.contacted=True
            q.save()
email_sender.short_description = "Send email to selected"

class ERetrieverAdmin(admin.ModelAdmin):
    fields = ('email','full_name','contacted','contacted_time','registered','blocked')
    list_display  = ('email', 'full_name','registered', 'contacted','contacted_time','blocked')
    list_filter = ('contacted',)
    search_fields = ['email']
    actions = [make_contacted,email_sender]
admin.site.register(ERetriever, ERetrieverAdmin)

