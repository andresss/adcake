from django.contrib.admin.views.decorators import staff_member_required
from django.contrib.auth.models import User
from django.shortcuts import render_to_response
from django.template.context import RequestContext
from models import ERetriever
from forms import ERetrieverForm
import re
import urllib
import urlparse
from BeautifulSoup import BeautifulSoup
from urllib import urlopen
import os

class MyOpener(urllib.FancyURLopener):
    version = 'Mozilla/5.0 (Windows; U; Windows NT 6.1; en-US; rv:1.9.2.15) Gecko/20110303 Firefox/3.6.15'

def process(url):
    myopener = MyOpener()
    pages = []
    page = myopener.open(url)
    text = page.read()
    page.close()
    soup = BeautifulSoup(text)
    for tag in soup.findAll('a', href=True):
        tag['href'] = urlparse.urljoin(url, tag['href'])
        pages.append(tag['href'])
    return pages

def emailRetriever(theUrl):
    try:
        text = urlopen(theUrl).read()
    except:
        return []
    pattern = re.compile(
    r"[\w!#$%&amp;'*+/=?^_`{|}~-]+" +
    r"(?:\.[\w!#$%&amp;'*+/=?^_`{|}~-]+)*" +
    r"@(?:[a-z0-9](?:[\w-]*[\w])?\.)+" +
    r"(?:[\w^\d]{2}|" +
    r"com|org|net|gov|mil|biz|info|mobi|name|aero|jobs|museum)\b"
    )
    return set(pattern.findall(text))



@staff_member_required
def eretriever_list(request):
    if request.POST:
        form = ERetrieverForm(request.POST)
        if form.is_valid():
            url = form.cleaned_data['url']
            print url 
            urllist = process(url)
            emails =[]
            for url2 in urllist:
                os.write(1,".")
                emails.extend(emailRetriever(url2))
            for e in emails:
                if User.objects.filter(email=e):
                    if ERetriever.objects.filter(email=e):
                        pass
                    else:
                        er = ERetriever(email=e,contacted=True,registered=True)
                        er.save()
                else:
                    if ERetriever.objects.filter(email=e):
                        pass
                    else:
                        er = ERetriever(email=e,contacted=False,registered=False)
                        er.save()
            form = ERetrieverForm()

    else:
        form = ERetrieverForm()
    return render_to_response('admin/eretriever/list.html',{'form':form},context_instance=RequestContext(request))
  