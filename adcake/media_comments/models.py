from django.conf import settings
from django.contrib.comments.models import Comment
from helpers.utils import send_comment_to_mail
from django.contrib.comments.signals import comment_was_posted
def comment_handler(sender, comment, request, **kwargs):
    if not settings.DEBUG:
        try:
            if comment.user_id != comment.content_object.owner.id:
                send_comment_to_mail(comment,comment.content_object,comment.content_object.owner.email)
        except:
            pass

comment_was_posted.connect(comment_handler, sender=Comment)