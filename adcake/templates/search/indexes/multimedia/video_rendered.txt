<div class="wrapmayor">
<div class="wraptocenter">
  <span></span>
  <a href="{{ object.get_absolute_url }}" title="{{ object.title }}">{% load thumbnail %}{% if object.get_thumbnails.1 %}<img src="{% thumbnail object.get_thumbnails.1 80x80 %}" />{% else %}{% if object.get_thumbnails.0 %}<img src="{% thumbnail object.get_thumbnails.0 80x80 %}" />{% endif %}{% endif %}</a>
</div>
</div>
<div class="obj-info">
  <h2><a href="{{ object.get_absolute_url }}" title="{{ object.title }}" class="title">{{ object.title }}</a></h2>
  <p class="obj-views"><span>By: </span><a href="{{ object.owner.get_profile.get_absolute_url}}">{{ object.owner|capfirst }}</a></p>
  <p class="obj-views"><span>Type:</span> Video</p>
  <p class="obj-views"><span>{{ object.view_count }} view{{ object.view_count|pluralize }}<span></p>
</div>
<br class="clear" />
