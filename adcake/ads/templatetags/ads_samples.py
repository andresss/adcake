from django import template
from ads.models import Ad
register = template.Library()
import memcache
@register.inclusion_tag('ads/priority_sample.html')
def priority_sample(quantity):
    """
    Display a table with `quantity` random ads chosen by its plans.
    See AdsManager.priority_sample() for more details.

    Usage::
        {% load ads_samples %}
        {% priority_sample `quantity` %}
    """
    mc = memcache.Client(['127.0.0.1:11211'], debug=0)
    ads = mc.get("ads_sample")
    if not ads:
        ads = Ad.objects.priority_sample(quantity)
        mc.set("ads_sample",ads,400)
    return {'ads': ads}
