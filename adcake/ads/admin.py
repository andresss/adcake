from django.contrib import admin

from models import Ad

class AdAdmin(admin.ModelAdmin):
    list_display  = ('link','image_img','alt_text', 'email', 'telephones', 'public')
    list_filter   = ('public',)
    search_fields = ['link', 'email', 'alt_text']

    fieldsets = (
        (None, {
        'fields': ('link', 'alt_text', 'creative', 'plan', 'public'),
        }),
        ('Contact Information', {
        'fields': ('email', 'telephones'),
        'classes': ['collapse', 'extrapretty'],
        }),
    )

admin.site.register(Ad, AdAdmin)
2