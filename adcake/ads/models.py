from random import random
from sorl.thumbnail.main import DjangoThumbnail
from django.db import models

AD_PLANS = (
    ('1', 'Ejemplo'),
)

PRIORITIES_FRECUENCY = {
    '1': (0, 100),
#    '2': (50, 85),
#    '3': (85, 100),
}

class AdManager(models.Manager):
    def priority_sample(self, qty):
        """
        """
        ads = []
        for i in range(qty):
            # Get the plan
            rnd_plan = int(random() * 100)
            for plan, interval in PRIORITIES_FRECUENCY.iteritems():
                if interval[0] <= rnd_plan < interval[1]:
                    break
            # append a random ad with that plan.
            try:
                while True:
                    ad = self.filter(plan=plan,public=True).order_by('?')[0]
                    if ad not in ads:
                         ads.append(ad)
                         break
            except IndexError:
                pass
        return ads

class Ad(models.Model):
    link = models.URLField()
    alt_text = models.CharField(max_length=100, blank=True)
    creative = models.ImageField(upload_to='uploads/ads/',
                                 help_text='Standard ads must be 125x125')
    plan = models.CharField(max_length=5, choices=AD_PLANS, default='1')
    public = models.BooleanField(default=True)
    email = models.EmailField(blank=True, null=True)
    telephones = models.CharField(max_length=80, blank=True, null=True)
    objects = AdManager()

    def __unicode__(self):
        return 'Ad: %s' % self.link

    def image_img(self):
        if self.creative:
            return u'<img src="%s" />' % DjangoThumbnail(self.creative,(64,64),['crop']).absolute_url
        else:
            return '(No-Image)'

    image_img.short_description = 'Preview'
    image_img.allow_tags = True
