from django import forms
enquiry_choices = (
    ('I encountered a problem with the website','I encountered a problem with the website'),
    ('I\'m interested in advertising with you','I\'m interested in advertising with you'),
    ('I\'m a potential asset partner','I\'m a potential asset partner'),
    ('I have a press enquiry','I have a press enquiry'),
    ('I have a general enquiry','I have a general enquiry'),
    ('Recommend a new feature for ADCAKE','Recommend a new feature for ADCAKE'),
    ('Other','Other'),
)
class ContactForm(forms.Form):
    enquiry = forms.ChoiceField(choices=enquiry_choices,required=True)
    first_name = forms.CharField(required=True)
    last_name = forms.CharField(required=True)
    email = forms.EmailField(required=True)
    telephone = forms.CharField(required=False)
    company = forms.CharField(required=False)
    job_title = forms.CharField(required=False)
    message = forms.CharField(required=True,widget=forms.Textarea)
