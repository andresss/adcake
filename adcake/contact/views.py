from django.core.mail import send_mail
from django.shortcuts import render_to_response
from contact.forms import ContactForm
from django.template import RequestContext
def contact(request):
    if request.method == "POST":
        form = ContactForm(request.POST)
        if form.is_valid():
            cd = form.cleaned_data
            bodymsg = "Sent by:%s %s,\n\nEmail:%s" % (cd['first_name'],cd['last_name'],cd['email'])
            if cd['telephone']:
                bodymsg += "\nTelephone:%s" % cd['telephone']
            if cd['company']:
                bodymsg += "\nCompany:%s" % cd['company']
            if cd['job_title']:
                bodymsg += "\nJob Title:%s" % cd['job_title']
            bodymsg += "\n\n Message: %s" % cd['message']
            send_mail('New Enquiry:'+cd['enquiry'],bodymsg,cd['email'],['support@adcake.com','ctaloc@gmail.com','advertti@gmail.com'])
            return render_to_response('contact/contact_completed.html',context_instance=RequestContext(request))
        else:
            return render_to_response('contact/contact.html',{'form': form},context_instance=RequestContext(request))
    else:
        form = ContactForm()
        return render_to_response('contact/contact.html',{'form': form},context_instance=RequestContext(request))
