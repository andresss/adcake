from django.conf.urls.defaults import *
from views import *

urlpatterns = patterns('',
    url(r'^box/load/$', box_load, name='box_load'),
    url(r'^box/add/$', box_add, name='box_add'),
    url(r'^box/delete/$', box_delete, name='box_delete'),
    url(r'^box/update/$', box_update, name='box_update'),
    url(r'^box/save_item/$', box_save_item, name='box_save_item'),
    url(r'^box/update_item/$', box_update_item, name='box_update_item'),
    url(r'^box/delete_item/$', box_delete_item, name='box_delete_item'),
    url(r'^box/load_button/$', box_load_button, name='box_load_button'),
    url(r'^(?P<action>(followers|following))/(?P<username>[\w.@+-]+)/$', follow_view, name='follow_list_for'),
    url(r'^(?P<action>(followers|following))/(?P<username>[\w.@+-]+)/page(?P<page>[0-9]+)/$', follow_view, name='follow_list_page_for'),
    url(r'^contacts/reorder/$', contacts_reorder, name='contacts_reorder'),
    url(r'^contacts/add/$', contacts_add, name='contacts_add'),
    url(r'^contacts/delete/$', contacts_delete, name='contacts_delete'),
    url(r'^contacts/update/$', contacts_update, name='contacts_update'),
)
