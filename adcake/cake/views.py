from django.http import HttpResponseBadRequest, HttpResponseServerError
from django.contrib.contenttypes.models import ContentType
from django.views.decorators.http import require_POST
from django.views.generic.list_detail import object_list
from django.contrib.auth.models import User
from django.contrib.auth.decorators import login_required
from django.shortcuts import get_object_or_404
from django.utils import simplejson

from cake.models import Box, BoxItem, ContactList, ContactListMember
from contrib.json import JsonResponse
from helpers.decorators import require_XHR

@login_required
@require_POST
@require_XHR
def box_load(request):
    boxes_obj = Box.objects.filter(owner=request.user)
    boxes = []
    for box_obj in boxes_obj:
        box = {'label': box_obj.label, 'items': []}
        for item_obj in box_obj.items.all():
            if item_obj.content_object is None:
                continue
            item = {
                'id': item_obj.pk,
                'name': item_obj.__unicode__(),
                'url': item_obj.content_object.get_absolute_url(),
                'watch': item_obj.watch,
                }
            box['items'].append(item)
        boxes.append(box)
    return JsonResponse(boxes)

@login_required
@require_POST
@require_XHR
def box_add(request):
    label = request.POST.get('label', None)
    if label is None or label == "":
        return HttpResponseBadRequest("Incomplete request")
    box, created = Box.objects.get_or_create(owner=request.user,
                                             label=label)
    return JsonResponse({'label': box.label, 'created': created})

@login_required
@require_POST
@require_XHR
def box_delete(request):
    label = request.POST.get('label', None)
    if label is None or label == "":
        return HttpResponseBadRequest("Incomplete request")
    try:
        box = Box.objects.get(owner=request.user, label=label)
    except Box.DoesNotExist:
        return JsonResponse({'succeed': False})
    for item in box.items.all():
        item.delete()
    box.delete()
    return JsonResponse({'succeed': True})

@login_required
@require_POST
@require_XHR
def box_update(request):
    new_label = request.POST.get('new_label', None)
    old_label = request.POST.get('old_label', None)
    if new_label is None or old_label is None:
        return HttpResponseBadRequest("Incomplete request")
    try:
        box = Box.objects.get(owner=request.user, label=old_label)
    except Box.DoesNotExist:
        return JsonResponse({'succeed': False})
    newbox = Box.objects.filter(owner=request.user, label=new_label)
    if newbox:
        return JsonResponse({
            'succeed': False,
            'error':'You alredy have a box with this label'
            })
    box.label = new_label
    box.save()
    return JsonResponse({'succeed': True})

@login_required
@require_POST
@require_XHR
def box_save_item(request):
    ctype = request.POST.get('content_type', False)
    obj_id = request.POST.get('object_id', False)
    box_label = request.POST.get('box_label', False)
    if not ctype or not obj_id or not box_label:
        return HttpResponseBadRequest("Incomplete request")
    # Look for the box, if it doesn't exists we create it.
    box, box_created = Box.objects.get_or_create(owner=request.user,
                                                 label=box_label)
    from django.core.exceptions import ObjectDoesNotExist
    try:
        ctype = ContentType.objects.get(pk__exact=ctype)
        obj = ctype.get_object_for_this_type(pk__exact=obj_id)
    except ObjectDoesNotExist:
        return HttpResponseServerError()
    # add the new item using get_or_create to avoid duplicates
    item, item_created = box.items.get_or_create(content_type=ctype,
                                                 object_id=obj_id)
    # generate an organized response
    response_dict = {
        'box': {'label': box.label, 'created': box_created,},
        'item': {'id': item.pk,
                 'name': item.__unicode__(),
                 'url': item.content_object.get_absolute_url(),
                 'watch': False,
                 'created': item_created,},
        }
    return JsonResponse(response_dict)

@login_required
@require_POST
@require_XHR
def box_update_item(request):
    #starred = request.POST.get('starred', None)
    watch = request.POST.get('watch', None)
    item_id = request.POST.get('id')
    try:
        item = BoxItem.objects.get(pk__exact=item_id)
    except BoxItem.DoesNotExist:
        return JsonResponse({'succeed': False})
    #if starred is not None: item.starred = starred
    if watch is not None: item.watch = watch
    item.save()
    return JsonResponse({'succeed': True})

@login_required
@require_POST
@require_XHR
def box_delete_item(request):
    item_id = request.POST.get('id')
    try:
        item = BoxItem.objects.get(pk__exact=item_id)
    except BoxItem.DoesNotExist:
        return JsonResponse({'succeed': False})
    item.delete()
    return JsonResponse({'succeed': True})

@login_required
@require_POST
@require_XHR
def box_load_button(request):
    ctype  = request.POST.get('ctype', None)
    obj_id = request.POST.get('obj_id', None)
    if ctype is None or obj_id is None:
        return HttpResponseBadRequest("Incomplete request")
    from django.core.exceptions import ObjectDoesNotExist
    try:
        ctype = ContentType.objects.get(pk__exact=ctype)
        obj = ctype.get_object_for_this_type(pk__exact=obj_id)
    except ObjectDoesNotExist:
        return HttpResponseServerError()
    response_dict = {
        'succeed': True,
        'item': obj.__unicode__(),
        'boxes': list(request.user.box_set.values('label')),
        }
    return JsonResponse(response_dict)

@login_required
@require_POST
@require_XHR
def contacts_reorder(request):
    new_order = request.POST.get('new_order', None)
    if not new_order:
        return HttpResponseBadRequest("Incomplete request")
    for contact in simplejson.loads(new_order):
        try:
            c = ContactListMember.objects.get(pk__exact=contact['id'])
            c.position = contact['index']
            c.save()
        except ContactListMember.DoesNotExist:
            pass
    return JsonResponse('')

@login_required
@require_POST
@require_XHR
def contacts_add(request):
    user_id = request.POST.get('user_id')
    if not user_id:
        return HttpResponseBadRequest("Incomplete request")
    try:
        new_member = User.objects.get(pk=user_id)
    except User.DoesNotExist:
        return HttpResponseServerError()
    new_contact = ContactListMember(
        contact_list = request.user.get_profile().contact_list,
        member = new_member)
    try:
        new_contact.save()
    except:
        return JsonResponse({'succeed': False,})
    return JsonResponse({
        'succeed': True,
        'contact': {'id': new_contact.pk,
                    'uid': new_member.pk,
                    'username': new_member.username or '',
                    'name': new_member.get_profile().get_name(),
                    'profile_url': '/',}
        })

@login_required
@require_POST
@require_XHR
def contacts_delete(request):
    contact_id = request.POST.get('contact_id')
    if not contact_id:
        return HttpResponseBadRequest("Incomplete request")
    try:
        contact = ContactListMember.objects.get(pk=contact_id)
    except ContactListMember.DoesNotExist:
        return HttpResponseServerError()
    contact.delete()
    return JsonResponse('')

@login_required
@require_POST
@require_XHR
def contacts_update(request):
    contact_id = request.POST.get('contact_id')
    if not contact_id:
        return HttpResponseBadRequest("Incomplete request")
    try:
        contact = ContactListMember.objects.get(pk=contact_id)
    except ContactListMember.DoesNotExist:
        return HttpResponseServerError()
    contact.watch = not contact.watch
    contact.save()
    return JsonResponse('')

def follow_view(request, action, username,page=1):
    member = get_object_or_404(User, username=username)
    if action == 'followers':
        qs = ContactListMember.objects.get_followers_for(member)
        title = 'Your Followers' \
                if member.pk == request.user.pk \
                else 'Followers for %s' % member.get_profile().get_name()
    else:
        qs = member.get_profile().contact_list.members.all()
        title = 'Your Contacts' \
                if member.pk == request.user.pk \
                else '%s is following...' % member.get_profile().get_name()
    return object_list(request, queryset=qs, paginate_by=14,page=page,
                       template_object_name='follow',
                       template_name='cake/%s.html' % action,
                       extra_context={'member': member, 'title': title,
                                      'action': action})
