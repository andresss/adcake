from datetime import datetime
from django.db import models
from django.utils.translation import ugettext as _
from django.contrib.auth.models import User
from django.contrib.contenttypes.models import ContentType
from django.contrib.contenttypes import generic

class Cake(models.Model):
    owner = models.ForeignKey(User)
    last_edit = models.DateField(auto_now=True)

    class Meta:
        abstract = True
    
class CakeElement(models.Model):
    date_added = models.DateField(auto_now_add=True)
    watch = models.BooleanField(default=False)

    class Meta:
        abstract = True

class Box(Cake):
    label = models.CharField(max_length=30)

    def __unicode__(self):
        return '%s [%s\'s Cake Box]' % (self.label, self.owner.username)

    class Meta:
        verbose_name_plural = 'Boxes'
        unique_together = ('owner', 'label')

class ContactList(Cake):
    def save(self):
        if not self.pk and ContactList.objects.filter(owner=self.owner):
            from django.db import IntegrityError
            raise IntegrityError(_("Owner field must be unique"))
        super(ContactList,self).save()

    def __unicode__(self):
        return '%s\'s contact list' % self.owner.username

    class Meta:
        verbose_name_plural = 'Contact Lists'

class BoxItem(CakeElement):
    content_type = models.ForeignKey(ContentType)
    object_id = models.PositiveIntegerField('Object ID')
    content_object = generic.GenericForeignKey('content_type', 'object_id')
    box = models.ForeignKey(Box, related_name='items')

    def save(self, *args, **kwargs):
        self.box.last_edit = datetime.today()
        self.box.save()
        super(BoxItem, self).save(**kwargs)

    def __unicode__(self):
        return '%s' % self.content_object

class ContactListMemberManager(models.Manager):
    def get_member(self, owner, member):
        return self.get(contact_list=owner.get_profile().contact_list,
                        member=member)

    def get_followers_for(self, user):
        return self.filter(member=user)

class ContactListMember(CakeElement):
    contact_list = models.ForeignKey(ContactList, related_name='members')
    member = models.ForeignKey(User)
    position = models.IntegerField(max_length=5, editable=False)
    objects = ContactListMemberManager()

    def save(self, *args, **kwargs):
        if not self.position:
            last = ContactListMember.objects.filter(
                contact_list=self.contact_list).order_by('-position')
            if last:
                self.position = last[0].position + 1;
            else:
                self.position = 0;
        self.contact_list.last_edit = datetime.today()
        self.contact_list.save()
        super(ContactListMember, self).save(**kwargs)

    def __unicode__(self):
        return '%s on %s' % (self.member, self.contact_list)

    class Meta:
        unique_together = ('contact_list', 'member')
