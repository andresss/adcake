from django.contrib import admin
from django.contrib.contenttypes.generic import GenericTabularInline

from models import Box, BoxItem, ContactList, ContactListMember

class BoxItemAdmin(admin.TabularInline):
    model = BoxItem
    extra = 5

class ContactListMemberAdmin(admin.TabularInline):
    model = ContactListMember
    extra = 7

class BoxAdmin(admin.ModelAdmin):
    inlines = [BoxItemAdmin]

class ContactListAdmin(admin.ModelAdmin):
    inlines = [ContactListMemberAdmin]

admin.site.register(Box, BoxAdmin)
admin.site.register(ContactList, ContactListAdmin)
