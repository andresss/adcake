from django.template import Library
from django.core.urlresolvers import reverse
from django.contrib.contenttypes.models import ContentType

from cake.models import ContactListMember

register = Library()

@register.simple_tag
def add_to_cake_box(object):
    ctype = ContentType.objects.get_for_model(object)
    return '<a href="#" title="Add %s to the Cake Box" rel="%s:%s" class="add-cake-box">Add to my cake</a>' % (object, ctype.id, object.id)

@register.inclusion_tag('add_to_contact_list.html', takes_context=True)
def add_to_contact_list(context, member):
    user = context.get('user')
    try:
        contact = ContactListMember.objects.get_member(owner=user,member=member)
    except ContactListMember.DoesNotExist:
        contact = None
    except AttributeError:
        return {}
    return { 'user': user, 'member': member, 'contact': contact }
