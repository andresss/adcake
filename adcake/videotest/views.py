from django.shortcuts import render_to_response
from django.http import HttpResponseRedirect
import videotools.forms as videoforms
from django import forms

class VideoUploadForm(forms.Form):
    name = forms.CharField(max_length=50)
    video = videoforms.VideoField(video_name='video')
    

def all(request):
    if request.method == 'POST': # If the form has been submitted...
         form = VideoUploadForm(request.POST, request.FILES)
         if form.is_valid(): # All validation rules pass
            # Process the data in form.cleaned_data
            # ...
            print form['video'].data.file_location
            return HttpResponseRedirect('/video/') # Redirect after POST
    else:
        form = VideoUploadForm() # An unbound form

    return render_to_response('videotest/video.html', {
        'form': form,
    })
