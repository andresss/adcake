from django.db import models
from django.conf import settings
from django.contrib.contenttypes.models import ContentType
from django.contrib.contenttypes import generic

from sorl.thumbnail.fields import ImageWithThumbnailsField

class SpotlightManager(models.Manager):
    def get_public(self):
        return self.filter(public=True)

class Spotlight(models.Model):
    content_type = models.ForeignKey(ContentType)
    object_id = models.PositiveIntegerField()
    content_object = generic.GenericForeignKey('content_type', 'object_id')
    image = ImageWithThumbnailsField(upload_to=settings.SPOTLIGHT_UPLOAD_DIR,
        help_text='Images must have 330x184px', thumbnail={'size': (99,55),}, extra_thumbnails={'preview':  {'size': (64,64), 'options': ['upscale']}},)
    name = models.CharField(max_length=100)
    description = models.CharField(max_length=200)
    public = models.BooleanField(default=True)

    objects = SpotlightManager()

    def __unicode__(self):
        return 'Spotlight: %s' % self.name

    def image_img(self):
        if self.image:
            print  self.image
            return u'<img src="%s" />' % self.image.extra_thumbnails['preview']
        else:
            return '(No-Image)'

    image_img.short_description = 'Preview'
    image_img.allow_tags = True
