from django.contrib.contenttypes import generic
from models import Spotlight
from django.contrib import admin

class SpotlightInline(generic.GenericTabularInline):
    extra = 1
    model = Spotlight

class SpotlightAdmin(admin.ModelAdmin):
    list_display = ('name','image_img','public')
admin.site.register(Spotlight,SpotlightAdmin)