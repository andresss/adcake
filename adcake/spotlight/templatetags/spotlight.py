from django.template import Library
from ..models import Spotlight
import memcache

register = Library()

@register.inclusion_tag('spotlight.html')
def spotlight():
    """
    Shows spotlight widget with all the public featured elements
    """
    mc = memcache.Client(['127.0.0.1:11211'], debug=0)
    spotlights=mc.get("spotlights")
    if not spotlights:
        spotlights=Spotlight.objects.get_public().order_by('-id')
        mc.set("spotlights",spotlights,7200)
    return {'spotlight':spotlights,}

@register.inclusion_tag('spotlight_home.html')
def spotlight_home():
    """
    Shows spotlight widget with all the public featured elements
    """
    mc = memcache.Client(['127.0.0.1:11211'], debug=0)
    spotlights=mc.get("spotlights")
    if not spotlights:
        spotlights=Spotlight.objects.get_public().order_by('-id')
        mc.set("spotlights",spotlights,7200)
    return {'spotlight':spotlights,}
