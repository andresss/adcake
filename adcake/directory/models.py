from django.contrib.auth.models import User
from django.db import models
from django.template.defaultfilters import slugify
from os.path import basename,exists
from os import makedirs, rename,unlink
from tagging.fields import TagField
from django.conf import settings
from helpers.utils import clean_cache
class DataInconsistency(Exception):
    "Some Data Inconsistency"
    pass

class CategoryManager(models.Manager):
    def get_root_categories(self):
        return self.filter(level=0)

    def get_with_path(self, parent_slugs, slug):
        if parent_slugs is None:
            path = slug
        else:
            path = parent_slugs + slug
        return self.get(full_path=path)
        

class Category(models.Model):
    """
    Category Tree. Implemented as a loop-free (acyclic) graph.
    
    >>> Category.objects.all().delete()
    >>> a = Category(title='a a')
    >>> b = Category(title='b')
    >>> a.save()
    >>> b.save()
    >>> c = Category(title='c c', parent_category=b)
    >>> c.save()
    >>> d = Category(title='d', parent_category=a)
    >>> d.save()
    
    #Unicode returns info regarding the parents
    >>> Category.objects.all()
    [<Category: a a>, <Category: b>, <Category: b :: c c>, <Category: a a :: d>]

    #Slug does too
    >>> [i.slug for i in Category.objects.all()]
    [u'a-a', u'b', u'c-c', u'd']

    #Full-path shows the url path to the category
    >>> [i.get_absolute_url() for i in Category.objects.all()]
    [u'/directory/categories/a-a/', u'/directory/categories/b/', u'/directory/categories/b/c-c/', u'/directory/categories/a-a/d/']

    #The level is auto calculated 
    >>> d.level
    1

    #Moving a whole branch
    >>> b.parent_category = a
    >>> b.save()
    >>> Category.objects.all()
    [<Category: a a>, <Category: a a :: b>, <Category: a a :: b :: c c>, <Category: a a :: d>]

    #Using self as a parent
    >>> d = Category.objects.get(slug='d')
    >>> d.parent_category = d
    >>> d.save()
    Traceback (most recent call last):
    ...
    DataInconsistency: No loops are allowed. Cause: d is a descendant of self.

    #Using a descendant as a parent
    >>> b.parent_category = c
    >>> b.save()
    Traceback (most recent call last):
    ...
    DataInconsistency: No loops are allowed. Cause: c-c is a descendant of self.
    """
    title = models.CharField(max_length=50)
    slug = models.SlugField(unique=True, editable=False)
    description = models.TextField(null=True, blank=True)
    parent_category = models.ForeignKey('self', blank=True, null=True,
                                        related_name='children')
    full_path = models.CharField(max_length=150, editable=False)
    level = models.IntegerField(blank=True, editable=False)
    objects = CategoryManager()
    
    def save(self, force_insert=False, force_update=False):
        self.slug = slugify(self.title)
        self.full_path = self._get_full_path()
        if self.parent_category:
            if self.parent_category in self.get_children(include_self=True):
                raise DataInconsistency("No loops are allowed. "+
                                        "Cause: %s is a descendant of self." %
                                        self.parent_category.slug)
            if self.parent_category.level is not None:
                self.level = self.parent_category.level + 1
                super(Category, self).save(force_insert, force_update)
            else:
                raise DataInconsistency('Parent category not well formated')
        else:
            self.level = 0
            super(Category, self).save(force_insert, force_update)
        for child in self.children.all():
            child.save()

    def _get_full_path(self, separator=u'/', field='slug', include_me=True):
        if include_me:
            path = getattr(self, field)
        else: path = ""
        parent = self.parent_category
        while parent is not None:
            path = getattr(parent, field) + separator + path
            parent = parent.parent_category
        return path

    def __unicode__(self):
        return self._get_full_path(separator=' :: ', field='title')

    def get_children(self, include_self=False):
        if not self.id:
            if include_self:
                return [self]
            return []
        
        children = [self]
        for child in children:
            children += child.children.all()
        if include_self:
            return children
        else:
            return children[1:]

    def get_traceback(self, include_self=False):
        """Returns the predecesors of the category"""
        parent = self.parent_category
        if include_self:
            tb = [self]
        else:
            tb = []
        while parent is not None:
            tb.append(parent)
            parent = parent.parent_category
        return tb

    def get_absolute_url(self):
        return u'/directory/categories/' + self._get_full_path() + '/'
        
    class Meta:
        verbose_name_plural = "Categories"

class AreaStudy(models.Model):
    name = models.CharField(max_length=50, unique=True)
    def __unicode__(self):
        return self.name

class Discipline(models.Model):
    name = models.CharField(max_length=50, unique=True)

class Lenguage(models.Model):
    name = models.CharField(max_length=50, unique=True)
    def __unicode__(self):
        return self.name

class Service(models.Model):
    name = models.CharField(max_length=60, unique=True)
    def __unicode__(self):
        return self.name

def upload_to(attributeu,attributec):
    def upload_callback(instance, filename):
        u=getattr(instance, attributeu)
        id_company=getattr(instance,attributec)
        return 'uploads/users/%s/companies/%s/%s' % (u.id,id_company,filename)
    return upload_callback

class Company(models.Model):
    owner = models.ForeignKey(User, related_name='companies')
    name = models.CharField(max_length=50)
    uniquename = models.CharField(max_length=30, unique=True, help_text="Required. 30 characters or fewer. Letters, numbers and . - _ characters")
    slug = models.SlugField(unique=True)
    logo = models.ImageField(upload_to = 'uploads/tmp/compimages/')
    email = models.EmailField()
    category = models.ManyToManyField('Category', related_name='companies')
    services = models.ManyToManyField(Service)
    tags = TagField()
    description = models.TextField(blank=True)
    philosophy = models.TextField(blank=True)
    president = models.TextField(blank=True)
    directors = models.TextField(blank=True)
    partners = models.TextField(blank=True)
    creation_date = models.DateField(blank=True, null=True)
    telephones = models.CharField(max_length=80, blank=True)
    address = models.TextField(blank=True)
    website = models.URLField(blank=True, null=True)
    featured = models.BooleanField('Featured',default=False)
    def save(self, *args, **kwargs):
        super(Company, self).save(*args, **kwargs)
        self.uniquename = self.uniquename.lower()
        self.slug = self.uniquename
        tmp_path = self.logo.path
        tmp_filename = basename(tmp_path)
        tfe = tmp_filename.split('.')[-1]
        rel_final_path = 'uploads/users/%s/companies/%s/%s.%s' % (self.owner.id,self.id,self.id,tfe)
        final_path = settings.MEDIA_ROOT + rel_final_path
        folder = settings.MEDIA_ROOT + "uploads/users/%s/companies/%s/" % (self.owner.id,self.id)
        try:
            makedirs(folder)
        except OSError:
            pass
        if exists(final_path):
            if not tmp_path == final_path:
                unlink(final_path)
        if not tmp_path == final_path:
            rename(tmp_path,final_path)
            self.logo = rel_final_path
        clean_cache("latest_companies")
        super(Company, self).save(*args, **kwargs)
    def delete(self):
        clean_cache("latest_companies")
        if self.cportfolio:
            gals = self.cportfolio.galleries.all()
            if gals:
                for gal in gals:
                    if not gal.portfolio:
                        gal.delete()
                    else:
                        gal.cportfolio=None
                        gal.save()
        self.job_set.all().delete()
        super(Company, self).delete()
        
    def get_upload_to(self, field_attname):
        """Get upload_to path specific to this photo."""
        return 'uploads/images/companies/%d/logos' % self.id

    def some_cats(self):
        cats=self.category.all()
        count = len(cats)
        fc =''
        for counter,cat in enumerate(cats[:3]):
            if counter == 0:
                fc += "%s" % cat.title
            else:
                fc += ", %s" % cat.title
            if counter == 2 and count > 3:
                fc += "..."
        return fc


    @models.permalink
    def get_absolute_url(self):
        return ('cportfolio_for_shortcut', [self.slug])

    def __unicode__(self):
        return self.name

    class Meta:
        verbose_name_plural = "Companies"

class Client(models.Model):
    name = models.CharField(max_length=50)
    company = models.ForeignKey(Company, related_name='clients')
    initial_date = models.DateField()
    final_date = models.DateField(blank=True, null=True)

    def __unicode__(self):
        return self.name

    class Meta:
        unique_together = ('name', 'initial_date', 'final_date')

class Achievement(models.Model):
    description = models.TextField()
    company = models.ForeignKey(Company, related_name='achievements')
    date = models.DateField()
