from django import forms
from django.contrib.auth.models import User
import re
from directory.models import Service
from models import Category, Company
from django.utils.translation import ugettext_lazy as _
from accounts.models import ReservedName
from django.conf import settings

COMPANY_FIELDSETS = (
    ('Required Information', {'fields': ('name','uniquename','category','services', 'logo','email'), 'classes': ['inlineLabels']}),
    ('Additional Information', {'fields': ('creation_date','description', 'philosophy','telephones', 'address', 'website'), 'classes': ['inlineLabels']}),
)

class CompanyForm(forms.ModelForm):
    uniquename = forms.RegexField(label="Unique Company Name", max_length=30, regex=r'^[\w.-]+$',help_text = "Required. 30 characters or fewer. Letters, digits and .-_ only.",error_messages = {'invalid': "This value may contain only letters, numbers and .-_ characters and no spaces."})
    category = forms.ModelMultipleChoiceField(queryset=Category.objects.all(),help_text='Press control (CTRL or &#8984;) button for multiple selections.')
    services = forms.ModelMultipleChoiceField(queryset=Service.objects.all(),help_text='Press control (CTRL or &#8984;) button for multiple selections.')
    class Meta:
        model = Company

    def clean_uniquename(self):
        uniquename=self.cleaned_data['uniquename'].lower()
        if self.instance:
            if not self.instance.uniquename == uniquename and (Company.objects.filter(uniquename=uniquename) or ReservedName.objects.filter(reserved=uniquename) or User.objects.filter(username__iexact=uniquename)):
                raise forms.ValidationError('This Unique Company Name is already reserved, please choose a new one.')
            if not re.match('^[\w.-]+$',uniquename):
                raise forms.ValidationError('This value may contain only letters, numbers and .-_ characters.')
        else:
            if Company.objects.filter(uniquename=uniquename) or ReservedName.objects.filter(reserved=uniquename) or User.objects.filter(username__iexact=uniquename):
                raise forms.ValidationError('This Unique Company Name is already reserved, please choose a new one.')
            if not re.match('^[\w.-]+$',uniquename):
                raise forms.ValidationError('This value may contain only letters, numbers and .-_ characters.')
        return uniquename

    
    def clean_logo(self):
        file = self.cleaned_data['logo']
        if file:
            if len(file.name.split('.')) == 1:
                raise forms.ValidationError(_('File type is not supported'))
            file_split = file.name.split('.')
            extension = file_split[-1]
            if not extension in settings.IMAGE_UPLOAD_FILE_EXTENSIONS:
                raise forms.ValidationError(_('Image format not supported, use one of the following formats: png, jpeg'))
        return file



class CategoryForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        super(CategoryForm, self).__init__(*args, **kwargs)
        if self.instance:
            self.fields['parent_category'].queryset = \
                Category.objects.exclude(slug__in=[i.slug for i in
                           self.instance.get_children(include_self=True)])