from django.conf.urls.defaults import *
from django.views.generic.create_update import create_object

from views import search, category_list, company_admin, company_delete,category_companies_list,new_company,company_list
from forms import COMPANY_FIELDSETS,CompanyForm
from models import Company, Category

all_companies = {
    'queryset' :  Company.objects.all().order_by('name'),
    'template_name' : 'directory/company_list.html',
    'template_object_name': 'company',
    'paginate_by': 7
    }


company_detail = {
    'queryset' :  Company.objects.all(),
    'template_name' : 'directory/company_detail.html',
    'template_object_name': 'company',
    }


urlpatterns = patterns('django.views.generic',
                       url(r'^companies/$', 'simple.redirect_to', {'url': '/directory/companies/page1/'}),
                       url(r'^companies/page(?P<page>\d+)/$',company_list, name='company_index'),
                       url(r'^company/new/$', new_company, name='new_company'),
                       url(r'^company/admin/(?P<slug>[-.\w]+)/$', company_admin, name='company_admin'),
                       url(r'^company/delete/(?P<object_id>\d+)/$', company_delete, dict(model=Company, template_object_name='company'), name='company_confirm_delete'),
                       url(r'^company/(?P<slug>[-.\w]+)/$', 'list_detail.object_detail', company_detail, name='company_detail'),
                       url(r'^search/(?P<low>\d+)to(?P<high>\d+|N)/(?P<query>.+)', search),
                       url(r'^categories/(?P<slug>[-.\w]+)/page(?P<page>\d+)/$',category_companies_list,name='category_companies_list'),
                       url(r'^categories/(?P<parent_slugs>([-.\w]+/)*)(?P<slug>[-\w]+)/$', category_list, name='category_list'),
                       url(r'^categories/$', category_list, name='category_index'),
)
