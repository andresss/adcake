from haystack import indexes
from haystack.sites import site
from directory.models import Company

class CompanyIndex(indexes.SearchIndex):
    text = indexes.CharField(document=True, use_template=True)
    rendered = indexes.CharField(use_template=True, indexed=False)

site.register(Company, CompanyIndex)
