from django.core.urlresolvers import reverse
from django.http import HttpResponseBadRequest, HttpResponseRedirect
from django.views.generic.simple import direct_to_template
from django.views.generic.list_detail import object_list
from django.views.generic.create_update import update_object, delete_object
from django.contrib.auth.decorators import login_required
from django.shortcuts import get_object_or_404, render_to_response
from helpers.utils import JsonResponse
from forms import COMPANY_FIELDSETS,CompanyForm
from models import Category, Company
from django.template import RequestContext
from portfolio.models import CompanyPortfolio
from django import forms


def search(request, low, high, query):
    if request.POST:
        return HttpResponseBadRequest("Wrong request type")
    else:
        if high == 'N':
            return JsonResponse(list(Company.index.search(query)[int(low):]))
        else:
            return JsonResponse(list(Company.index.search(query)[int(low):int(high)]))

def category_list(request, parent_slugs='', slug=''):
    extra = {}
    if slug == '':
        qset = Category.objects.get_root_categories()
    else:
        extra['parent'] = Category.objects.get_with_path(parent_slugs, slug)
        qset = extra['parent'].children.all()
    return object_list(request, template_name='directory/category_list.html',
                       template_object_name='category', queryset=qset,
                       extra_context=extra)

def company_list(request,page=1):
    return object_list(request,template_name='directory/company_list.html',template_object_name='company',queryset=Company.objects.all().order_by('name'),paginate_by=7,page=page)

def category_companies_list(request,slug='',page=1):
    extra={}
    extra['slug'] = slug
    cat = Category.objects.get(slug=slug)
    qset = cat.companies.all()
    return object_list(request, template_name='directory/category_companies_list.html',template_object_name='company',paginate_by=7,page=page,queryset=qset,extra_context=extra)

@login_required
def company_admin(request, slug):
    """
    """
    company = get_object_or_404(Company, slug=slug)
    if company.owner != request.user:
        return direct_to_template('directory/company_admin_denied.html')
    extra = {'title': 'Administration for %s' % company.name,
             'status': 'updating',
             'fieldsets': COMPANY_FIELDSETS}
    return update_object(request,form_class=CompanyForm, model=Company, slug_field='slug', slug=slug,
                         login_required=True, template_object_name='company',
                         extra_context=extra)

@login_required
def new_company(request,*args,**kwargs):
    template = 'directory/company_form.html'
    if request.POST:
        companyform = CompanyForm(request.POST, request.FILES)
        if companyform.is_valid() and companyform.cleaned_data['owner'] == request.user and len(request.user.companies.all()) < 1:
            company = companyform.save()
            CompanyPortfolio(company=company).save()
            return HttpResponseRedirect(reverse('cportfolio_for_shortcut',args=[company.slug]))
    else:
        companyform = CompanyForm()
    context = {'form':companyform, 'fieldsets': COMPANY_FIELDSETS, 'title': 'Add a new company'}
    return render_to_response(template,context,context_instance=RequestContext(request))



@login_required
def company_delete(request, *args, **kwargs):
    """
    """
    company = get_object_or_404(Company, pk=kwargs['object_id'])
    if company.owner != request.user:
        return direct_to_template('directory/company_admin_denied.html')
    kwargs['extra_context'] = {
        'title': 'Are you sure you want to delete %s' % company.name
    }
    kwargs['post_delete_redirect'] = reverse('my_profile') + '?sec=companies'
    return delete_object(request, **kwargs)
