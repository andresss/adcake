from django.contrib import admin

from spotlight.admin import SpotlightInline
from tagging.forms import TagField

from forms import CategoryForm,CompanyForm
from models import Category, Company, Client, Achievement, Service, Discipline, AreaStudy

class CategoryAdmin(admin.ModelAdmin):
    form = CategoryForm
    ordering = ['parent_category__id', 'title']
    list_display = ('__unicode__', 'slug', 'description', 'level',)
    search_fields = ('^title', 'slug',)
    list_filter = ('level',)

class Client_Inline(admin.TabularInline):
    model = Client
    extra = 3

class Achievement_Inline(admin.StackedInline):
    model = Achievement
    extra = 2

class CompanyAdmin(admin.ModelAdmin):
    prepopulated_fields = {'uniquename': ('name',),}
    list_display = ('name', 'owner' , 'website',)
    search_fields = ('^name', 'owner__username', 'uniquename', 'logo', 'website',)
    ordering = ['name',]
    list_filter = ('category', 'creation_date',)

    inlines = [Client_Inline, Achievement_Inline, SpotlightInline]
    fieldsets = (
        (None, {
        'fields': ('owner', 'name', 'uniquename', 'email', 'logo', 'tags','featured'),
        }),
        ('Company Description', {
        'fields': ('category','description', 'philosophy', 'creation_date'),
        'classes': ['extrapretty'],
        }),
        ('Company Members', {
        'fields': ('president', 'directors', 'partners'),
        'classes': ['collapse'],
        }),
        ('Company Contact Info', {
        'fields': ('telephones', 'address', 'website'),
        'classes': ['collapse'],
        }),
        )

admin.site.register(Category, CategoryAdmin)
admin.site.register(Company, CompanyAdmin)
admin.site.register(Achievement)
admin.site.register(Service)
admin.site.register(Discipline)
admin.site.register(AreaStudy)


