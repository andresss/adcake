from django.contrib import admin
from models import Country,UsState,Institution

admin.site.register(Country)
admin.site.register(UsState)
admin.site.register(Institution)

