from django.conf.urls.defaults import *
from django.views.generic.simple import direct_to_template

from multimedia.models import Image
from models import Gallery

# Portfolio ----
urlpatterns = patterns('portfolio.views',
    url(r'edit/$', 'view_portfolio', name='my_portfolio'),
    url(r'xhr/reorder_branches/$', 'xhr_reorder_branches', name='portfolio_xhr_reorder_branches'),
    url(r'xhr/save_description/$', 'xhr_save_description', name='portfolio_xhr_save_description'),
    url(r'xhr/(?P<branch>(video|audio|gallery))/reorder/$', 'xhr_reorder_branch', name='portfolio_xhr_reorder_branch'),
    url(r'xhr/(?P<branch>(video|audio|gallery))/add_items/$', 'xhr_add', name='portfolio_xhr_add'),
    url(r'xhr/(?P<branch>(video|audio|gallery))/create_items/$', 'xhr_create', name='portfolio_xhr_create'),
    url(r'xhr/(?P<branch>(video|audio|gallery))/delete_item/$', 'xhr_delete', name='portfolio_xhr_delete'),
)

# Galleries ----
gallery_info = {
    'paginate_by': 12,
    'template_object_name': 'gallery',
}

gallery_delete = {
    'model': Gallery,
    'template_object_name': 'gallery',
}

urlpatterns += patterns('portfolio.views',
    url(r'(?P<username>[\w.-]+)/galleries/$', 'gallery_list', gallery_info, name='galleries_for'),
    url(r'(?P<username>[\w.-]+)/galleries/page(?P<page>[0-9]+)/$', 'gallery_list', gallery_info, name='galleries_page_for'),
    url(r'(?P<username>[\w.-]+)/galleries/delete/(?P<object_id>[0-9]+)/$', 'gallery_delete', gallery_delete, name='gallery_confirm_delete'),
    url(r'(?P<username>[\w.-]+)/galleries/delete/complete/$', direct_to_template, {'template': 'gallery_complete_delete.html'}, name='gallery_complete_delete'),
    # URL for 'add a new gallery'
    # URL for 'edit this gallery'
)

# Images ----
urlpatterns += patterns('portfolio.views',
    url(r'(?P<username>[\w.-]+)/galleries/(?P<slug>[\w\-]+)/$', 'gallery_detail', dict(slug_field='slug', template_object_name='gallery'), name='gallery_detail'),
    url(r'(?P<username>[\w.-]+)/images/(?P<image_id>\d+)/$', 'image_detail_portfolio', name='image_detail_portfolio'),
    url(r'(?P<username>[\w.-]+)/galleries/(?P<g_slug>[\w\-]+)/(?P<slug>[\w\-]+)/$', 'image_detail', dict(slug_field='slug', gallery_slug_field='slug', queryset=Image.objects.all(), template_name='portfolio/image_detail.html', template_object_name='image'), name='image_detail'),
    # URL for 'add a new image to this gallery'
    # URL for 'edit this image'
)

# Administration ----
urlpatterns += patterns('portfolio.views',
    url(r'gallery_detail/administration/$', 'gallery_detail_administration', name='gallery_detail_administration'),
    url(r'gallery_detail/delete_image/$', 'gallery_detail_delete_image', name='gallery_detail_delete_image'),
    url(r'video/delete/$', 'video_delete', name='video_delete'),
    url(r'gallery/delete/$', 'gallery_delete', name='gallery_delete'),
)



# Portfolio, user view
urlpatterns += patterns('portfolio.views',
    url(r'(?P<username>[\w.-]+)/$', 'view_portfolio', name='portfolio_for'),
)
