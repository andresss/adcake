from django import forms
from django.utils.translation import ugettext_lazy as _
from django.contrib.auth.models import User
from countries.countries.models import Country
from portfolio.models import CompanyPortfolio
from resume.models import Resume
from copy import copy
from models import Gallery
from multimedia.models import Video, Audio, AdCategory
from django.conf import settings
from django.template.defaultfilters import filesizeformat
from directory.models import Company
import oembed
from tagging.forms import TagField

GENDER_CHOICES = [('M', 'Male'), ('F', 'Female')]

mapping = {
    'gender': forms.fields.TypedChoiceField(choices=GENDER_CHOICES),
    'birth date': forms.fields.DateField(),
    'website': forms.fields.URLField()
    }

class PersonalInfoForm(forms.Form):
    subject = forms.CharField(max_length=100)

    def __init__(self, *args, **kwargs):
        user = kwargs.get('user', None)
        if user is None:
            raise Exception('keyword argument \'user\' must be provided')
        del kwargs['user']
        super(PersonalInfoForm, self).__init__(*args, **kwargs)
        resume = Resume.objects.get(user=user)
        for category, attrs in resume.personal_info.items():
            for key, value in attrs.items():
                self.fields[key] = copy(mapping.get(key, forms.CharField()))


class GalleryForm(forms.ModelForm):
    title = forms.CharField(max_length=50)
    adcategory = forms.ModelChoiceField(label=_('Advertising Category'),queryset=AdCategory.objects.all(),help_text=_('Chose the category that more fits more with this Gallery.'))
    agency = forms.CharField(label="Ad Agency",required=False)
    client = forms.CharField(label="Advertiser / Client",required=False)
    region = forms.ModelChoiceField(label="Region",queryset=Country.objects.all())
    def __init__(self, *args, **kwargs):
        user = kwargs.pop('user', None)
        cportfolio = kwargs.pop('cportfolio', None)
        super(GalleryForm, self).__init__(*args, **kwargs)
        if user:
            self.portfolio_id = user.portfolio.id
            cl=Company.objects.filter(owner=user)
            qs = CompanyPortfolio.objects.filter(company__in=list(cl))
            self.fields['portfolio'] = forms.BooleanField(initial=True,label="Add to your portfolio.")
            if len(qs) > 0:
                if cportfolio:
                    self.fields['cportfolio'] =forms.ModelChoiceField(label="Add Gallery to Company",queryset=qs,initial=cportfolio.company, empty_label="Select a Company",required=False)
                else:
                    self.fields['cportfolio'] =forms.ModelChoiceField(label="Add Gallery to Company",queryset=qs, empty_label="Select a Company",required=False)
            else:
                self.fields['cportfolio'] =forms.ModelChoiceField(label="Add Gallery to Company",queryset=qs, empty_label="Select a Company",required=False,widget=forms.HiddenInput)

    def clean_title(self):
         if self.portfolio_id is not None:
            if Gallery.objects.filter(portfolio=self.portfolio_id,title=self.cleaned_data['title']):
                raise forms.ValidationError(_('A gallery with this title already exists in your personal portfolio'))
            return self.cleaned_data['title']
         else:
            raise forms.ValidationError(_('You must specify a personal portfolio'))
    class Meta:
        model = Gallery
        exclude = ('cportfolio','portfolio', 'images', 'slug', 'pub_date', 'publish', 'view_count')
        
class GalleryForms(forms.ModelForm):
    def clean_title(self):
        if Gallery.objects.filter(title=self.cleaned_data['title']):
            raise forms.ValidationError(_('A gallery with this title already exists in your portfolio'))
        return self.cleaned_data['title']
    class Meta:
        model = Gallery
        exclude = ('portfolio', 'images', 'slug', 'pub_date', 'publish', 'view_count')

class GalleryEditForm(forms.ModelForm):
    adcategory = forms.ModelChoiceField(label=_('Advertising Category'),queryset=AdCategory.objects.all(),help_text=_('Chose the category that more fits more with this Gallery.'))
    def __init__(self, *args, **kwargs):
        user = kwargs.pop('user', None)
        instance = kwargs.get('instance')
        cportfolio = instance.cportfolio
        super(GalleryEditForm, self).__init__(*args, **kwargs)
        if user:
            self.portfolio_id = user.portfolio.id
            cl=Company.objects.filter(owner=user)
            qs = CompanyPortfolio.objects.filter(company__in=list(cl))
            self.fields['portfolio'] = forms.BooleanField(initial=True,label="Add to your portfolio.")
            if len(qs) > 0:
                if cportfolio:
                    self.fields['cportfolio'] =forms.ModelChoiceField(label="Add Gallery to Company",queryset=qs,initial=cportfolio.company, empty_label="No Company",required=False)
                else:
                    self.fields['cportfolio'] =forms.ModelChoiceField(label="Add Gallery to Company",queryset=qs, empty_label="No Company",required=False)
            else:
                self.fields['cportfolio'] =forms.ModelChoiceField(label="Add Gallery to Company",queryset=qs, empty_label="No Company",required=False,widget=forms.HiddenInput)

    def clean_title(self):
        gal = self.instance
        if gal.title == self.cleaned_data['title']:
            pass
        else:
            if Gallery.objects.filter(portfolio=gal.portfolio_id,title=self.cleaned_data['title']):
                raise forms.ValidationError(_('A gallery with this title already exists in your portfolio'))
        return self.cleaned_data['title']
    class Meta:
        model = Gallery
        fields = ('title','adcategory','agency','client','region','description')

class AudioForm(forms.ModelForm):
    class Meta:
        model = Audio
        exclude = ('added', 'length', 'author_set', 'owner', 'slug','view_count')

class VideoForm(forms.ModelForm):
    class Meta:
        model = Video
        exclude = ('added', 'length', 'original', 'thumbnail', 'thumb_times',
                   'author_set', 'owner', 'slug', 'view_count')

class VideoSuperForm(forms.ModelForm):

    role = forms.CharField(max_length=200, label=
                           _('How did you contribute to the creation of '+
                             'this content?'),
                           help_text=_('example: Context Designer'),
                           min_length=2)
    tags = forms.CharField(max_length=200,
                           help_text=_('Comma Separated , eg: video, animation'),
                           min_length=2)



    class Meta:
        model = Video
        fields = ('title','media','role','creation','tags')

        
class VideoAuthorFormWeb(forms.ModelForm):

    role = forms.CharField(max_length=200, label=
                           _('How did you contribute to the creation of this work?'),
                           help_text=_('Example: Producer, Art Director, Designer'),
                           min_length=2)
    tags = TagField(label="Tags",help_text="Tags separated by commas,eg: creativity,film,illustration",required=False)
    oembed = forms.URLField(label="Video URL",help_text="Import your video from Vimeo or Youtube. (e.g, http://www.youtube.com/watch?v=NDv4oPpj8vs)",required=True)

    adcategory = forms.ModelChoiceField(label=_('Advertising Category'),queryset=AdCategory.objects.all(),help_text=_('Choose the category that fits more with this video.'))
    agency = forms.CharField(label="Ad Agency",required=False)
    client = forms.CharField(label="Advertiser / Client",required=False)
    region = forms.ModelChoiceField(label="Region",queryset=Country.objects.all())
    def __init__(self, *args, **kwargs):
        user = kwargs.pop('user', None)
        super(VideoAuthorFormWeb, self).__init__(*args, **kwargs)
        if user:
            self.fields['portfolio'] = forms.BooleanField(initial=True,label="Include in your portfolio",required=False)
            qs = user.companies.all()
            if len(qs) > 0:
                self.fields['mcompany'] = forms.ModelMultipleChoiceField(label="Add this video to your companies",queryset=qs,widget=forms.CheckboxSelectMultiple,required=False)
            else:
                 self.fields['mcompany'] = forms.ModelMultipleChoiceField(label="Add this video to your companies",queryset=qs,widget=forms.HiddenInput,required=False)

    def clean(self):
        cleaned_data = self.cleaned_data
        mcompany = cleaned_data.get("mcompany")
        portfolio = cleaned_data.get("portfolio")
        if not portfolio and not mcompany:
            raise forms.ValidationError("You have to include the video in your portfolio or in one of your companies")
        return cleaned_data

    def clean_oembed(self):
        roembed = self.cleaned_data['oembed']
        if roembed.find("youtube.com") == -1 and roembed.find("vimeo.com") == -1:
            raise forms.ValidationError("the video source has to be from \"YOUTUBE\" or \"VIMEO\", example: http://www.youtube.com/watch?v=fOJqWLAcBcA")
        if roembed.find("user") != -1:
            video_id = roembed.split('/')[-1]
            roembed ="http://www.youtube.com/watch?v="+video_id
        if roembed.find("www.vimeo.com/") != -1:
            roembed = "http://vimeo.com/" + roembed.split('www.vimeo.com/')[-1]
        try:
            print "entro0"
            print roembed
            resource = oembed.site.embed(roembed)
            print "entro1"
            resource.get_data()
            print "entro2"
        except:
            raise forms.ValidationError("Something went wrong. We do not support Unlisted Url Videos. Please check your video setting and try again.")
        return roembed


    class Meta:
        model = Video
        fields = ('oembed','title','adcategory','agency','client','region','role','creation','tags')

    class Media:
        js = ('js/fancy_upload/FancyUpload2.js',
              'js/fancy_upload/Fx.ProgressBar.js',
              'js/fancy_upload/Swiff.Uploader.js',)

class VideoAuthorForm(forms.ModelForm):
    role = forms.CharField(max_length=200, label=
                           _('How did you contribute to the creation of '+
                             'this content?'),
                           help_text=_('example: Context Designer'),
                           min_length=2)
    tags = forms.CharField(max_length=200,
                           help_text=_('Separated by spaces, eg: video animation'),
                           min_length=2)
    media = forms.FileField(label= _('Video file'),help_text=_('Max. file size: 800MB. Supported video formats: WMV, MOV, MPEG-4, MPEG, AVI, FLV'),required=True)

    adcategory = forms.ModelChoiceField(label=_('Advertising Category'),queryset=AdCategory.objects.all(),help_text=_('Chose the category that more fits more with this video.'))
    agency = forms.CharField(label="Ad Agency",required=False)
    client = forms.CharField(label="Advertiser / Client",required=False)
    region = forms.ModelChoiceField(label="Region",queryset=Country.objects.all())
    def __init__(self, *args, **kwargs):
        user = kwargs.pop('user', None)
        super(VideoAuthorForm, self).__init__(*args, **kwargs)
        if user:
            self.fields['portfolio'] = forms.BooleanField(initial=True,label="Include in your portfolio",required=False)
            qs = user.companies.all()
            if len(qs) > 0:
                self.fields['mcompany'] = forms.ModelMultipleChoiceField(label="Add this video to your companies",queryset=qs,widget=forms.CheckboxSelectMultiple,required=False)
            else:
                 self.fields['mcompany'] = forms.ModelMultipleChoiceField(label="Add this video to your companies",queryset=qs,widget=forms.HiddenInput,required=False)



    def clean(self):
        cleaned_data = self.cleaned_data
        mcompany = cleaned_data.get("mcompany")
        portfolio = cleaned_data.get("portfolio")
        if not portfolio and not mcompany:
                raise forms.ValidationError("You have to include the video in your portfolio or in one of your companies")
        return cleaned_data

    def clean_media(self):
        file = self.cleaned_data['media']
        if file:
            file_type = file.content_type.split('/')[0]
            file_split = file.name.split('.')
            if len(file_split) == 1:
                raise forms.ValidationError(_('File type is not supported'))
            extension = file_split[-1]
            if not extension in settings.VIDEO_UPLOAD_FILE_EXTENSIONS:
                raise forms.ValidationError(_('Video format not supported, use one of the following formats: wmv, avi, mp4, flv, mov, mpeg'))
            if file_type in settings.VIDEO_UPLOAD_FILE_TYPES:
                if file._size > settings.VIDEO_UPLOAD_FILE_MAX_SIZE:
                    raise forms.ValidationError(_('Please keep filesize under %s. Current filesize %s') % (filesizeformat(settings.VIDEO_UPLOAD_FILE_MAX_SIZE), filesizeformat(file._size)))
            else:
                raise forms.ValidationError(_('File type is not supported'))
        return file

    class Meta:
        model = Video
        fields = ('title','media','adcategory','agency','client','region','role','creation','tags')

    class Media:
        js = ('js/fancy_upload/FancyUpload2.js',
              'js/fancy_upload/Fx.ProgressBar.js',
              'js/fancy_upload/Swiff.Uploader.js',)