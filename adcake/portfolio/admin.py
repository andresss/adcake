from django.contrib import admin

from spotlight.admin import SpotlightInline

from models import Portfolio, Gallery, CategoryGallery,PlayableElement

class Gallery_Inline(admin.StackedInline):
    model = Gallery
    extra = 3

class GalleryAdmin(admin.ModelAdmin):
    list_display = ('title', 'description', 'portfolio', 'pub_date', 'publish')
    list_filter = ['pub_date', 'publish']
    date_hierarchy = 'pub_date'
    prepopulated_fields = {'slug': ('title',)}
    filter_horizontal = ('images',)

    inlines = [SpotlightInline]

class PortfolioAdmin(admin.ModelAdmin):
    inlines = [Gallery_Inline]

class CategoryGalleryAdmin(admin.ModelAdmin):
    pass



admin.site.register(PlayableElement)
admin.site.register(Portfolio, PortfolioAdmin)
admin.site.register(CategoryGallery, CategoryGalleryAdmin)
admin.site.register(Gallery, CategoryGalleryAdmin)
