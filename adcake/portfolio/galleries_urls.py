from django.conf.urls.defaults import *
from portfolio.models import Gallery
from portfolio.forms import GalleryForm
from portfolio.views import create_gallery,gallery_all_list

urlpatterns = patterns('',

    url(r'page(?P<page>\d+)/$',gallery_all_list, name='gallery_index'),
    url(r'new/$', create_gallery, dict(form_class=GalleryForm, login_required=True, extra_context={'title': 'Add a new gallery'}), name='gallery_new'),
)
