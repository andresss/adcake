import random
from datetime import datetime
from django.db import models
from django.contrib.auth.models import User
from django.contrib.contenttypes.models import ContentType
from django.core.urlresolvers import reverse
from countries.countries.models import Country
from helpers.utils import check_clean_cache
from multimedia.models import Playable, Video, Audio, Image,AdCategory
from directory.models import Company


class NotAuthoringException(Exception):
    """
    An user can only add to his portfolio media that he claimed
    author role for
    """
    pass

class PlayableElement(models.Model):
    portfolio = models.ForeignKey('Portfolio', related_name='playables')
    playable = models.ForeignKey(Playable, related_name='portfolios')
    position = models.IntegerField(max_length=4, editable=False, blank=True,
                                   null=True)

class CompanyPortfolio(models.Model):
    company = models.OneToOneField(Company, related_name='cportfolio')
    playable = models.ManyToManyField(Playable,null=True)
    video_position = models.PositiveIntegerField(max_length=1, default=1,
                                                 editable=False)
    audio_position = models.PositiveIntegerField(max_length=1, default=3,
                                                 editable=False)
    gallery_position = models.PositiveIntegerField(max_length=1, default=2,
                                                   editable=False)
    def contains(self,media):
     try:
        args = {'%s__pk' % media._meta.object_name.lower(): media.pk,}
        new_play = self.playable.get(**args)
     except Playable.DoesNotExist:
        return False
     return True
    
    def sorted_branches(self):
        """
        """
        from forms import GalleryForm, VideoAuthorForm, AudioForm
        from helpers.forms import add_helpful_classes
        branches = [
            { 'name': 'video',
              'plural_name': 'videos',
              'position': self.video_position,
              'featured_list':self._get_playable_type('video'),
              'form': VideoAuthorForm(user=self.company.owner)},
            { 'name': 'gallery',
              'plural_name': 'galleries',
              'position': self.gallery_position,
              'featured_list': self.galleries.all(),
              'form': GalleryForm(user=self.company.owner,cportfolio=self)},
            ]
        def branch_compare(b1, b2):
            if b1['position'] > b2['position']:
                return 1
            elif b1['position'] == b2['position']:
                return 0
            else:
                return -1
        branches.sort(cmp=branch_compare)
        return branches

    def _get_playable_type(self, pl_type):
        """
        Return a list of the videos or audios featured on the portfolio.
        It doesnt return a queryset because there's no way to filter
        through table inheritance
        """
        playables = []
        for pl in self.playable.all():
            try:
                pl = getattr(pl, pl_type)
                playables.append(pl)
            except:
                continue
        return playables
    def __unicode__(self):
        return '%s' % self.company
    
class Portfolio(models.Model):
    # TODO: tests...
    owner = models.OneToOneField(User, related_name='portfolio')
    playable_set = models.ManyToManyField(Playable, through=PlayableElement,
                                          blank=True, null=True)
    description = models.TextField(blank=True, null=True)
    # For interface sortable branches
    video_position = models.PositiveIntegerField(max_length=1, default=1,
                                                 editable=False)
    audio_position = models.PositiveIntegerField(max_length=1, default=3,
                                                 editable=False)
    gallery_position = models.PositiveIntegerField(max_length=1, default=2,
                                                   editable=False)
    def _get_image_number(self):
        n = 0
        for gal in self.galleries.all():
            n = n + gal.image_count
        return n
    image_total = property(_get_image_number)
        
    def sorted_branches(self):
        """
        """
        from forms import GalleryForm, VideoAuthorForm, AudioForm, VideoAuthorFormWeb
        from helpers.forms import add_helpful_classes
        branches = [
            { 'name': 'video',
              'plural_name': 'videos',
              'all_link': reverse('videos_with', args=[self.owner.username, 1]),
              'position': self.video_position,
              'featured_list':self._get_playable_type('video'),
# Changed Temporally
#              'featured_list': self.videos,
              'unfeatured_list': self.unfeatured_videos, 
              'form': VideoAuthorFormWeb(user=self.owner)},

            { 'name': 'audio',
              'plural_name': 'audio tracks',
              'all_link': '',
              'position': self.audio_position,
              'featured_list': self.audios,
              'unfeatured_list': self.unfeatured_audios, 
              'form': AudioForm()},
            { 'name': 'gallery',
              'plural_name': 'galleries',
              'all_link': reverse('galleries_for', args=[self.owner.username]),
              'position': self.gallery_position,
              'featured_list': self.galleries.get_featured(),
              'unfeatured_list': self.galleries.get_unfeatured(), 
              'form': GalleryForm(user=self.owner) },
            ]
        def branch_compare(b1, b2):
            if b1['position'] > b2['position']:
                return 1
            elif b1['position'] == b2['position']:
                return 0
            else:
                return -1
        branches.sort(cmp=branch_compare)
        return branches

    def sorted_view_branches(self):
        """
        """
        from forms import GalleryForm, VideoAuthorForm, AudioForm, VideoAuthorFormWeb
        from helpers.forms import add_helpful_classes
        branches = [
            { 'name': 'video',
              'plural_name': 'videos',
              'all_link': reverse('videos_with', args=[self.owner.username, 1]),
              'position': self.video_position,
              #'featured_list':self._get_playable_type('video'),
              'featured_list':self.videos,
              'participation_list':self.participation_videos(),

            },
            { 'name': 'gallery',
              'plural_name': 'galleries',
              'all_link': reverse('galleries_for', args=[self.owner.username]),
              'position': self.gallery_position,
              #'featured_list': self.galleries.get_featured(),
              'featured_list': self.galleries.get_featured(),
             },
            ]
        def branch_compare(b1, b2):
            if b1['position'] > b2['position']:
                return 1
            elif b1['position'] == b2['position']:
                return 0
            else:
                return -1
        branches.sort(cmp=branch_compare)
        return branches

    def contains(self,media):
         try:
            args = {'playable__%s__pk' % media._meta.object_name.lower(): media.pk,}
            new_play = self.playables.get(**args)
         except PlayableElement.DoesNotExist:
            return False
         return True
    
    def set_featured_playable(self, media, position=None):
        """
        Sets the position in witch the playable has been added to the
        featured list.
        """
        if media._meta.module_name == 'gallery':
            media.set_as_featured(position=position)
            return media
        if position is None:
            try:
                last = self.playables.latest('position')
                position = last.position + 1
            except PlayableElement.DoesNotExist:
                position = 0
        try:
            args = {'playable__%s__pk' % media._meta.object_name.lower(): media.pk,}
            new_play = self.playables.get(**args)
            new_play.position = position
        except PlayableElement.DoesNotExist:
            new_play = PlayableElement(portfolio=self, playable=media,
                                       position=position)
        # Verify if the playable has an authorrole = portfolio.owner, if
        # it does then save the playable element, otherwise raise an exception
        if not new_play.playable.authors.filter(author=self.owner):
            raise NotAuthoringException()
        new_play.save()
        return new_play

    def unset_featured_playable(self, media):
        """
        Delete one playable from the featured list and restore the position
        in the rest of elements
        throws Playable.DoesNotExist
        """
        if media._meta.module_name == 'gallery':
            media.set_as_unfeatured()
            return media
        args = {'playable__%s__pk' % media._meta.object_name.lower(): media.pk,}
        playable = self.playables.get(**args)
        position = playable.position
        for pl in self.playables.order_by('-position'):
            if pl.position > position:
                pl.position = pl.position - 1
                pl.save()
            else:
                break
        playable.delete()
        return media
    def _get_playable_type(self, pl_type):
        """
        Return a list of the videos or audios featured on the portfolio.
        It doesnt return a queryset because there's no way to filter
        through table inheritance
        """
        playables = []
        for pl in self.playables.all():
            try:
                pl = getattr(pl.playable, pl_type)
                playables.append(pl)
            except:
                continue
        return playables
    def participation_videos(self):
        return Video.objects.filter(authors__author=self.owner).distinct().exclude(owner=self.owner)
    @property
    def videos(self):
        return self._get_playable_type('video')
        
    @property
    def audios(self):
        return self._get_playable_type('audio')
            
    def _not_featured_videos(self):
        """
        """
        return Video.objects.filter(
            authors__author=self.owner
            ).exclude(
            pk__in=[video.pk for video in self.videos]
            )

    def _not_featured_audios(self):
        """
        """
        return Audio.objects.filter(
            authors__author=self.owner
            ).exclude(
            pk__in=[audio.pk for audio in self.audios]
            )

    unfeatured_videos = property(_not_featured_videos)
    unfeatured_audios = property(_not_featured_audios)

    def __unicode__(self):
        return '%s\'s portfolio' % self.owner

    def get_absolute_url(self):
        pass

class GalleryManager(models.Manager):
    def get_published(self):
        return self.filter(publish=True)

    def get_featured(self):
        return self.filter(featured=True)

    def get_unfeatured(self):
        return self.filter(featured=False)
    def unempty(self):
        return self.filter()



class CategoryGallery(models.Model):
    name = models.CharField(max_length=100)
    slug = models.SlugField()
    def __unicode__(self):
        return self.name
    class Meta:
        verbose_name_plural = 'Category Galleries'


#
# TODO: implement GalleryUploadForm with zipfiles... see django-photologue
class Gallery(models.Model):
    title = models.CharField(max_length=100)
    slug = models.SlugField(help_text='Unique URL-friendly title for the\
    article.')
    agency = models.CharField("Ad Agency",max_length=60,null=True,blank=True)
    client = models.CharField("Advertiser / Client",max_length=60,null=True,blank=True)
    region = models.ForeignKey(Country,null=False,blank=False)
    adcategory = models.ForeignKey(AdCategory,null=False,blank=False)
    images = models.ManyToManyField(Image, related_name='galleries', blank=True,null=True)
    description = models.TextField(blank=True, null=True)
    portfolio = models.ForeignKey('Portfolio', related_name='galleries')
    cportfolio = models.ForeignKey(CompanyPortfolio,related_name='galleries',null=True,blank=True)
    featured = models.BooleanField(default=True, editable=False)
    position = models.IntegerField(max_length=4, editable=False, blank=True,
                                   null=True)
    pub_date = models.DateTimeField('Publication Date', default=datetime.now())
    publish = models.BooleanField(default=True)
    view_count = models.PositiveIntegerField(default=0)
    objects = GalleryManager()

    def delete(self):
        for i in self.images.all():
            if len(i.galleries.all()) == 1 and (i.owner.id == self.portfolio.owner.id or i.owner.id == self.cportfolio.company.owner.id):
                i.delete()
                check_clean_cache(i)
        super(Gallery, self).delete()
        
    def title_used(self,slug):
        for img in self.images.all():
            if img.slug == slug:
                return True
        return False

    def _image_count(self):
        """
        Number of images in this gallery
        """
        return self.images.count()
    image_count = property(_image_count)

    def _empty(self):
        return not self.images.exists()
    empty = property(_empty)

    
    def _more_images(self,limit=3):
        if self.image_count > 3:
            return True
        else:
            return False
    more_images = property(_more_images)

    def set_as_featured(self, position=None):
        """
        Sets a gallery as a featured element in its portfolio.
        If the position is not given, sets the position as the
        last gallery of the featured gallery list
        """
        if position is None:
            try:
                latest = Gallery.objects.latest('position')
                position = latest.position + 1
            except:
                position = 0
        self.position = position
        self.publish = True
        self.featured = True
        self.save()

    def set_as_unfeatured(self):
        """
        Sets a gallery as an unfeatured element in its portfolio.
        """
        self.featured = False
        self.save()

    @property
    def first_image(self):
        """
        Return the first image of the gallery
        """
        try:
            return self.images.all()[0]
        except:
            return None

    def latest(self, limit=5):
        """
        Return the latest images added to this gallery using <limit> to
        slice the queryset
        """
        if limit > self.image_count:
            limit = self.image_count
        return self.images.all()[:limit]

    def sample(self, limit=4):
        """
        Return a random sample of images of this gallery
        """
        if limit > self.image_count:
            limit = self.image_count
        return random.sample(self.images.all(), limit)

    def sample_filled(self, fill=3):
        """
        Return a random sample of images with a quantity
        of 'fill', If there are no enough images in the
        gallery to fullfil 'fill' the spaces left in blank
        will be filled with None
        """
        sample = self.sample(limit=fill)
        if len(sample) < fill:
            for i in range(len(sample), fill):
                sample.append(None)
        return sample

    @models.permalink
    def get_absolute_url(self):
        return ('gallery_detail', (), {
            'username': self.portfolio.owner.username,
            'slug': self.slug,
            })

    def __unicode__(self):
        return self.title

    class Meta:
        verbose_name_plural = 'Galleries'
        unique_together = ('title', 'portfolio')
        get_latest_by = 'pub_date'
        ordering = ['-id']

