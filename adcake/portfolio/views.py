from django.conf import settings
from django.http import Http404, HttpResponseRedirect
from django.utils.translation import ugettext
from django.core.urlresolvers import reverse
from django.utils.translation import ugettext as _
from django.views.generic.create_update import delete_object, apply_extra_context, redirect
from django.views.generic.list_detail import object_list, object_detail
from django.views.generic.simple import direct_to_template
from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User
from django.contrib.auth.views import redirect_to_login
from django.template import RequestContext, loader
from django.shortcuts import get_object_or_404
from endless_pagination.decorators import page_template
from contrib.json import JsonResponse
from multimedia.models import Video, Image, Audio
from multimedia.forms import ImageForm, ImageMetaForm
from django.contrib.comments import *
from django.shortcuts import render_to_response
from helpers.utils import check_clean_cache
from models import Gallery
from forms import PersonalInfoForm,GalleryEditForm
from xhr_views import *

def view_portfolio(request,username=None):
    extra = {}
    if request.POST:
        if request.POST['branch'] == 'video':
            if request.POST['type'] == "F":
                f = VideoAuthorForm(request.POST, request.FILES,user=request.user)
            else:
                f = VideoAuthorFormWeb(request.POST,user=request.user)
        else:
                f = GalleryForm(request.POST, request.FILES,user=request.user)
        if f.is_valid():
            obj = f.save(commit=False)
            if request.POST['branch'] == 'gallery':
                obj.slug = slugify(obj.title)
                if f.cleaned_data['cportfolio']:
                    obj.cportfolio=f.cleaned_data['cportfolio']
                obj.portfolio = request.user.portfolio
                obj.save()
            if request.POST['branch'] == 'video':
                obj.owner = request.user
                try:
                    obj.type = request.POST['type']
                    if request.POST['type']=="O":
                        obj.save(webvideo=True)
                    else:
                        obj.save()
                    AuthorRole(media_file=obj, author=request.user, authorized=True, role=f.cleaned_data['role']).save()
                    if f.cleaned_data['mcompany']:
                        for comp in f.cleaned_data['mcompany']:
                            comp.cportfolio.playable.add(obj)
                    if f.cleaned_data['portfolio']:
                        request.user.portfolio.set_featured_playable(obj)
                        return HttpResponseRedirect(reverse('video_detail',args=[obj.pk,obj.slug]))
                except Exception, e:
                    pass
        else:
            if request.POST['branch'] == "video":
                extra['formw'] = f
                extra['formg'] = GalleryForm(user=request.user)
                extra['portfolio'] = request.user.portfolio
            if request.POST['branch'] == "gallery":
                extra['formw'] = VideoAuthorFormWeb(user=request.user)
                extra['formg'] = f
                extra['portfolio'] = request.user.portfolio
            template = 'portfolio/edit.html'
            return direct_to_template(request, template=template,extra_context=extra)
    if username is None and request.user.is_authenticated():
        owner = request.user
        template = 'portfolio/edit.html'
        extra['personal_info_form'] = PersonalInfoForm(user=owner)
        extra['formw'] = VideoAuthorFormWeb(user=request.user)
        extra['formg'] = GalleryForm(user=request.user)
    elif username is None and not request.user.is_authenticated():
        return redirect_to_login(request.path)
    else:
        try:
            owner = User.objects.get(username=username,is_active=True)
            extra['owner'] = owner
            extra['profile']= owner.get_profile()
            extra['gallery_image_list'] = Image.objects.filter(owner=owner)
            extra['video_list'] = owner.portfolio.videos
        except User.DoesNotExist:
            raise Http404(_('Specified user does not exist.'))
        template = 'portfolio/view.html'
    #return render_to_response(template,dict(extra, portfolio=owner.portfolio),context_instance=RequestContext(request))
    return direct_to_template(request, template=template,extra_context=dict(extra, portfolio=owner.portfolio))


def gallery_list(request, *args, **kwargs):
    """
    Wrapper around django.views.generic.list_detail.object_list
    Verifies that the given user_id belongs to an active user and
    gets the QuerySet containing his galleries.
    """
    try:
        username = kwargs.pop('username')
        user = User.objects.get(username=username, is_active=True)
    except KeyError:
        raise AttributeError(_('gallery_list must be called with an user_id'))
    except User.DoesNotExist:
        raise Http404(_('Specified user does not exist.'))
    if user == request.user:
        queryset = user.portfolio.galleries.all()
    else:
        queryset = user.portfolio.galleries.get_published()
    if request.POST:
        form = GalleryForm(request.POST,user=request.user)
        if form.is_valid():
            gallery = form.save(commit=False)
            if form.cleaned_data['cportfolio']:
                gallery.cportfolio=form.cleaned_data['cportfolio']
            gallery.slug = slugify(gallery.title)
            gallery.portfolio = request.user.portfolio
            gallery.save()
            return HttpResponseRedirect(reverse('gallery_detail',args=[request.user.username,gallery.slug]))
    elif request.user.is_authenticated():
        form = GalleryForm(user=request.user)
    else:
        form = None
    extra = kwargs.pop('extra_context', {})
    extra['owner'] = user
    extra['form']  = form
    if request.GET and request.GET.has_key('del'):
        try:
            gl = Gallery.objects.get(pk=request.GET['del'])
        except Gallery.DoesNotExist:
            extra['gallery_deleted'] = True
    kwargs = dict(kwargs, extra_context=extra)
    if request.is_ajax():
        return JsonResponse(queryset)
    return object_list(request,queryset, **kwargs)




def gallery_all_list(request,page, *args, **kwargs):
    if request.user.is_authenticated() and request.POST:
        form = GalleryForm(request.POST,user=request.user)
        if form.is_valid():
            gallery = form.save(commit=False)
            if form.cleaned_data['cportfolio']:
                gallery.cportfolio=form.cleaned_data['cportfolio']
            gallery.slug = slugify(gallery.title)
            gallery.portfolio = request.user.portfolio
            gallery.save()
            return HttpResponseRedirect(reverse('gallery_detail',args=[request.user.username,gallery.slug]))
    elif request.user.is_authenticated():
        form = GalleryForm(user=request.user)
    else:
        form =[]
    extra = kwargs.pop('extra_context', {})
    extra['form']  = form
    extra['owner'] = request.user
    kwargs = dict(kwargs, extra_context=extra)
    order = request.GET.get('order')
    if order == 'views':
        #galleries = Gallery.objects.filter(images__isnull=False).order_by('-view_count')
        galleries = Gallery.objects.all().order_by('-view_count')
    elif order == 'date':
        galleries = Gallery.objects.all().order_by('-id')
    else:
        galleries = Gallery.objects.all().order_by('-view_count')
    return object_list(request,paginate_by=12,template_object_name="gallery",page=page,queryset=galleries,template_name='portfolio/gallery_whole_list.html', **kwargs)
def create_gallery(request, model=None, template_name=None,
        template_loader=loader, extra_context=None, post_save_redirect=None,
        login_required=False, context_processors=None, form_class=None):
    """
    Generic object-creation function.

    Templates: ``<app_label>/<model_name>_form.html``
    Context:
        form
            the form for the object
    """
    if extra_context is None: extra_context = {}
    if login_required and not request.user.is_authenticated():
        return redirect_to_login(request.path)

    model, form_class = Gallery, GalleryForm
    if request.method == 'POST':
        form = form_class(request.POST, request.FILES)
        if form.is_valid():
            gallery = form.save(commit=False)
            gallery.slug = slugify(gallery.title)
            gallery.portfolio = request.user.portfolio
            gallery.save()
            if request.user.is_authenticated():
                request.user.message_set.create(message=ugettext("The %(verbose_name)s was created successfully.") % {"verbose_name": model._meta.verbose_name})
            return redirect(post_save_redirect, gallery)
    else:
        form = form_class(user=request.user)

    # Create the template, context, response
    if not template_name:
        template_name = "%s/%s_form.html" % (model._meta.app_label, model._meta.object_name.lower())
    t = template_loader.get_template(template_name)
    c = RequestContext(request, {
        'form': form,
    }, context_processors)
    apply_extra_context(extra_context, c)
    return HttpResponse(t.render(c))


def gallery_detail(request, *args, **kwargs):
    """
    Wrapper around django.views.generic.list_detail.object_list
    Verifies that the given user_id belongs to an active user and
    gets the QuerySet containing his galleries.

    The wrapper also verifies that the given gallery slug represents
    a gallery that belongs to the user's galleries. If it doesn't raise Http404
    """
    try:
        username = kwargs.pop('username')
        user = User.objects.get(username=username, is_active=True)
    except KeyError:
        raise AttributeError(_('gallery_list must be called with a user_id.'))
    except User.DoesNotExist:
        raise Http404(_('Specified user does not exist.'))
    try:
        slug_field = kwargs.pop('slug_field', 'slug')
        gallery = user.portfolio.galleries.get(**{slug_field: kwargs['slug']})
    except KeyError:
        raise AttributeError(_('gallery_detail must be called with a gallery slug.'))
    except Gallery.DoesNotExist:
        raise Http404(_('The user %s does not have a gallery with slug %s' % (user.username, kwargs['slug'])))

    if request.method == "POST" and request.user.is_authenticated():
        if request.POST['form'] == 'add-image':
            new_image_form = ImageForm(request.POST, request.FILES,gallery=gallery)
            gallery_edit_form = GalleryEditForm(instance=gallery)
            if new_image_form.is_valid():
                image = new_image_form.save(commit=False)
                image.owner = request.user
                image.slug = slugify(image.title)
                image.save(gal_id=gallery.id)
                gallery.images.add(image)
                AuthorRole(media_file=image.media_ptr, author=request.user, authorized=True, role=new_image_form.cleaned_data['role']).save()
        elif request.user.is_authenticated():
            new_image_form = ImageForm(gallery=gallery)
            gallery_edit_form = GalleryEditForm(request.POST,instance=gallery,user=request.user)
            print gallery_edit_form
            if gallery_edit_form.is_valid():
                gall = gallery_edit_form.save(commit=False)
                if gallery_edit_form.cleaned_data['cportfolio']:
                    gall.cportfolio=gallery_edit_form.cleaned_data['cportfolio']
                elif not gallery_edit_form.cleaned_data['cportfolio']:
                    gall.cportfolio=None
                gall.slug = slugify(gall.title)
                edited = gall.save(force_update=True)
                return HttpResponseRedirect(reverse('gallery_detail', args=[user.username,gall.slug]))

    else:
        new_image_form = ImageForm(gallery=gallery)
        if request.user.is_authenticated():
            gallery_edit_form = GalleryEditForm(instance=gallery,user=request.user)
        else:
            gallery_edit_form = None

    if not kwargs.has_key('extra_context'):
        kwargs['extra_context'] = {}
    kwargs['extra_context']['owner'] = user
    kwargs['extra_context']['new_image_form'] = new_image_form
    kwargs['extra_context']['gallery_edit_form'] = gallery_edit_form
    if user == request.user:
        queryset = user.portfolio.galleries.all()
    else:
        queryset = user.portfolio.galleries.get_published()
        gallery.save()
    return object_detail(request, queryset, **kwargs)

@login_required
def gallery_delete2(request, *args, **kwargs):
    """
    """
    gallery = get_object_or_404(Gallery, pk=kwargs['object_id'])
    if gallery.portfolio.owner != request.user:
        return HttpResponseRedirect(reverse('homepage'))
    kwargs['post_delete_redirect'] = '%s?del=%s' % (
        reverse('galleries_for', args=[request.user.username]), gallery.pk)
    try:
        kwargs.pop('user_id')
    except:
        pass
    gallery.images.all().delete()
    return delete_object(request, **kwargs)

@page_template("portfolio/image_detail_page.html")
def image_detail(request,template="portfolio/image_detail.html", *args, **kwargs):
    """
    Wrapper around django.views.generic.list_detail.object_detail
    Verifies that the given user_id belongs to an active user and
    gets the QuerySet containing his galleries.

    The wrapper also verifies that the given gallery slug represents
    a gallery that belongs to the user's galleries. If it doesn't raise Http404.

    In addition, the wrapper verifies that the given image slug represents an
    image that belong to the gallery, and if it doesn't, also arise 404
    """
    try:
        username = kwargs.pop('username')
        user = User.objects.get(username=username, is_active=True)
    except KeyError:
        raise AttributeError(_('gallery_list must be called with a user_id.'))
    except User.DoesNotExist:
        raise Http404(_('Specified user does not exist.'))
    try:
        gallery_slug_field = kwargs.pop('gallery_slug_field', 'slug')
        gallery = user.portfolio.galleries.get(**{gallery_slug_field: kwargs['g_slug']})
    except KeyError:
        raise AttributeError(_('image_detail must be called with a gallery slug <g_slug>.'))
    except Gallery.DoesNotExist:
        raise Http404(_('The user %s does not have a gallery with slug %s' % (user.username, kwargs['g_slug'])))
    try:
        slug_field = kwargs.pop('slug_field', 'slug')
        img = gallery.images.get(**{slug_field: kwargs['slug']})
        co = ContentType.objects.get(app_label="multimedia",model="image")
        comments = Comment.objects.filter(object_pk=str(img.id),content_type=co)
    except KeyError:
        raise AttributeError(_('image_detail must be called with a image slug <slug>.'))
    except Image.DoesNotExist:
        raise Http404(_('The gallery %s does not have an image with slug %s' % (gallery.title, kwargs['slug'])))

    if request.method == "POST":
        form = ImageMetaForm(request.POST, instance=img)
        if form.is_valid():
            image = form.save(commit=False)
            image.owner = request.user
            image.slug = slugify(image.title)
            image.save()
            form.save_m2m()
            check_clean_cache(image)
            gallery.images.add(image)
            return HttpResponseRedirect(reverse('image_detail',
                                                args=[gallery.portfolio.owner.username,
                                                      gallery.slug,
                                                      image.slug]))
    else:
        form = ImageMetaForm(instance=img)
    try:
        g=img.galleries.all()[0]
        g.view_count+=1
        g.save()
    except:
        pass
    img.view_count += 1
    img.save()
    next = img.get_next_in_gallery(gallery)
    if next is not None:
        next = reverse('image_detail', args=[gallery.portfolio.owner.username,
                                             gallery.slug,
                                             next.slug])
    else: next = ''
    previous = img.get_previous_in_gallery(gallery)
    if previous is not None:
        previous = reverse('image_detail', args=[gallery.portfolio.owner.username,
                                                 gallery.slug,
                                                 previous.slug])
    else: previous = ''
    do_zoom=img.image.width > settings.IMAGE_SHOWCASE_SIZE[0] or img.image.height > settings.IMAGE_SHOWCASE_SIZE[1]
    zoom_dim=""
    if do_zoom:
        if img.image.width < 2000 or img.image.height < 1500:
            zoom_dim = "%sx%s" % (img.image.width,img.image.height)
    if gallery.cportfolio:
        company=gallery.cportfolio.company
    else:
        company=None
    context = {
        'gallery':gallery,
        'company':company,
        'portfolio':gallery.portfolio,
        'previous_image_url':previous,
        'next_image_url':next,
        'form':form,
        'image': img,
        'comments':comments,
        'page_template': page_template,
        'do_zoom' : do_zoom,
        'zoom_dim': zoom_dim,
    }
    return render_to_response(template,context,context_instance=RequestContext(request))

@page_template("portfolio/image_detail_page.html")
def image_detail_portfolio(request,template="portfolio/image_detail_portfolio.html", *args, **kwargs):
    """
    Wrapper around django.views.generic.list_detail.object_detail
    Verifies that the given user_id belongs to an active user and
    gets the QuerySet containing his galleries.

    The wrapper also verifies that the given gallery slug represents
    a gallery that belongs to the user's galleries. If it doesn't raise Http404.

    In addition, the wrapper verifies that the given image slug represents an
    image that belong to the gallery, and if it doesn't, also arise 404
    """
    try:
        username = kwargs.pop('username')
        image_id = kwargs.pop('image_id')
        user = User.objects.get(username=username, is_active=True)
    except KeyError:
        #raise AttributeError(_('gallery_list must be called with a user_id.'))
        Http404(_('Specified user does not exist.'))
    except User.DoesNotExist:
        raise Http404(_('Specified user does not exist.'))
    img = Image.objects.get(id=image_id)
    img.view_count += 1
    img.save()
    co = ContentType.objects.get(app_label="multimedia",model="image")
    comments = Comment.objects.filter(object_pk=str(img.id),content_type=co)
    next = img.get_next_general(owner=img.owner)
    if next is not None:
        next = reverse('image_detail_portfolio', args=[username,next.id])
    else: next = ''
    previous = img.get_previous_general(owner=img.owner)
    if previous is not None:
        previous = reverse('image_detail_portfolio', args=[username,previous.id])
    else: previous = ''
    do_zoom=img.image.width > settings.IMAGE_SHOWCASE_SIZE[0] or img.image.height > settings.IMAGE_SHOWCASE_SIZE[1]
    zoom_dim=""
    if do_zoom:
        if img.image.width < 2000 or img.image.height < 1500:
            zoom_dim = "%sx%s" % (img.image.width,img.image.height)
    print "entro"
    context = {
        'gallery':img.main_gallery(),
        'previous_image_url':previous,
        'next_image_url':next,
        'image': img,
        'comments':comments,
        'page_template': page_template,
        'do_zoom' : do_zoom,
        'zoom_dim': zoom_dim,
    }
    return render_to_response(template,context,context_instance=RequestContext(request))

