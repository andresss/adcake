from functools import wraps
from django.conf import settings
from django.db import IntegrityError
from django.http import HttpResponse, HttpResponseBadRequest
from django.template import loader, Context
from django.contrib.auth.models import User
from django.contrib.contenttypes.models import ContentType
from django.views.decorators.http import require_POST
from django.contrib.auth.decorators import login_required
from django.template.defaultfilters import slugify
from django.utils import simplejson
from django.core.context_processors import csrf
from contrib.json import JsonResponse
from helpers.decorators import require_XHR
from multimedia.models import Video, Image, Audio, AuthorRole
from forms import GalleryForm, VideoForm, VideoAuthorForm, AudioForm, GalleryForms,VideoAuthorFormWeb
from helpers.utils import check_clean_cache
from models import Gallery, PlayableElement
#
# PORTFOLIO
#
BRANCH_MODELS = {
    'video': Video, 'audio': Audio, 'gallery': Gallery,
}
BRANCH_FORMS = {
    'video': VideoAuthorForm, 'audio': AudioForm, 'gallery': GalleryForm,
}

def require_all(func):
    @wraps(func)
    @login_required
    @require_POST
    @require_XHR
    def decorator(*args, **kwargs):
        return func(*args, **kwargs)
    return decorator

@require_all
def xhr_reorder_branches(request):
    order = request.POST.get('new_order', None)
    if order is None:
        return HttpResponseBadRequest("Incomplete request")
    for br in simplejson.loads(order):
        setattr(request.user.portfolio, '%s_position' % br['name'], br['index'])
    request.user.portfolio.save()
    return JsonResponse('')

@require_all
def xhr_save_description(request):
    description = request.POST.get('description', '')
    request.user.portfolio.description = description
    request.user.portfolio.save()
    return JsonResponse('')

@require_all
def xhr_reorder_branch(request, branch):
    order = request.POST.get('new_order', None)
    if order is None:
        return HttpResponseBadRequest("Incomplete request")
    model = BRANCH_MODELS[branch]
    for item in simplejson.loads(order):
        try:
            work = model.objects.get(pk=item['id'])
            request.user.portfolio.set_featured_playable(work, item['index'])
        except model.DoesNotExist:
            pass
    return JsonResponse('')

@require_all
def xhr_add(request, branch):
    works = request.POST.get('works', '[]')
    model = BRANCH_MODELS[branch]
    items = {'featured_list': [],}
    for work in simplejson.loads(works):
        work = model.objects.get(pk=work)
        request.user.portfolio.set_featured_playable(work)
        items['featured_list'].append(work)
    c = Context({'branch': items, 'portfolio': request.user.portfolio, 'MEDIA_URL': settings.MEDIA_URL})
    t = loader.get_template('portfolio/lists/%s_list.html' % branch)
    return HttpResponse(t.render(c))

@require_all
def xhr_delete(request, branch):
    work_id = request.POST.get('id', None)
    if work_id is None:
        return HttpResponseBadRequest("Incomplete request")
    model = BRANCH_MODELS[branch]
    try:
        work = model.objects.get(pk=work_id)
    except model.DoesNotExist:
        return JsonResponse({'succeed': False})
    response = {'succeed': True, 'work_url': work.get_absolute_url(),
                'work_name': '%s' % work}
    request.user.portfolio.unset_featured_playable(work)
    return JsonResponse(response)

#
# GALLERIES
#
@require_all
def gallery_detail_administration(request):
    gallery_id = request.POST.get('gallery_id')
    publish = request.POST.get('publish')
    featured = request.POST.get('featured')
    if gallery_id is None:
        return HttpResponseBadRequest("Incomplete request")
    try:
        gallery = Gallery.objects.get(pk=gallery_id)
    except Gallery.DoesNotExist:
        return JsonResponse({'succeed': False})
    if publish is not None:
        gallery.publish = simplejson.loads(publish)
    if featured is not None:
        featured = simplejson.loads(featured)
        if featured:
            gallery.set_as_featured()
        else:
            request.user.portfolio.unset_featured_playable(gallery)
    gallery.save()
    return JsonResponse({'succeed': True})

@require_all
def gallery_detail_delete_image(request):
    image_id = request.POST.get('image_id')
    if image_id is None:
        return HttpResponseBadRequest("Incomplete request")
    try:
        image = Image.objects.get(pk=image_id)
    except Image.DoesNotExist:
        return JsonResponse({'succeed': False})
    image.delete()
    check_clean_cache(image)
    return JsonResponse({'succeed': True})

@require_all
def video_delete(request):
    video_id = request.POST.get('video_id')
    if video_id is None:
        return HttpResponseBadRequest("Incomplete request")
    try:
        video = Video.objects.get(pk=video_id)
    except video.DoesNotExist:
        return JsonResponse({'succeed': False})
    video.delete()
    return JsonResponse({'succeed': True})

@require_all
def gallery_delete(request):
    gallery_id = request.POST.get('gallery_id')
    if gallery_id is None:
        return HttpResponseBadRequest("Incomplete request")
    try:
        gallery = Gallery.objects.get(pk=gallery_id)
    except gallery.DoesNotExist:
        return JsonResponse({'succeed': False})
    gallery.delete()
    return JsonResponse({'succeed': True})

@login_required
@require_POST
def xhr_create(request, branch):
    if branch == 'video':
        if request.POST['type'] == "F":
            f = VideoAuthorForm(request.POST, request.FILES,user=request.user)
        else:
            f = VideoAuthorFormWeb(request.POST,user=request.user)
    else:
            f = GalleryForm(request.POST, request.FILES,user=request.user)
    if f.is_valid():
        obj = f.save(commit=False)
        if branch == 'gallery':
            obj.slug = slugify(obj.title)
            if f.cleaned_data['cportfolio']:
                obj.cportfolio=f.cleaned_data['cportfolio']
            obj.portfolio = request.user.portfolio
            obj.save()
        if branch == 'video':
            obj.owner = request.user
            try:
                obj.type = request.POST['type']
                if request.POST['type']=="O":
                    obj.save(webvideo=True)
                else:
                    obj.save()
                AuthorRole(media_file=obj, author=request.user, authorized=True, role=f.cleaned_data['role']).save()
                if f.cleaned_data['mcompany']:
                    for comp in f.cleaned_data['mcompany']:
                        comp.cportfolio.playable.add(obj)
                if f.cleaned_data['portfolio']:
                    request.user.portfolio.set_featured_playable(obj)
            except Exception, e:
                print e
                pass
        try:
            if branch == 'video':

                f = VideoAuthorForm(user=request.user)
                fw = VideoAuthorFormWeb(user=request.user)
                print "ENTRO sexta ETAPA"
                c = Context({'obj': obj, 'branch': branch, 'form': f, 'MEDIA_URL': settings.MEDIA_URL})
                print "ENTRO sexta ETAPA"
            else:
                f = GalleryForm(user=request.user)
                c = Context({'obj': obj, 'branch': branch, 'form': f, 'MEDIA_URL': settings.MEDIA_URL})
        except IntegrityError:
            c = Context({'branch': branch, 'form': f})
    else:
        print "Entro bien a donde debia"
        c = Context({'branch': branch, 'form': f, 'MEDIA_URL': settings.MEDIA_URL})
        c.update(csrf(request))
    t = loader.get_template('portfolio/creation.html')
    tr=t.render(c)
    return HttpResponse(tr)