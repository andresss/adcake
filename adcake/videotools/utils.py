from django.utils.translation import ugettext as _
from videotools.signals import convert_video
from django.db.models.signals import post_save

## used for remote connection S3
import mimetypes
from django.shortcuts import render_to_response
from django.conf import settings
from boto.s3.connection import S3Connection
from boto.s3.key import Key
from boto.s3.bucket import Bucket
## End S3

import commands, settings
import os
import time
import urllib

VIDEO_UPLOAD_PATH = 'uploads/video/'

def store_video_in_s3(filename_key,path):
    conn = S3Connection(settings.ACCESS_KEY, settings.PASS_KEY)
    b = Bucket(conn,"adcake")
    k = Key(b)
    k.key = filename_key
    k.set_metadata("Content-Type",'video/x-flv')
    k.set_contents_from_filename(path)
    k.set_acl("public-read")
    os.remove(path)

def delete_video_s3(filename_key):
    conn = S3Connection(settings.ACCESS_KEY, settings.PASS_KEY)
    b = Bucket(conn,"adcake")
    k = Key(b)
    k.key = filename_key
    b.delete_key(k)

class FFmpegError(Exception):
    "Error executing ffmpeg"
    pass

def get_length(original):
    print 'Getting length'
    print '--------------'
    ffmpeg = "ffmpeg -i %s 2>&1 | grep \"Duration\" | cut -d ' ' -f 4 | sed s/,//" % (settings.MEDIA_ROOT + original)
    print ffmpeg
    a = commands.getoutput(ffmpeg)

    if 'ERROR' in a:
        raise FFmpegError(_('Error getting the video length\n'))
    else:
        print "  length: %s" % a
        return a

def _get_seconds(length):
    l = length.split(':')
    return int(l[-1].split('.')[0]) + int(l[-2])*60 + int(l[-3])*3600


def generate_thumb(original, length):
    print 'Generating Thumbnails'
    print '---------------------'
    file_path = settings.MEDIA_ROOT + original
    thumb_path = '.'.join(file_path.split('.')[:-1])
    l = _get_seconds(length)
    cuts = []
    cuts.append(float("%.2f" % (l * 1.00/5)))
    cuts.append(float("%.2f" % (l * 2.00/5)))
    cuts.append(float("%.2f" % (l * 3.00/5)))
    cuts.append(float("%.2f" % (l * 4.00/5)))
    for n, i in enumerate(cuts):
        ffmpeg = "ffmpeg  -ss %s -i %s -vframes 1 -s 320x240 -y %s_thumb%s.jpg" % (i,file_path,thumb_path,n)
        print ffmpeg
        a = commands.getoutput(ffmpeg)

        if 'ERROR' in a or 'Could not' in a:
            raise FFmpegError(_('Error generating the thumbnail\n'))
    path = "%s_thumb%%s.jpg" % '.'.join(original.split('.')[:-1])
    nums = '0,1,2,3'
    return (path, nums)

def generate_thumb_web(video_id=None,url=None,destination=None):
    if destination:
        file_path = settings.MEDIA_ROOT + destination
        os.chdir(file_path)
        urllib.urlretrieve(url,filename=''.join(['videow',str(video_id),'_thumb0.',url.split('.')[-1]]))
        path = "%s_thumb%%s.jpg" % ''.join([destination,'videow',str(video_id)])
        nums = '0'
    else:
        path = "images/no-video-preview%%s.jpg"
        nums = '0'
    return (path, nums)

def save_video(sender, **kwargs):
    media = sender.media
    original = '%svideo%s.flv' % (VIDEO_UPLOAD_PATH, sender.pk)
    original_path = settings.MEDIA_ROOT + original
    #command_line = "ffmpeg -i %s -y -an -pass 1 -vcodec libx264 -threads 4 -b 1024kbps -flags +loop -cmp +chroma -partitions +parti4x4+partp8x8+partb8x8 -subq 1 -trellis 0 -refs 1 -bf 3 -b_strategy 1 -coder 1 -me_range 16 -g 250 -keyint_min 25 -sc_threshold 40 -i_qfactor 0.71 -rc_eq 'blurCplx^(1-qComp)' -qcomp 0.6 -qmin 10 -qmax 51 -qdiff 4 %s;ffmpeg -i %s -y -acodec libfaac -ab 128k -pass 2 -vcodec libx264 -threads 4 -b 1024kbps -flags +loop -cmp +chroma -partitions +parti4x4+partp8x8+partb8x8 -flags2 +mixed_refs -subq 5 -trellis 1 -refs 5 -bf 3 -b_strategy 1 -coder 1 -me_range 16 -g 250 -keyint_min 25 -sc_threshold 40 -i_qfactor 0.71 -rc_eq 'blurCplx^(1-qComp)' -qcomp 0.6 -qmin 10 -qmax 51 -qdiff 4 %s" % (media.path, original_path, media.path, original_path)
    command_line = "ffmpeg -i %s -sameq -f flv %s"% (media.path, original_path)
    a = commands.getoutput(command_line)
    print a
    if 'ERROR' in a:
        raise Exception(_('Error converting the video. Please ' +
                         'use one of the supported formats.'))
    return original

convert_video.connect(save_video, sender=None)
