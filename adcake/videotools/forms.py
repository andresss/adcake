from django.forms.fields import FileField
from django.core.files import temp as tempfile
from django.utils.translation import ugettext as _
from django.core.files.storage import FileSystemStorage 
import os, commands, settings
from django import forms
try:
    from cStringIO import StringIO
except ImportError:
    from StringIO import StringIO

class VideoField(FileField):
    """
    
    """
    def __init__(self, video_root=settings.MEDIA_ROOT, prefix='videos',
                 video_name=None, *args, **kwargs):
        self.video_root = video_root
        self.prefix = prefix
        self.video_name = video_name
        super(VideoField, self).__init__(*args, **kwargs)

    def clean(self, data, initial=None):
        super(VideoField, self).clean(data, initial)
        if not data:
            return data
        if not hasattr(data, 'temporary_file_path'):
            if hasattr(settings, 'FILE_UPLOAD_TEMP_DIR'):
                _file = tempfile.NamedTemporaryFile(suffix='.upload', dir=settings.FILE_UPLOAD_TEMP_DIR)
            else:
                _file = tempfile.NamedTemporaryFile(suffix='.upload')

            if hasattr(data, 'read'):
                _file.file.write(data.read())
            else:
                _file.file.write(data['content'])
            _file.file.flush()
            _file.file.close()
            data.temporary_file_path = lambda : _file.name
            
        path = data.temporary_file_path()

        if not self.video_name:
            self.video_name = os.path.split(path)[-1].split('.')[0]

        #convert the file to flv
        self._convert(path)

        print path
            
        #provide data to return (as file)
        data.file_location = "%s/%s/%s.flv" % (self.video_root.strip('/'),
                                               self.prefix.strip('/'),
                                               self.video_name.strip('/'))
        #import ipdb; ipdb.set_trace()
        print 'ok?'
        return data
        
    def _convert(self, path):
        s = FileSystemStorage(location=os.path.join(self.video_root, self.prefix))
        name = s.get_available_name(self.video_name)
        ffmpeg = "ffmpeg -i %s -ar 22050 -ab 32000 -f flv -s 320x240 - | flvtool2 -U stdin %s%s/%s.flv" % (path, self.video_root, self.prefix, name)
        print ffmpeg
        a = commands.getoutput(ffmpeg)
        if 'ERROR' in a:
            raise forms.ValidationError(_('Error converting the video. Please ' +
                                          'use one of the supported formats.'))
