import videotools.forms as videoforms
from django.db import models
from django.db.models.fields.files import FieldFile, FileField
from django.core.files.uploadedfile import UploadedFile
from django.core.files import File

class VideoFieldFile(FieldFile):
    
    def save(self, name, content, save=True):
        name = content.file_location.split('/')[-1]
        name = u'uploads/video/' + name
        self.storage.save(name, content)
        self._name = name
        setattr(self.instance, self.field.name, self.name)

        # Update the filesize cache
        self._size = len(content)
        
        # Save the object because it has changed, unless save is False
        if save:
            self.instance.save()
    save.alters_data = True


class VideoField(models.FileField):
    attr_class = VideoFieldFile

    def __init__(self, verbose_name=None, name=None, **kwargs):
        FileField.__init__(self, verbose_name, name, **kwargs)

    def save_form_data(self, instance, data):
        if data and isinstance(data, UploadedFile):
            getattr(instance, self.name).save(data.name, data, save=False)

    def formfield(self, **kwargs):
        defaults = {'form_class': videoforms.VideoField}
        defaults.update(kwargs)
        return super(VideoField, self).formfield(**defaults)
