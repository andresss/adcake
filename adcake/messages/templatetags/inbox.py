from django.template import Library, Node
from messages.models import Message

register = Library()     

class InboxOutput(Node):
    def render(self, context):
        user = context['user']
        count = user.received_messages.filter(read_at__isnull=True, recipient_deleted_at__isnull=True).count()
        return "%s" % (count)        
        
def do_print_inbox_count(parser, token):
    """
    A templatetag to show the unread-count for a logged in user.
    Prints the number of unread messages in the user's inbox.
    Usage::
        {% load inbox %}
        {% inbox_count %}
     
    """
    return InboxOutput()

@register.inclusion_tag('messages/inbox_table.html',
                        takes_context=True)
def inbox_table(context):
    message_list = Message.objects.inbox_for(context['user'],
                                             only_new=True)
    return { 'message_list': message_list, }

register.tag('inbox_count', do_print_inbox_count)
