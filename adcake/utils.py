from django.shortcuts import render_to_response
from django.template import RequestContext
from os.path import isdir
from shutil import rmtree
import subprocess, re


pattern = re.compile(r'Stream.*Video.*([0-9]{3,})x([0-9]{3,}).*')

def get_size(pathtovideo):
    x,y=0,0
    p = subprocess.Popen(['ffmpeg', '-i', pathtovideo],
                         stdout=subprocess.PIPE,
                         stderr=subprocess.PIPE)
    stdout, stderr = p.communicate()
    match = pattern.search(stderr)
    if match:
        x, y = map(int, match.groups())
    else:
        x = y = 0
    return x, y


def render_response(req, *args, **kwargs):
	kwargs['context_instance'] = RequestContext(req)
	return render_to_response(*args, **kwargs)

def removeDir(path):
    if isdir(path):
        rmtree(path)
