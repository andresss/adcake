from django.contrib.contenttypes.models import ContentType
from django.core.exceptions import ValidationError
from django.http import HttpResponseBadRequest
from functools import wraps
from django.views.decorators.http import require_POST
from django.contrib.auth.decorators import login_required
from accounts.models import UserNotification
from helpers.decorators import require_XHR
from contrib.json import JsonResponse
from jobs.models import Job, JobApply
from jobs.utils import job_apply_notification


def require_all(func):
    @wraps(func)
    @login_required
    @require_POST
    @require_XHR
    def decorator(*args, **kwargs):
        return func(*args, **kwargs)
    return decorator


@require_all
def xhr_delete_job(request):
        job_id = request.POST.get('job_id', None)
        if job_id is None:
            return HttpResponseBadRequest("Incomplete request")
        try:
            job = Job.objects.get(pk=job_id)
        except model.DoesNotExist:
            return JsonResponse({'succeed': False})
        response = {'succeed': True}
        if request.user == job.company.owner:
            job.delete()
        return JsonResponse(response)


@require_all
def xhr_apply_job(request):
    job_id = request.POST.get('job_id', None)
    try:
        job=Job.objects.get(id=job_id)
    except Job.DoesNotExist:
        response={'succeed':False}
        return JsonResponse(response)
    try:
        JobApply.objects.get(job=job,user=request.user)
    except JobApply.DoesNotExist:
        job_apply=JobApply(user=request.user,job=job)
        job_apply.save()
        job_apply_notification(job.company.owner,request.user,job)
        response={'succeed':True}
        return JsonResponse(response)
    response={'succeed':False}
    return JsonResponse(response)