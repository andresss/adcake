from django.contrib.auth.models import User
from django.contrib.contenttypes.models import ContentType
from django.template import Context, Template, loader
from accounts.models import UserNotification
from accounts.models import DEGREE_LVL
from messages.models import Message

def job_apply_notification(job_owner,applier,job):
#    content_type=ContentType.objects.get_for_model(job)
#    un=UserNotification(user=job_owner,notification="awdawd",content_type=content_type,object_id=job_id)
#    un.save()
    adcakeusr=User.objects.get(username__iexact="adcake")
    applier_profile=applier.get_profile()
    degree_lvl = dict(DEGREE_LVL)
    if applier_profile.degree:
        applier_degree= degree_lvl[applier_profile.degree]
    else:
        applier_degree=""
    companies=applier.companies.all()
    if companies:
        company=companies[0].uniquename
    else:
        company=None
    t = loader.get_template('jobs/apply/message_apply.html')
    c = Context({ 'job_title': job.title,'company': company,'username':applier.username,'full_name':applier.get_full_name(),'career':applier_profile.career,'degree':applier_degree,'languages':applier_profile._get_lenguages()})
    rendered = t.render(c)
    msg = Message(sender = adcakeusr,recipient = job_owner,subject = "Job Application",body = rendered)
    msg.save()