from models import Job
from django import forms
from directory.models import Category

JOB_FIELDSETS = (
    (None,{'fields':('title','expire_date','requirements','contact','country','category','State','place','company'), 'classes':['inlineLabels']}),
)

class JobForm(forms.ModelForm):
     def __init__(self, *args, **kwargs):
        user = kwargs.pop('user', None)
        super(JobForm, self).__init__(*args, **kwargs)
        if user:
            qs = user.companies.all()
            if len(qs) > 0:
                company_initial=qs[0]
            else:
                company_initial=None
            if len(qs) > 0:
                self.fields['company'] = forms.ModelChoiceField(label="Company",queryset=qs,initial=company_initial,required=True,empty_label="----")
     class Meta:
        model = Job

class JobEditForm(forms.ModelForm):
    class Meta:
        model = Job