from django.contrib.auth.models import User
from django.db import models
from countries.countries.models import Country,UsState
from directory.models import Category,Company 
class Job(models.Model):
    title = models.CharField(unique_for_date='pub_date',max_length=50,blank=False,null=False)
    pub_date = models.DateField(auto_now_add=True)
    expire_date = models.DateField(help_text="After this date passes, the post will be automatically deleted.")
    requirements = models.TextField()
    contact = models.TextField(verbose_name="Additional Information",null=True,blank=True)
    country = models.ForeignKey(Country,blank=False,null=False)
    category = models.ForeignKey(Category,blank=False,null=False)
    company = models.ForeignKey(Company,blank=False,null=False)
    State = models.CharField(max_length=40,blank=False,null=False)
    place = models.CharField(verbose_name="City",max_length=50,)
    def __unicode__(self):
        return self.title

    @models.permalink
    def get_absolute_url(self):
        return ('job_detail', [self.pk])


class JobApply(models.Model):
    job = models.ForeignKey(Job,unique=False,null=False)
    user = models.ForeignKey(User,unique=False,null=False)
    class Meta:
        unique_together = ('job', 'user',)
