from django.conf.urls.defaults import *
from jobs.models import Job
from jobs.views import new_job, job_detail, my_jobs, xhr_delete_job,xhr_apply_job
from views import category_list,category_jobs_list,region_list

all_jobs = {
    'queryset' :  Job.objects.all(),
    'template_name' : 'jobs/job_list.html',
    'template_object_name': 'job',
    'paginate_by': 10
    }

category_detail = {
        'template_name' : 'jobs/category_jobs_list.html',
        'template_object_name':'job',
}
urlpatterns = patterns('django.views.generic',
                       url(r'^$', 'simple.redirect_to', {'url': '/jobs/page1/'}),
                       url(r'^page(?P<page>\d+)/$', 'list_detail.object_list', all_jobs, name='jobs_index'),
                       url(r'^categories/$', category_list, name='category_index_jobs'),
                       url(r'^regions/$', region_list, name='region_index_jobs'),
                       url(r'^categories/(?P<slug>[-\w]+)/page(?P<page>\d+)/$',category_jobs_list,name='category_jobs_list'),
                       url(r'^new/$', new_job,name='new_job'),
                       url(r'^myposts/$','simple.redirect_to', {'url': '/jobs/myposts/page1/'}),
                       url(r'^myposts/page(?P<page>\d+)/$', my_jobs,name='my_jobs'),
                       url(r'^delete/$',xhr_delete_job,name='delete_job'),
                       url(r'^apply/$',xhr_apply_job,name='apply_job'),
                       url('(?P<object_id>\d+)/$',job_detail,name='job_detail'),
                       
)
