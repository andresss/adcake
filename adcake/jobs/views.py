# Create your views here.
from django.contrib.auth.decorators import login_required
from django.http import HttpResponseRedirect
from django.shortcuts import render_to_response, get_object_or_404
from django.template.context import RequestContext
from django.views.generic.create_update import create_object
from django.views.generic.list_detail import object_list, object_detail
from directory.models import Category
from countries.countries.models import Country
from jobs.forms import JobEditForm, JobForm
from jobs.models import Job
from xhr_views import *
from django.core.urlresolvers import reverse

@login_required
def new_job(request):
    context={'title':'Post a Job'}
    template='jobs/job_form.html'
    if request.POST:
        form = JobForm(request.POST,user=request.user)
        if form.is_valid():
            job = form.save()
            return HttpResponseRedirect(reverse('job_detail',args=[job.pk]))
        else:
            context['form']=form
    else:
        context['form']=JobForm(user=request.user)
    return render_to_response(template,context,context_instance=RequestContext(request))

@login_required()
def job_detail(request,object_id):
    context={}
    job=get_object_or_404(Job, pk=object_id)
    if request.user.is_authenticated():
        job_apply=JobApply.objects.filter(user=request.user,job=job)
        context['applied']=job_apply
    context['job']=job
    template='jobs/job_detail.html'
    if request.POST:
        form = JobEditForm(request.POST,instance=job)
        if form.is_valid():
            form.save()
            context['form']=JobEditForm(instance=job)
        else:
            context['form']=form
    else:
        context['form']=JobEditForm(instance=job)
    return render_to_response(template,context,context_instance=RequestContext(request))

@login_required()
def region_list(request):
    return object_list(request,template_name="jobs/region_list.html",template_object_name="region",queryset=Country.objects.all())
@login_required()
def category_list(request, parent_slugs='', slug=''):
    extra = {}
    if slug == '':
        qset = Category.objects.filter(job__isnull=False)
    else:
        extra['parent'] = Category.objects.get_with_path(parent_slugs, slug)
        qset = extra['parent'].children.all()
    return object_list(request, template_name='jobs/category_list.html',
                       template_object_name='category', queryset=qset,
                       extra_context=extra)
@login_required()
def my_jobs(request,page=1):
    try:
        company = request.user.companies.all()[0]
        qset=Job.objects.filter(company=company)
    except:
        qset = Job.objects.none()
    return object_list(request, template_name='jobs/my_jobs.html',template_object_name='job',paginate_by=10,page=page,queryset=qset)
    
322
@login_required
def category_jobs_list(request,slug='',page=1):
    cat = Category.objects.get(slug=slug)
    qset = cat.job_set.all()
    return object_list(request, template_name='jobs/category_jobs_list.html',template_object_name='job',paginate_by=1,page=page,queryset=qset,extra_context={'category':cat.title})