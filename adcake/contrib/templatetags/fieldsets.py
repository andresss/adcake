# -*- coding:utf-8 -*-
#
# Use the tag like this:
#
# {% draw_form form fieldsets %}
#
# Where 'form' is the form to be draw and 'fieldsets' is a tuple containing the
# fieldsets and the contained fields.
#
# Example on how to build the fieldsets parameter
#
# fiedsets = (
#     ('Personal Data', {'fields':('name','gender'), 'id':'personal_data', 'classes': ['some-class']}),
#     ('Address', {'fields':('street','number','city','zip_code'), 'id':'address', 'classes': ['some-other-class']}),
# )
#

from django.template import Library

from helpers.forms import add_helpful_classes

register = Library()

@register.inclusion_tag('uni_form/uni_form.html')
def draw_form(form, fieldsets=False, uni_form=False):
    if uni_form:
       add_helpful_classes(form)
    
    def get_fields_html(fields, f):
        fields_html = []
        append = fields_html.append
        for field_name in fields:
            field = f[field_name]
            cls = []
            help_text = ''
            errors = ''
            if f.fields[field_name].required:
                cls.append('required')
            if field.help_text:
                help_text = '<p class="formHint">%s</p>' % field.help_text
            if f[field_name].errors:
                errors = [error.title() for error in f[field_name].errors]
                cls.append('error')
            if uni_form:
                cls.append('ctrlHolder')
                if errors:
                    errFields = ['<li class="errorField"><strong>%s</strong></li>' % error for error in errors]
                    errHolder = '<ul class="errorHolder">%s</ul>' % ''.join(errFields)
                    errors = errHolder
            cls = ' class="%s"' % " ".join(cls)
            append('<li%s>%s<label for="%s">%s:</label> %s %s</li>' % (unicode(cls), unicode(errors), unicode(field_name), unicode(field.label), unicode(field), unicode(help_text)))
        return "".join(fields_html)
    
    form_html = []
    append_to_form = form_html.append
    form.auto_id = True
    
    fieldset_template = '<fieldset%(id)s%(class)s><legend>%(legend)s</legend><ul>%(fields)s</ul></fieldset>' 
    fieldset_template_no_legend = '<fieldset%(id)s%(class)s><ul>%(fields)s</ul></fieldset>' 
    
    if fieldsets:
        for fieldset in fieldsets:
            context = {}
            id = fieldset[1].get('id')
            if id:
                context['id'] = ' id="%s"' % id
            else:
                context['id'] = ''
            classes = fieldset[1].get('classes')
            if classes:
                context['class'] = ' class="%s"' % ' '.join(classes)
            else:
                context['class'] = ''
            context['legend'] = fieldset[0]
            fields = fieldset[1]['fields']
            context['fields'] = get_fields_html(fields, form)
            if context['legend'] is None:
                append_to_form(fieldset_template_no_legend % context)
            else:
                append_to_form(fieldset_template % context)
        return { 'form': form, 'fieldset_form': "".join(form_html) }
    else:
        fields = form.fields.keys()
        return { 'form': form, 'fieldset_form': get_fields_html(fields, form) }
