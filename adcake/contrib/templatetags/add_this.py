from django.template import Library

register = Library()

@register.inclusion_tag('add_this.html')
def add_this():
    return locals()
