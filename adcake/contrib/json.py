from django.core.serializers import serialize
from django.db.models.query import QuerySet
from django.http import HttpResponse
from django.utils import simplejson
from django.utils.functional import Promise
from django.utils.translation import force_unicode

# Lazy Serializer for dicts that have lazy strings like
# forms.errors...
class JSONLazyEncoder(simplejson.JSONEncoder):
    def default(self, o):
        if isinstance(o, Promise):
            return force_unicode(o)
        else:
            return super(JSONLazyEncoder, self).default(o)

class JsonResponse(HttpResponse):
    def __init__(self, object, fields=None):
        if isinstance(object, QuerySet):
            if fields is None:
                content = serialize('json', object)
            else:
                content = serialize('json', object, fields=fields)
        else:
            content = simplejson.dumps(object, cls=JSONLazyEncoder)
        super(JsonResponse, self).__init__(content, mimetype='application/javascript')
