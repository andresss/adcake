# Create your views here.
from directory.models import Category
from django.contrib.auth.models import User
from django.shortcuts import render_to_response
from django.contrib.auth.decorators import login_required
from django.contrib.auth.views import redirect_to_login
from django.views.generic.simple import direct_to_template
from django.views.generic.list_detail import object_list
from django.template import RequestContext
from django.core.urlresolvers import reverse
from django.http import HttpResponseBadRequest
from django.views.generic.simple import direct_to_template
from django.views.generic.list_detail import object_list
from django.views.generic.create_update import update_object, delete_object
from accounts.models import UserProfile
from endless_pagination.decorators import page_template


def category_list(request, parent_slugs='', slug=''):
    extra = {}
    if slug == '':
        qset = Category.objects.get_root_categories()
    else:
        extra['parent'] = Category.objects.get_with_path(parent_slugs, slug)
        qset = extra['parent'].children.all()
    return object_list(request,template_name='professionals/category_list.html',
                       template_object_name='category', queryset=qset,
                       extra_context=extra)


@page_template("professionals/professional_list_page.html")
def professional_list(request,template='professionals/professional_list.html',*args, **kwargs):
    order = request.GET.get('order')
    if order == 'name':
        professionals = UserProfile.objects.filter(user__is_active=True).order_by('user__username')
    elif order == 'date':
        professionals = UserProfile.objects.filter(user__is_active=True).order_by('user__date_joined')
    else:
        professionals = UserProfile.objects.filter(user__is_active=True)
    context = {
        'professionals':professionals,
        'page_template': page_template,
       }
    return render_to_response(template,context,context_instance=RequestContext(request))

def category_professionals_list(request,slug='',page=1):
    extra = {}
    extra['slug'] = slug
    cat = Category.objects.get(slug=slug)
    qset = cat.userprofile_set.all()
    return object_list(request, template_name='professionals/category_professionals_list.html',template_object_name='professional',paginate_by=12,page=page,queryset=qset)
