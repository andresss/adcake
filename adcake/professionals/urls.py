from django.contrib.auth.models import User
from django.conf.urls.defaults import *
from accounts.models import UserProfile
from views import category_list, category_professionals_list, professional_list
all_professionals = {
    'queryset' :  UserProfile.objects.all(),
    'template_name' : 'professionals/professional_list.html',
    'template_object_name': 'professional',
    'paginate_by': 2
    }

professional_detail = {
    'queryset' :  UserProfile.objects.all(),
    'template_name' : 'professionals/professional_detail.html',
    'template_object_name': 'professional',
    }

urlpatterns = patterns('django.views.generic',
                       url(r'^$',professional_list,name='professional_index'),
                       url(r'^categories/$', category_list, name='category_index_p'),
                       url(r'^categories/(?P<slug>[-\w]+)/$',category_professionals_list, name="category_professionals_list") ,
                       url(r'^categories/(?P<slug>[-\w]+)/page(?P<page>\d+)/$',category_professionals_list, name="category_professionals_list_page") ,
                       url(r'^categories/(?P<parent_slugs>([-\w]+/)*)(?P<slug>[-\w]+)/$', category_list, name='category_list_p'),
                       url(r'^professional/(?P<slug>[-\w]+)/$', 'list_detail.object_detail', professional_detail, name='professional_detail'),

)
