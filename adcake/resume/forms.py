from models import Category
from django import forms

class CategoryForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        super(CategoryForm, self).__init__(*args, **kwargs)
        try:
            unicode(self.instance)
            self.fields['parent_category'].queryset = Category.objects.exclude(id__in=[i.id for i in self.instance.get_children(include_self=True)])
        except:
            pass


class SearchForm(forms.Form):
    skill = forms.CharField(label='skill', widget=forms.DateTimeInput)
    level = forms.ChoiceField([(-1,'Indifferent'),(0, 'N/A'), (1,'Beginer'),
                               (2,'Intermediate'),(3,'Advanced'),
                               (5, 'Expert')],widget=forms.Select,initial=-1)
    dig = forms.BooleanField(initial=False, required=False, help_text="Search"+
                             " within the comments and/or descriptions", label="Deep search")
