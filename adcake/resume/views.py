from django.template.defaultfilters import force_escape
from django.forms.formsets import formset_factory
from django.views.generic.list_detail import object_list
from models import Category, Resume, CategorySkill
from django.shortcuts import render_to_response
from django.template import RequestContext
from django.contrib.auth.decorators import login_required
from django.views.decorators.http import require_POST
from django.http import HttpResponseForbidden
from contrib.json import JsonResponse
from helpers.decorators import require_XHR
from django.core.paginator import Paginator
from django.utils import simplejson
from forms import SearchForm
from decorators import require_owner
from django.contrib.auth.models import User
from django.conf import settings

@login_required
def personal_resume(request, template):
    r = Resume.objects.get(user=request.user)
    category_tree = Category.objects.filter(resume=r).as_items()
    return render_to_response(template, {'category_tree':category_tree,'resume':r},
                              context_instance=RequestContext(request))

@login_required
def resume_detail(request,template,username):
    u = User.objects.get(username=username)
    r = Resume.objects.get(user=u)
    category_tree = Category.objects.filter(resume=r).as_items()
    return render_to_response(template, {'category_tree':category_tree,'resume':r},
                              context_instance=RequestContext(request))   

@login_required
@require_POST
@require_owner(lambda request: Category.objects.get(id=request.POST['parent']))
def add_property(request):
    c = Category.objects.get(id=request.POST['parent'])
    skill = c.add_named_skill(request.POST['name'], request.POST['value'],
                              int(request.POST['level']), request.POST['comment'])
    return JsonResponse({'id': skill.id,
                         'comment': force_escape(skill.comment),
                         'level': skill.level,
                         'value': force_escape(skill.value),
                         'name': force_escape(skill.skill.name.capitalize())})

@login_required
@require_POST
@require_owner(lambda request: Category.objects.get(id=request.POST['parent']))
def add_skill(request):
    c = Category.objects.get(id=request.POST['parent']);
    skill = c.add_skill(request.POST['name'], int(request.POST['level']),
                        request.POST['comment'])
    return JsonResponse({'id': skill.id,
                         'level': skill.level,
                         'name': force_escape(skill.skill.name.capitalize())})

@login_required
@require_POST
@require_owner(lambda request: Category.objects.get(id=request.POST['parent']))
def add_description(request):
    c = Category.objects.get(id=request.POST['parent']);
    cat = c.add_description(request.POST['description'])
    return JsonResponse({'id': cat.id,
                         'description': force_escape(cat.description)})

@login_required
@require_POST
def add_subcategory(request):
    if request.POST.get('parent') == 'none':
        r = Resume.objects.get(user=request.user)
        c = r.add_category(request.POST['title'],
                           public=request.POST['public']=='true')
        return JsonResponse({'id': c.id,
                             'parent': 'none',
                             'title': force_escape(c.title)})
    
    c = Category.objects.get(id=request.POST['parent']);
    if c.get_owner() != request.user:
        return HttpResponseForbidden('Permission denied')
    cat = c.add_subcategory(request.POST['title'],
                            request.POST['public']=='true')
    return JsonResponse({'id': cat.id,
                         'parent': cat.parent_category.id,
                         'title': force_escape(cat.title)})

@login_required
@require_POST
@require_owner(lambda request: 
               CategorySkill.objects.get(id=request.POST['id']))
def delete_property(request):
    try:
        CategorySkill.objects.get(id=request.POST['id']).delete()
        return JsonResponse({'status': 'deleted'})
    except:
        return JsonResponse({'status': 'not deletable'})

delete_skill = delete_property

@login_required
@require_POST
@require_owner(lambda request: Category.objects.get(id=request.POST['id']))
def delete_category(request):
    try:
        Category.objects.get(id=request.POST['id']).delete()
        return JsonResponse({'status': 'deleted'})
    except:
        return JsonResponse({'status': 'not deletable'})

@login_required
@require_POST
def get_property(request):
    try:
        p = CategorySkill.objects.values('skill__name', 'value',
                                         'level', 'comment', 'deletable',
                                         'link').get(id=request.POST['id'])
        return JsonResponse(p)
    except:
        return JsonResponse('not found')


@login_required
@require_POST
@require_owner(lambda request: CategorySkill.objects.get(id=request.POST['id']))
def edit_property(request):
    try:
        p = CategorySkill.objects.get(id=request.POST['id'])
        s = p.skill
        if p.deletable:
            s.name = request.POST['name']
        s.save()
        p.value = request.POST['value']
        had_level = (int(p.level) != 0)
        p.level = request.POST['level']
        p.comment = request.POST['comment']
        p.save()
        return JsonResponse({'id': p.id,
                             'name': force_escape(p.skill.name[0].upper() + p.skill.name[1:]),
                             'value': force_escape(p.value),
                             'comment': force_escape(p.comment),
                             'level': p.level,
                             'had_level': had_level})
    except:
        return JsonResponse('not found')

@login_required
@require_POST
@require_XHR
def categories_reorder(request):
    new_order = request.POST.get('new_order', None)
    if not new_order:
        return HttpResponseBadRequest("Incomplete request")
    for category in simplejson.loads(new_order):
        try:
            c = Category.objects.get(pk__exact=category['id'].split('-')[1])
            c.position = category['index']
            c.save()
        except:
            pass
    return JsonResponse('')

@login_required
@require_POST
@require_XHR
def skills_reorder(request):
    new_order = request.POST.get('new_order', None)
    if not new_order:
        return HttpResponseBadRequest("Incomplete request")
    for skill in simplejson.loads(new_order):
        try:
            s = CategorySkill.objects.get(id=skill['id'])
            s.position = skill['index']
            s.save()
        except:
            pass

    return JsonResponse('')

@login_required
def search(request, page):
    if request.GET.get('form-TOTAL_FORMS'):
        from django.db.models import Q
        SearchFormSet = formset_factory(SearchForm)
        formset = SearchFormSet(request.GET)
        final_q = Q()
        patterns = []
        if formset.is_valid():
            for form in formset.cleaned_data:
                lookup = form.get('skill')
                if lookup:
                    patterns.append(lookup)
                    inner_q = Q(skill__name__contains=form['skill'])
                    if form['dig'] != True:
                        inner_q = inner_q | Q(comment__contains=form['skill'])
                        inner_q = inner_q | Q(value__contains=form['skill'])
                    if form['level'] != '-1':
                        inner_q = inner_q & Q(level=form['level'])
                    final_q = final_q | inner_q
        else:
            return render_to_response('resume/resume_list.html', 
                                      {'resume_list': None,
                                       'formset':formset},
                                      context_instance=RequestContext(request))
        skills = CategorySkill.objects.exclude(deletable=False, 
                                               value='Update your Information')
        skills = skills.filter(final_q)

        from collections import defaultdict
        d = defaultdict(int)
        for skill in skills:
            d[skill.category.resume] += 1

        #Ordering is not taking level into account for objects with the same
        #number of matches (To-Do: create a beter ordering key)
        rev_items = sorted([(v, k) for k, v in d.items()])
        
        class Result():
            pass

        all_resumes = []
        for count, resume in rev_items:
            result = Result()
            result.resume = resume
            result.count = count
            result.user = resume.user
            result.matches = []
            result.patterns = patterns
            for skill in (s for s in skills if s.category.resume == resume):
                result.matches.append((skill.skill.name,
                                       skill.comment,
                                       skill.value))
            all_resumes.append(result)
            
        paginator = Paginator(all_resumes, settings.RESUME_NUM_PER_PAGE)

        return render_to_response('resume/resume_list.html', 
                                  {'resume_list': all_resumes,
                                   'formset':None,
                                   'patterns':patterns,
                                   'paginator': paginator,
                                   'getvars': request.GET,
                                   'page': paginator.page(page)},
                                  context_instance=RequestContext(request))

    else:
        qset = Resume.objects.none()
        query = request.GET.get('query')
        if query:
            query = query.split()
            qset = Resume.objects.search(query)

        formset = formset_factory(SearchForm, extra=2)

        return object_list(request, template_name='resume/resume_list.html',
                           template_object_name='resume', queryset=qset, 
                           paginate_by=10, extra_context={'formset':formset})

