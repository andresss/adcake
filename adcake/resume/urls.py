from django.conf.urls.defaults import *
from views import *
from models import Resume

urlpatterns = patterns('django.views.generic',
        url('^edit/$', personal_resume, {'template':'resume/myresume.html'}, name='personal_resume'),
        url('^my-resume/$', personal_resume, {'template':'resume/myresume.html'}, name='owner_resume_detail'),
        url('^search/$', 'simple.redirect_to', {'url': '/resume/search/page1/'}, name="resume_index"),
        url('^search/page(?P<page>\d+)/$', search, name='search_resume'),
        url('^property/add/$', add_property, {}, name='add_property'),
        url('^skill/add/$', add_skill, {}, name='add_skill'),
        url('^description/add/$', add_description, {}, name='add_description'),
        url('^subcategory/add/$', add_subcategory, {}, name='add_subcategory'),
        url('^property/delete/$', delete_property, {}, name='delete_property'),
        url('^skill/delete/$', delete_skill, {}, name='delete_skill'),
        url('^category/delete/$', delete_category, {}, name='category_property'),
        url('^property/get/$', get_property, {}, name='get_property'),
        url('^property/edit/$', edit_property, {}, name='edit_property'),
        url('^categories/reorder/$', categories_reorder, {}, name='reorder_category'),
        url('^skills/reorder/$', skills_reorder, {}, name='reorder_skills'),
        url('^(?P<username>[\w.-]+)/$',resume_detail, {'template':'resume/resume_detail.html'}, name='resume_detail'),
)
