from django.db import models
from skills.models import Skill
from django.contrib.auth.models import User
from django.db.models.aggregates import Max
from django.utils.translation import ugettext_lazy as _
from django.db.models.query import QuerySet
from django.db.models import Q
import settings, operator

class DataInconsistency(Exception):
    "Some Data Inconsistency"
    pass
    
class ResumeManager(models.Manager):
    def search(self, keywords):
        ids = [val['category__resume'] for val in 
               CategorySkill.objects.search(keywords).
               values('category__resume').distinct()]
        return self.filter(id__in=ids)
    
class Resume(models.Model):
    user = models.ForeignKey(User, unique=True)
    objects = ResumeManager()

    def __unicode__(self):
        return _("%s's Resume") % self.user 
    
    def add_category(self, title, public=True, deletable=True):
        c = Category(resume=self, title=title, parent_category=None,
                     public=public, deletable=deletable)
        c.save()
        return c        
    
    def empty(self):
        categories =self.category_list.all()
        for c in categories:
            if not c.is_empty():
                return False
        return True
            

    @property
    def personal_info(self):
        return dict([(i, dict([(j.skill.name, (j.value, j))
                               for j in i.skill_set.exclude(
                                          value=None).filter(deletable=False)]))
                     for i in Category.objects.filter(resume=self,
                                                      deletable=False)])
    
class CategoryManager(models.Manager):
    def get_query_set(self):
        return self.model.QuerySet(self.model)

    def as_dict(self):
        return self.get_query_set().as_dict()

    def as_items(self):
        return self.get_query_set().as_items()
        
class Category(models.Model):
    resume = models.ForeignKey(Resume, related_name='category_list')
    title = models.CharField(max_length=100)
    parent_category = models.ForeignKey('self', blank=True, null=True,
                                        related_name='children')
    public = models.BooleanField(default=True)
    position = models.IntegerField(blank=True, default=0)
    description = models.TextField(blank=True)
    deletable = models.BooleanField(default=False)
    objects = CategoryManager()
    
    def __unicode__(self):
        return "<%s: %s / %s>" % (self.position, self.resume, self.title)
    
    def is_empty(self):
        return self.description == "<br>"
    def save(self, force_insert=False, force_update=False):
        if not self.position:
            self.position = (Category.objects.aggregate(
                    Max('position')).values()[0] or 0) + 1 # Agregates! yei!
        if self.parent_category is not None:
            if (self.parent_category.parent_category is not None):
                raise DataInconsistency('Only one inheritance level allowed')
        super(Category, self).save(force_insert, force_update)


    def delete(self):
        if not self.deletable:
            raise Exception('This instance is not deletable.'+
                            ' Add the deletable flag first.')
        super(Category, self).delete()

    def get_children(self, include_self=False, ordering=''):
        if not self.id:
            if include_self:
                return [self]
            return []
        
        children = [self]
        for child in children:
            children += child.children.all().order_by(ordering)

        if include_self:
            return children
        else:
            return children[1:]

    def add_named_skill(self, name, value, level=3, comment=None, deletable=True):
        sk = Skill.objects.add_skill(None, name, value, self, level, comment, deletable)
        return CategorySkill.objects.get(~Q(value=None), id=sk[0])

    def _get_paired_values(self):
        """
        """
        return self.skill_set.exclude(value=None).order_by('position')
    paired_values = property(_get_paired_values)

    def add_skill(self, name, level=3, comment=None):
        sk = Skill.objects.add_skill(None, name, None, self, level, comment)
        return CategorySkill.objects.get(pk=sk[0])

    def _get_skills(self):
        """
        """
        return self.skill_set.filter(value=None).order_by('position')
    skills = property(_get_skills)

    def add_description(self, description):
        self.description = description
        self.save()
        return self

    def add_subcategory(self, title, public):
        c = Category(resume=self.resume, title=title, parent_category=self,
                     public=public, deletable=True)
        c.save()
        return c

    def get_owner(self):
        return self.resume.user
                     
    class QuerySet(QuerySet):
        def as_dict(self):
            mapping = {}
            for category in self.filter(parent_category=None).order_by('position'):
                mapping[category] = category.get_children(ordering='position')
            return mapping

        def as_items(self):
            items = self.as_dict().items()
            items.sort(cmp=(lambda x,y: x[0].position > y[0].position
                            and 1 or -1))
            for item in items:
                item[1].sort(cmp=(lambda x,y: x.position > y.position
                                 and 1 or -1))
            return items
        
    class Meta:
        verbose_name_plural = _('Categories')


class CategorySkillManager(models.Manager):
    def skills(self):
        return self.order_by('position');

    def search(self, keywords, levels=[]):
        all = [(Q(skill__name__contains=keyword) |
                Q(value__contains=keyword)) for keyword in keywords]
        queryset = reduce(operator.__or__, all)
        return self.filter(queryset).filter(deletable=True)


class CategorySkill(models.Model):
    """
    Explicit ManyToMany relationship between a category and its skills.
    """
    category = models.ForeignKey(Category, related_name='skill_set')
    skill = models.ForeignKey(Skill, related_name='category_related')
    level = models.IntegerField(max_length=5, default='3')
    value = models.TextField(blank=True, null=True)
    comment = models.TextField(blank=True, null=True)
    position = models.IntegerField(blank=True, default=0)
    link = models.URLField(blank=True, null=True)
    deletable = models.BooleanField(default=True)
    objects = CategorySkillManager()
    
    def __unicode__(self):
        return '%s: %s(%s)' % (self.category, self.skill, self.level)

    def delete(self):
        if not self.deletable:
            raise Exception('This instance is not deletable.'+
                            ' Add the deletable flag first.')
        super(CategorySkill, self).delete()

    def save(self, force_insert=False, force_update=False):
        if not self.position:
            self.position = (CategorySkill.objects.aggregate(
                    Max('position')).values()[0] or 0) + 1 # Agregates! yei!
        super(CategorySkill, self).save()

    def get_owner(self):
        return self.category.resume.user
        
    class Meta:
        unique_together = ('category', 'skill', 'value')
        verbose_name = _('Category Skill')
        verbose_name_plural = _('Category Skills')
