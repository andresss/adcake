from django.http import HttpResponseForbidden

def require_owner(functions):
    """
    Decorator to make a view only accept execute only if the request
    comes form the resume owner, Usage::

        @require_owner(lambda request: critical_object)
        def some_view(request, *args, **kwargs):
            # Safe to modify the resume
            pass



        @require_owner([lambda request: critical_object1,
                        lambda request: critical_object2])
        def some_view(request, *args, **kwargs):
            # Safe to modify the resume
            pass


    Note that critical_object must provide a get_owner method.

    """
    def decorator(func):
        def wrapper(request, *args, **kwargs):
            if hasattr(functions, '__iter__'):
                for function in functions:
                    if function(request).get_owner() != request.user:
                        return HttpResponseForbidden('Permission denied')
            else:
                if functions(request).get_owner() != request.user:
                    return HttpResponseForbidden('Permission denied')
            return func(request, *args, **kwargs)
        return wrapper
    return decorator

