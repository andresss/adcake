from django.contrib import admin
from forms import CategoryForm
from models import Category, Resume, CategorySkill

class CategoryAdmin(admin.ModelAdmin):
    form = CategoryForm
    
class ResumeAdmin(admin.ModelAdmin):
    pass

class CategorySkillAdmin(admin.ModelAdmin):
    pass

admin.site.register(CategorySkill, CategorySkillAdmin)
admin.site.register(Category, CategoryAdmin)
admin.site.register(Resume, ResumeAdmin)

