from django import forms
from tinymce.widgets import TinyMCE
from models import Article

class ArticleAdminForm(forms.ModelForm):
    summary = forms.CharField(
        widget=TinyMCE(attrs={'cols': 100, 'rows': 15},
                       mce_attrs={'theme': 'simple',}),
        required=False)
    content = forms.CharField(widget=TinyMCE(attrs={'cols': 100, 'rows': 35}))
    
    class Meta:
        model = Article
