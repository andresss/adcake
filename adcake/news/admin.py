from django.contrib import admin

from spotlight.admin import SpotlightInline

from forms import ArticleAdminForm
from models import Article, Document, Image

class ImageAdmin(admin.TabularInline):
    model = Image
    extra = 3

class DocumentAdmin(admin.TabularInline):
    model = Document
    extra = 3

class ArticleAdmin(admin.ModelAdmin):
    form = ArticleAdminForm
    prepopulated_fields = {'slug': ('headline',)}
    date_hierarchy = 'pub_date'
    list_display = ('headline', 'subtitle', 'summary', 'pub_date', 'last_modified')
    list_filter = ['pub_date', 'last_modified', 'publish', 'author']
    search_fields = ['author', 'headline', 'subtitle', 'summary', 'content']
    save_as = True
    save_on_top = True

    inlines = [ImageAdmin, DocumentAdmin, SpotlightInline]
    fieldsets = (
        ('Article', {
        'fields': ('author', 'headline', 'slug', 'subtitle', 'summary', 'content', 'tags'),
        'classes': ['extrapretty'],
        }),
        ('Article options', {
        'fields': ('thumbnail', 'type', 'companies', 'publish', 'pub_date', 'last_modified'),
        }),
        )

admin.site.register(Article, ArticleAdmin)
