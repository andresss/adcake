from django.conf.urls.defaults import *
from django.views.generic.list_detail import object_list

from news.models import *
from tagging.views import tagged_object_list

published_articles = Article.objects.get_published()

news_info = {
    'queryset'   : published_articles,
    'date_field' : 'pub_date',
    'allow_empty': False,
    'template_object_name': 'article',
}

news_archive_info = {
    'queryset': published_articles,
    'paginate_by': 10,
    'allow_empty': False,
    'template_object_name': 'article',
}

article_detail_info = {
    'queryset': published_articles,
    'slug_field': 'slug',
    'date_field': 'pub_date',
    'template_object_name': 'article',
}

urlpatterns = patterns('django.views.generic.date_based',
    url(r'^$', 'archive_index', dict(news_info, template_object_name='article_list'), name='news_index'),
    url(r'^(?P<year>\d{4})/?$', 'archive_year', dict(news_info, make_object_list=True), name='news_year_archive'),
    url(r'^(?P<year>\d{4})/(?P<month>[a-z]{3})/$', 'archive_month', news_info, name='news_month_archive'),
    url(r'^(?P<year>\d{4})/(?P<week>\d{1,2})/$', 'archive_week', news_info, name='news_week_archive'),
    url(r'^(?P<year>\d{4})/(?P<month>[a-z]{3})/(?P<day>\d{2})/$', 'archive_day', news_info, name='news_day_archive'),
    url(r'^(?P<year>\d{4})/(?P<month>[a-z]{3})/(?P<day>\d{2})/(?P<slug>[\w-]+)/$', 'object_detail', article_detail_info, name='news_article_detail'),
    url(r'^archive/$', object_list, news_archive_info, name='news_archive'),
    url(r'^archive/page(?P<page>[0-9]+)/$', object_list, news_archive_info, name='news_archive_page'),
    url(r'^today/$', 'archive_today', news_info, name='news_today_archive'),
    url(r'^tags/(?P<tag>\w+)/page(?P<page>[0-9]+)/$', tagged_object_list, dict(queryset_or_model=published_articles, paginate_by=10, template_object_name='article'), name='news_for_tag'),
)
