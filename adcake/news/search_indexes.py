from haystack import indexes
from haystack.sites import site
from news.models import Article

class ArticleIndex(indexes.SearchIndex):
    text = indexes.CharField(document=True, use_template=True)
    rendered = indexes.CharField(use_template=True, indexed=False)

site.register(Article, ArticleIndex)
