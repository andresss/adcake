import os
from datetime import datetime

from django.db import models
from django.conf import settings
from django.contrib.auth.models import User

from tagging.fields import TagField
from directory.models import Company

TYPE = (
    ('1', 'Admin'),
    ('2', 'Event'),
    )

class ArticleManager(models.Manager):
    def get_published(self):
        return self.filter(publish=True)

    def get_drafts(self):
        return self.filter(publish=False)

class Article(models.Model):
    author = models.ForeignKey(User)
    headline = models.CharField(max_length=150, unique_for_date='pub_date')
    subtitle = models.CharField(max_length=150, blank=True)
    slug = models.SlugField(help_text='Unique URL-friendly title for the\
    article.', unique_for_date='pub_date')
    summary = models.TextField(help_text="A single paragraph summary or\
    preview of the article.", blank=True) 
    content = models.TextField()
    thumbnail = models.ImageField(help_text='Thumbnails must have 64x64px\
    max.', upload_to=settings.NEWS_UPLOAD_DIR+'/thumbs/%Y/%m/%d', blank=True,
    null=True)
    tags = TagField()
    companies = models.ManyToManyField(Company, null=True, blank=True)
    type = models.CharField(max_length=2, choices=TYPE, blank=True)
    pub_date = models.DateTimeField('Publication date', default=datetime.now(),
                                    help_text='Articles publish in the future\
                                    will not be included in the page.')
    last_modified = models.DateTimeField(default=datetime.now(),
                                         help_text='This field is\
                                         auto-generated.')
    publish = models.BooleanField(default=True)
    objects = ArticleManager()

    @models.permalink
    def get_absolute_url(self):
        return ('news_article_detail', (), {
            'year': self.pub_date.year,
            'month': self.pub_date.strftime('%b').lower(),
            'day': self.pub_date.strftime('%d'),
            'slug': self.slug,
            })

    def save(self):
        self.last_modified = datetime.now()
        super(Article, self).save()

    def __unicode__(self):
        return self.headline

class Document(models.Model):
    document = models.FileField(upload_to = settings.NEWS_UPLOAD_DIR +
                                '/documents/%Y/%m/%d')
    title = models.CharField(max_length=50)
    summary = models.CharField(max_length=100)
    article = models.ForeignKey(Article)

    def __unicode__(self):
        return self.title

    def _get_url(self):
        return self.document.url
    url = property(_get_url)

##     def get_absolute_url(self):
##         args = self.pub_date.strftime("%Y/%m/%d").lower().split("/") + [self.slug]
##         return reverse('pr-document-detail', args=args)

    def doc_dir(self):
        return os.path.dirname(self.get_file_filename())

    def remove_dirs(self):
        if os.path.isdir(self.doc_dir()):
            if os.listdir(self.doc_dir()) == []:
                os.removedirs(self.doc_dir())

    def delete(self):
        super(Document, self).delete()
        self.remove_dirs()

class Image(models.Model):
    image = models.ImageField(upload_to = settings.NEWS_UPLOAD_DIR +
                              '/images/%Y/%m/%d')
    caption = models.CharField(max_length=100)
    article = models.ForeignKey(Article)

    def __unicode__(self):
        return self.caption

    def doc_dir(self):
        return os.path.dirname(self.get_file_filename())

    def remove_dirs(self):
        if os.path.isdir(self.doc_dir()):
            if os.listdir(self.doc_dir()) == []:
                os.removedirs(self.doc_dir())

    def _get_url(self):
        return self.image.url
    url = property(_get_url)

    def delete(self):
        super(Document, self).delete()
        self.remove_dirs()
