from django.http import Http404, HttpResponseRedirect
from django.views.generic.list_detail import object_list, object_detail
from django.views.generic.simple import direct_to_template
from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User
from django.conf import settings
from django.contrib.auth import login as auth_login
from django.shortcuts import get_object_or_404, render_to_response
from django.contrib.auth import REDIRECT_FIELD_NAME
from django.contrib.sites.models import get_current_site
from django.template import RequestContext
from accounts.forms import RequestAccountForm
from accounts.forms import AuthenticationForm
from django.core.urlresolvers import reverse

from models import UserProfile
from forms import PictureForm
from resume.models import Category, Resume


def login(request, template_name='registration/login.html',
          redirect_field_name=REDIRECT_FIELD_NAME,
          authentication_form=AuthenticationForm):
    """Displays the login form and handles the login action."""

    redirect_to = request.REQUEST.get(redirect_field_name, '')

    if request.method == "POST":
        form = authentication_form(data=request.POST)
        if form.is_valid():
            # Light security check -- make sure redirect_to isn't garbage.
            if not redirect_to or ' ' in redirect_to:
                redirect_to = settings.LOGIN_REDIRECT_URL

            # Heavier security check -- redirects to http://example.com should
            # not be allowed, but things like /view/?param=http://example.com
            # should be allowed. This regex checks if there is a '//' *before* a
            # question mark.
            elif '//' in redirect_to and re.match(r'[^\?]*//', redirect_to):
                    redirect_to = settings.LOGIN_REDIRECT_URL

            # Okay, security checks complete. Log the user in.
            auth_login(request, form.get_user())

            if request.session.test_cookie_worked():
                request.session.delete_test_cookie()

            return HttpResponseRedirect(redirect_to)

    else:
        form = authentication_form(request)

    request.session.set_test_cookie()

    current_site = get_current_site(request)

    return render_to_response(template_name, {
        'form': form,
        redirect_field_name: redirect_to,
        'site': current_site,
        'site_name': current_site.name,
    }, context_instance=RequestContext(request))





def view_profile(request, username):
    user = get_object_or_404(User, username=username)
    ownership = user == request.user
    r = Resume.objects.get(user=user)
    category_tree = Category.objects.filter(resume=r).as_items()
    return object_detail(request, queryset=UserProfile.objects.all(),
                         object_id=user.get_profile().pk,
                         template_object_name='profile', extra_context={'ownership': ownership,'category_tree':category_tree,'resume':r,})

@login_required
def edit_profile(request):
    return direct_to_template(request, template='accounts/edit.html', extra_context=request.GET)

@login_required
def upload_picture(request):
    if request.POST:
        form = PictureForm(request.POST, request.FILES) 
        u = request.user.get_profile()
        if form.is_valid():
            print 'valid'
            u.image = request.FILES['image']
            u.save()
            return HttpResponseRedirect('/accounts/edit/')
        else:
            return HttpResponseRedirect('/accounts/edit/?error=true')
    template = 'accounts/edit_forms/picture.html'
    form = PictureForm()
    return direct_to_template(request, template=template, extra_context={'form':form,
                                                                         'user':request.user})

def request_account(request):
    if not request.user.is_authenticated():
        if request.POST:
            form = RequestAccountForm(request.POST)
            if form.is_valid():
                form.save()
                return HttpResponseRedirect(reverse('request_complete'))
        form = RequestAccountForm()
        print form
        template = 'accounts/request_account.html'
        return direct_to_template(request, template=template,
                                  extra_context={'form':form})
    else:
        return HttpResponseRedirect(reverse('homepage'))