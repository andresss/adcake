from django.conf.urls.defaults import *
from django.views.generic.simple import direct_to_template
from django.contrib.auth import views as auth_views
from accounts.views import login as auth_login
import views as accounts_views
from forms import PersonalInformationForm, AccountForm, PERSONAL_INFO_FIELDSETS,\
    SHARING_INFO_FIELDSETS,PrivateSettingsForm, PasswordChangeForm


urlpatterns = patterns('django.views.generic',
                       url(r'^login/$', auth_login, name='auth_login'),
                       url(r'^logout/$', auth_views.logout, {'next_page': '/'}, name='auth_logout'),
                       url(r'^password/change/$', auth_views.password_change, name='auth_password_change'),
                       url(r'^password/change/done/$', auth_views.password_change_done, name='auth_password_change_done'),
                       url(r'^password/reset/$', auth_views.password_reset, name='auth_password_reset'),
                       url(r'^password/reset/confirm/(?P<uidb36>[0-9A-Za-z]+)-(?P<token>.+)/$', auth_views.password_reset_confirm, name='auth_password_reset_confirm'),
                       url(r'^password/reset/complete/$', auth_views.password_reset_complete, name='auth_password_reset_complete'),
                       url(r'^password/reset/done/$', auth_views.password_reset_done, name='auth_password_reset_done'),
                       url(r'^edit/$', accounts_views.edit_profile, name='my_profile'),
                       url(r'^profile/(?P<username>[\w.@+-]+)/$', accounts_views.view_profile, name='profile_for'),
                       url(r'^request/$', accounts_views.request_account,name='request_account'),
                       url(r'^request/complete/$', direct_to_template,{'template': 'accounts/request_complete.html'},name='request_complete'),
                       )

# XHR views ----
urlpatterns += patterns('accounts.xhr_views',
    url(r'edit/xhr/personal_info/', 'serve_form', {'template_name': 'accounts/edit_forms/personal_info.html', 
                                                   'formclass': PersonalInformationForm, 
                                                   'initial_user': ('username','first_name', 'last_name', 'birthdate','country','gender', 'job_status', 'website', 'institutions','career','areas_study','employer','summary','equipment_software','aditional_notes','address','city','state','country','zipcode','hphone','wphone','mphone'),
                                                   'extra_context': {'fieldsets': PERSONAL_INFO_FIELDSETS}}),
    url(r'edit/xhr/account/', 'serve_form', {'template_name': 'accounts/edit_forms/personal_info.html', 
                                             'formclass': AccountForm,
                                             'initial_user': ('username','first_name','city','state','country','zipcode','last_name','lenguages','birthdate','country','gender','categories','address1','career','address2','job_status', 'website', 'institutions','carrers','degree','areas_study','employer','summary','equipment_software','aditional_notes','address','city','state','zipcode','hphone','wphone','mphone'),
                                             'extra_context': {'fieldsets': PERSONAL_INFO_FIELDSETS}}),
    url(r'edit/xhr/password/', 'serve_form', {'template_name': 'accounts/edit_forms/default.html', 
                                              'formclass': PasswordChangeForm,
                                              'initial_user': ('user',),
                                              'extra_context': {'branch': 'Password'}}),
    url(r'edit/xhr/private_settings/', 'serve_form', {'template_name': 'accounts/edit_forms/private_settings.html', 
                                           'formclass': PrivateSettingsForm,
                                           'initial_user': ('follow_email', 'author_email', 'privateim_email', 'comments_email'),
                                           'extra_context': {'fieldsets': SHARING_INFO_FIELDSETS}}),
    url(r'edit/xhr/picture/', accounts_views.upload_picture),
    url(r'edit/upload/$', accounts_views.upload_picture),
    url(r'edit/xhr/companies/', direct_to_template, {'template': 'accounts/edit_forms/companies.html'}),
)
