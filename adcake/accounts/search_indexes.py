from haystack import indexes
from haystack.sites import site
from accounts.models import UserProfile

class UserProfileIndex(indexes.SearchIndex):
    text = indexes.CharField(document=True, use_template=True)
    rendered = indexes.CharField(use_template=True, indexed=False)
site.register(UserProfile, UserProfileIndex)
