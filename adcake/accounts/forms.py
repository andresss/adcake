from django import forms
from django.contrib.auth.models import User
from django.utils.translation import ugettext_lazy as _
from accounts.models import Request, ReservedName
from countries.countries.models import Country,UsState
#from django.contrib.localflavor.us.forms import USPhoneNumberField
from directory.models import Lenguage, Company
from models import UserProfile
from django.forms.extras.widgets import Select
from django.contrib.auth import authenticate

PERSONAL_INFO_FIELDSETS = (
    ('Basic Information', {'fields': ('username','first_name', 'last_name', 'birthdate','gender',), 'classes': ['inlineLabels']}),
    ('Work', {'fields': ('job_status','categories','employer','summary',), 'classes':['inlineLabels']}),
    ('Contact Information', {'fields': ('hphone','mphone','wphone','address1','address2','city','state','zipcode','country','website',), 'classes': ['inlineLabels']}),
    ('Education', {'fields': ('degree','career','lenguages','equipment_software',), 'classes': ['inlineLabels']}),
)

SHARING_INFO_FIELDSETS = (
    ('Sharing Information', {'fields': ('share_binformation','share_cinformation','share_education','share_portafolio',), 'Classes': ['inlineLabels']}),('Notifications', {'fields':('follow_email', 'author_email', 'privateim_email', 'comments_email',),'Classes':['inlineLabels']}),
)
class PersonalInformationForm(forms.ModelForm):
    def done(self, request):
        u = request.user.get_profile()
        c = Country.objects.get(iso=request.POST['country'])
        f = PersonalInformationForm(request.POST, instance=u)
        try:
            f.save()
            return True
        except Exception, e:
            print e
            return False

    class Meta:
        model = UserProfile
        exclude = ('user', 'image', 'network', 'follow_email', 'author_email', 'privateim_email', 'comments_email')

class AccountForm(forms.ModelForm):
    country = forms.ModelChoiceField(queryset=Country.objects.all(),widget=Select,required=False)
    username = forms.RegexField(label=_("Username"), max_length=30, regex=r'^[\w.-]+$',
        help_text = _("Required. 30 characters or fewer. Letters, digits and . - _ only."),
        error_message = _("This value may contain only letters, numbers and .-_ characters."))
    first_name = forms.CharField(label=_('First Name'), max_length=30, required=False)
    last_name = forms.CharField(label=_('Last Name'), max_length=30, required=False,
                                help_text=_('With your full name it will be easier\
                                for other people to find you'))
    hphone = forms.CharField(label=_('Home Phone'),help_text='In the format (XX)XXX-XX-XXXX', required=False)
    mphone =  forms.CharField(label=_('Mobile Phone'),help_text='In the format (XX)XXX-XX-XXXX', required=False)
    wphone =  forms.CharField(label=_('Work Phone'),help_text='In the format (XX)XXX-XX-XXXX', required=False)
    equipment_software = forms.CharField(widget=forms.Textarea, label=_('Equipment and Software'),required=False)
    career = forms.CharField(widget=forms.Textarea, label=_('Careers'),required=False)
    class Meta:
        model = UserProfile
        exclude = ('user', 'image', 'network', 'follow_email', 'author_email', 'privateim_email', 'comments_email')
        
    def __init__(self, *args, **kwargs):
        self.request = kwargs.pop('request', None)
        super(AccountForm, self).__init__(*args, **kwargs)

    def clean_username(self):
        if self.request.user.username == self.cleaned_data['username'].lower():
            return self.cleaned_data['username'].lower()
        if User.objects.filter(username__iexact=self.cleaned_data['username']) or ReservedName.objects.filter(reserved__iexact=self.cleaned_data['username']) or Company.objects.filter(uniquename__iexact=self.cleaned_data['username']):
            raise forms.ValidationError(_(u'This username is already taken. Please choose another.'))
        else:
            return self.cleaned_data['username'].lower()
    def done(self, request):
        try:
            u = request.user
            u.username= self.cleaned_data['username']
            u.first_name= self.cleaned_data['first_name']
            u.last_name= self.cleaned_data['last_name']
            u.save()
            up = request.user.get_profile()
            up.birthdate = self.cleaned_data['birthdate']
            up.gender = self.cleaned_data['gender']
            up.job_status = self.cleaned_data['job_status']
            up.employer = self.cleaned_data['employer']
            up.summary = self.cleaned_data['summary']
            up.hphone = self.cleaned_data['hphone']
            up.mphone = self.cleaned_data['mphone']
            up.wphone = self.cleaned_data['wphone']
            up.address1 = self.cleaned_data['address1']
            up.address2 = self.cleaned_data['address2']
            up.equipment_software = self.cleaned_data['equipment_software']
            up.categories.clear()
            for cat in self.cleaned_data['categories']:
                up.categories.add(cat)
            up.city = self.cleaned_data['city']
            up.state = self.cleaned_data['state']
            up.zipcode = self.cleaned_data['zipcode']
            up.country = self.cleaned_data['country']
            up.website = self.cleaned_data['website']
            up.degree = self.cleaned_data['degree']
            up.career = self.cleaned_data['career']
            up.lenguages.clear()
            for leng in self.cleaned_data['lenguages']:
                up.lenguages.add(leng)
            up.equipment_software = self.cleaned_data['equipment_software']
            up.save()
            return True
        except Exception, e:
            print e
            return False

class PrivateSettingsForm(forms.ModelForm):
    class Meta:
        model = UserProfile
        fields = ('follow_email', 'author_email', 'privateim_email', 'comments_email','share_binformation','share_cinformation','share_education','share_portafolio')

    def done(self, request):
        u = request.user.get_profile()
        f = PrivateSettingsForm(request.POST, instance=u)
        try:
            f.save()
            return True
        except Exception, e:
            print e
            return False
    
class PasswordChangeForm(forms.Form):
    new_password1 = forms.CharField(label=_("New password"), widget=forms.PasswordInput)
    new_password2 = forms.CharField(label=_("New password confirmation"), widget=forms.PasswordInput)
    old_password = forms.CharField(label=_("Old password"), widget=forms.PasswordInput)

    def __init__(self, user, *args, **kwargs):
        self.user = user
        super(PasswordChangeForm, self).__init__(*args, **kwargs)

    def clean_new_password2(self):
        password1 = self.cleaned_data.get('new_password1')
        password2 = self.cleaned_data.get('new_password2')
        if password1 and password2:
            if password1 != password2:
                raise forms.ValidationError(_("The two password fields didn't match."))
        return password2

    def clean_old_password(self):
        """
        Validates that the old_password field is correct.
        """
        old_password = self.cleaned_data["old_password"]
        if not self.user.check_password(old_password):
            raise forms.ValidationError(_("Your old password was entered incorrectly. Please enter it again."))
        return old_password

    def done(self, request):
        u = request.user
        try:
            u.set_password(self.cleaned_data['new_password1'])
            u.save()
            return True
        except Exception, e:
            print e
            return False
PasswordChangeForm.base_fields.keyOrder = ['old_password', 'new_password1', 'new_password2']

class PictureForm(forms.ModelForm):
    image = forms.ImageField()

    class Meta:
        model = UserProfile
        fields = ('image',)




class AuthenticationForm(forms.Form):
    """
    Base class for authenticating users. Extend this to get a form that accepts
    username/password logins.
    """
    username = forms.CharField(label=_("Username"), max_length=30)
    password = forms.CharField(label=_("Password"), widget=forms.PasswordInput)

    def __init__(self, request=None, *args, **kwargs):
        """
        If request is passed in, the form will validate that cookies are
        enabled. Note that the request (a HttpRequest object) must have set a
        cookie with the key TEST_COOKIE_NAME and value TEST_COOKIE_VALUE before
        running this validation.
        """
        self.request = request
        self.user_cache = None
        super(AuthenticationForm, self).__init__(*args, **kwargs)

    def clean(self):
        username = self.cleaned_data.get('username')
        password = self.cleaned_data.get('password')

        if username and password:
            self.user_cache = authenticate(username=username.lower(), password=password)
            if self.user_cache is None:
                raise forms.ValidationError(_("Please enter a correct username and password. Note that both fields are case-sensitive."))
            elif not self.user_cache.is_active:
                raise forms.ValidationError(_("This account is inactive."))

        # TODO: determine whether this should move to its own method.
        if self.request:
            if not self.request.session.test_cookie_worked():
                raise forms.ValidationError(_("Your Web browser doesn't appear to have cookies enabled. Cookies are required for logging in."))

        return self.cleaned_data

    def get_user_id(self):
        if self.user_cache:
            return self.user_cache.id
        return None

    def get_user(self):
        return self.user_cache

    
class RequestAccountForm(forms.ModelForm):
    class Meta:
        model = Request
        fieds = ('')