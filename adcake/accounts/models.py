# -*- coding: utf-8 -*-
from django.contrib.auth.models import User
from django.db import models
from cake.models import ContactListMember, Box
from countries.countries.models import Country,UsState,Institution
from directory.models import Company
from multimedia.models import Video
from sorl.thumbnail.fields import ImageWithThumbnailsField
from directory.models import *
from django.contrib.contenttypes.models import ContentType
from django.contrib.contenttypes import generic

USER_GENDER = (
    ('M', 'Male'),
    ('F', 'Female'),
    )


DEGREE_LVL = (
    ('B', 'Bachelor\'s Degree'),
    ('M', 'Master\'s Degree'),
    ('D', 'Doctoral Degree'),
    ('V', 'Vocational'),
    ('C', 'Certification'),
    ('P', 'Professional'),
    ('H', 'High School'),
)

    
    
JOB_STATUS = (
    ('U', 'Unemployed'),
    ('L', 'Seeking a Full-Time Job'),
    ('P', 'Seeking Part-Time Job'),
    ('F', 'Seeking Freelance Work'),
    ('E', 'Employed'),
    )

class Network(models.Model):
    name = models.CharField(max_length=30)

    def __unicode__(self):
        return self.name

def upload_to(attribute):

    def upload_callback(instance, filename):
        u=getattr(instance, attribute)
        return 'uploads/users/%s/avatar/%s' % (u.id, filename)
    return upload_callback

class UserProfile(models.Model):
    user = models.ForeignKey(User, unique=True)
    image = ImageWithThumbnailsField(upload_to= upload_to('user'),
                                     thumbnail={'size': (96, 96),'quality':100},
                                     extra_thumbnails={
        'tiny'  : {'size': (24,24),'quality':100, 'options': ['upscale'],},
        'small' : {'size': (48,48),'quality':100, 'options': ['upscale'],},
        'featured'  : {'size': (60,60),'quality':100, 'options': ['upscale'],},
        'medium': {'size': (64,64),'quality':100, 'options': ['upscale'],},
        'large' : {'size': (128,128),'quality':100, 'options': ['upscale']},
        'big' : {'size': (148,148),'quality':100, 'options': ['upscale']},},
                                     blank=True)
    birthdate = models.DateField(blank=True, null=True)
    institutions = models.ManyToManyField(Institution, null=True,blank=True)
    categories = models.ManyToManyField(Category, null=True,blank=True) #Category is related with the working field of the proffessional
    job_status = models.CharField(max_length=1, choices=JOB_STATUS, blank=True,null=True)
    degree = models.CharField(max_length=1, choices=DEGREE_LVL, blank=True,null=True)
    career = models.TextField(null=True,blank=True)
    gender = models.CharField(max_length=1, choices=USER_GENDER, blank=True)
    website = models.URLField(blank=True, null=True, help_text='Have a homepage or blog?')
    areas_study = models.ManyToManyField(AreaStudy, null=True,blank=True)
    lenguages = models.ManyToManyField(Lenguage, null=True,blank=True)
    discipline = models.ManyToManyField(Discipline, null=True,blank=True)
    employer = models.CharField(max_length=40, blank=True,null=True)
    summary = models.TextField(blank=True, null=True)
    equipment_software = models.TextField(blank=True,null=True)
    address1 = models.CharField(max_length=100,blank=True,null=True)
    address2 = models.CharField(max_length=100,blank=True,null=True)
    city =  models.CharField(max_length=35, blank=True, null=True)
    state = models.CharField(max_length=35, blank=True, null=True)
    zipcode = models.CharField(max_length=7, blank=True, null=True)
    hcode =  models.CharField(max_length=5, blank=True, null=True)
    mcode =  models.CharField(max_length=5, blank=True, null=True)
    wcode =  models.CharField(max_length=5, blank=True, null=True)
    hphone = models.CharField(max_length=18, blank=True,null=True)
    mphone = models.CharField(max_length=18, blank=True, null=True)
    wphone = models.CharField(max_length=18, blank=True, null=True)
    country = models.ForeignKey(Country, blank=True, null=True)
    #timezone = models.DateTimeField(blank=True, null=True)
    network = models.ManyToManyField(Network, blank=True, null=True,related_name='networks')
    follow_email = models.BooleanField('Starts following you', default=False)
    author_email = models.BooleanField('Requests authoring on your work', default=False)
    privateim_email = models.BooleanField('Sends you a private IM', default=False)
    comments_email = models.BooleanField('Comment on your work', default=False)
    share_binformation = models.BooleanField('Basic Information', default=True) 
    share_cinformation = models.BooleanField('Contact Information', default=True) 
    share_education = models.BooleanField('Education', default=True) 
    share_work = models.BooleanField('Work', default=True)
    share_portafolio = models.BooleanField('Portfolio',default=True)
    featured = models.BooleanField('Featured',default=False)
    # Account Preferences
    # private_profile, time_zone?, gallery_permissions, video_permissions, hide_email, 
    # Notifications
    # email me when: someone starts following you, someone's claiming authoring on your work,
    # someone wrote you a private message, comments on my galleries/videos, etc...
    # Password
    # Change password, set security question?

    def _get_image(self):
        try:
            return self.image.url
        except ValueError:
            from django.conf import settings
            return settings.MEDIA_URL + 'images/no-avatar.png'
    avatar = property(_get_image)

    def _get_contact_list(self):
        return self.user.contactlist_set.all()[0]
    contact_list = property(_get_contact_list)

    def _get_last_contact_list(self):
        return self.contact_list.members.all()[:4]
    contact_list_last = property(_get_last_contact_list)
    
    def _get_followers(self):
        return ContactListMember.objects.get_followers_for(self.user)
    followers = property(_get_followers)

    def _get_following(self):
        return self.contact_list.members.all()
    following = property(_get_following)
    
    def _get_job_status(self):
        if self.job_status=='E':
            js="Employed"
        elif self.job_status=='F':
            js="Seeking Freelance Work"
        elif self.job_status=='P':
            js="Seeking Part-Time Job"
        elif self.job_status=='L':
            js="Seeking a full time job"
        elif self.job_status=='U':
            js="Unemployed"
        else:
            js=None
        return js
    job_stat = property(_get_job_status)

    def __get_region(self):
        return self.country
    get_region = property(__get_region)
    
    def _get_lenguages(self):
        return ", ".join([l.name for l in self.lenguages.all()])
    lengs = property(_get_lenguages)

    def _get_cakebox_length(self):
        n = 0
        bl = Box.objects.filter(owner=self.user)
        for b in bl:
            n += len(b.items.all())
        return n
    cakebox_length = property(_get_cakebox_length)


    @models.permalink
    def get_absolute_url(self):
        return ('profile_for', (), {
            'username': self.user.username,
        })

    def get_name(self):
        try:
            return self.user.username.capitalize() or self.user.get_full_name() or self.user.email
        except UserProfile.DoesNotExist:
            return self.user.email

    def following_count(self):
        return self.following.count()


    def __unicode__(self):
        return self.get_name()

    def __videos_count(self):
        return len(self.user.portfolio.videos)
    videos_count = property(__videos_count)
    
    def __galleries_count(self):
        return len(self.user.portfolio.galleries.get_featured())
    galleries_count = property(__galleries_count)

    def some_cats(self):
        cats=self.categories.all()
        count = len(cats)
        fc =''
        for counter,cat in enumerate(cats[:4]):
            if counter == 0:
                fc += "%s" % cat.title
            else:
                fc += ", %s" % cat.title
            if counter == 2 and count > 3:
                fc += "..."
        return fc

class Request(models.Model):
    first_name = models.CharField(max_length=30,blank=False,null=False)
    last_name = models.CharField(max_length=30,blank=False,null=False)
    email = models.EmailField(blank=False,null=False)
    phone = models.CharField(max_length=20,blank=True,null=True)
    categories = models.ManyToManyField(Category, null=True,blank=True) #Category is related with the working field of the proffessional
    job_status = models.CharField(max_length=1, choices=JOB_STATUS, blank=False,null=False)
    degree = models.CharField(max_length=1, choices=DEGREE_LVL, blank=True,null=True)
    career = models.TextField(null=True,blank=True)
    gender = models.CharField(max_length=1, choices=USER_GENDER, blank=True)
    website = models.URLField(blank=True, null=True, help_text='Do you have a homepage or a blog?')
    employer = models.CharField(max_length=40, blank=True,null=True)
    summary = models.TextField(blank=True, null=True)
    equipment_software = models.TextField(blank=True,null=True)
    country = models.ForeignKey(Country, blank=False, null=False)

    def __unicode__(self):
        return self.email

class ReservedName(models.Model):
    reserved = models.CharField(('Reserved name'), max_length=30, unique=True, help_text=("Required. 30 characters or fewer. Letters, numbers and @/./+/-/_ characters"))
    def is_reserved(self,uniquename):
        if self.objects.filter(reserved__exact=uniquename):
            return True
        else:
            return False
    def __unicode__(self):
        return self.reserved


class UserNotification(models.Model):
    user = models.ForeignKey(User,unique=False,null=False)
    notification = models.CharField(name="Notification",max_length=200,blank=False)
    content_type = models.ForeignKey(ContentType,null=True,blank=True)
    object_id = models.PositiveIntegerField(null=True,blank=True)
    content_object = generic.GenericForeignKey('content_type', 'object_id')