from django.core.management.base import BaseCommand, CommandError
from django.contrib.auth.models import User

from accounts.utils import profile_creation

class Command(BaseCommand):
    help = "Create an user profile for the super user."
    requires_model_validation = True

    def handle(self, *args, **kargs):
        try:
            superusers = User.objects.filter(is_superuser=True)
            for user in superusers:
                profile_creation(user)
            return 'Profile created for users: %s' % ', '.join([unicode(i)
                                                                for i in
                                                                superusers])
        except ImportError, error:
            return 'Error importing: %s' % error
        except Exception, e:
            return 'Something went wrong, are you sure you have at least one user?, look: %s' % e
