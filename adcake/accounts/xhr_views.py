from django.http import HttpResponseRedirect
from django.shortcuts import render_to_response
from django.template import RequestContext

def serve_form(request, formclass,
               template_name='accounts/edit_forms/default.html',
               initial_user=None, initial=None,
               on_success=None, extra_context=None,
               success_message='Successfully saved.',
               error_message='Error during save.'):
    """
    Takes a formclass and a template and renders the a form of `formclass` in
    that template.
    When a POST request is recieved, the form is validated and if it all goes
    well then a form method (by convention: done()) is called. If there are
    errors, the template shows again with the errors that were found.
    """
    extra = {}
    if request.POST:
        if 'user' in initial_user:
            form = formclass(request.user, request.POST)
        else:
            form = formclass(request.POST, request=request)
        print form.is_valid(), form.errors
        if form.is_valid():
            if not form.done(request):
                extra['error'] = error_message
            else:
                if on_success:                    return HttpResponseRedirect(on_success)
                else:
                    extra['message'] = success_message
    else:
        initial_data = {}
        if initial_user is not None:
            user = request.user
            for key in initial_user:
                if hasattr(user, key):
                    initial_data[key] = getattr(user, key)
                elif hasattr(user.get_profile(), key):
                    initial_data[key] = getattr(user.get_profile(), key)
                else:
                    pass

            for key, val in initial_data.items():
                if hasattr(val, 'all'):
                    initial_data[key] = val.all()

        if initial is not None:
            initial_data.update(initial)

        if 'user' in initial_data:
            form = formclass(initial_data['user'])
        else:
            form = formclass(initial=initial_data)
    extra['form'] = form
    if extra_context is not None:
        extra.update(extra_context)
    return render_to_response(template_name, extra,
                              context_instance=RequestContext(request))

def companies_admin(request):
    pass
