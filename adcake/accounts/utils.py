from django.db import IntegrityError

from models import UserProfile
from cake.models import ContactList
from portfolio.models import Portfolio
from resume.models import Resume, Category
from django.utils.translation import ugettext as _


def initialize_resume(resume):
    resume.category_list.all().delete()
    resume.add_category(title='Objective', deletable=True)
    resume.add_category(title='Education', deletable=True)
    resume.add_category(title='Experience', deletable=True)
    resume.add_category(title='Skills', deletable=True)
    resume.add_category(title='Knowledge', deletable=True)
    resume.add_category(title='Language', deletable=True)
    
def profile_creation(user):
    """
    Models to be created at user-registration time
    """
    try:
        UserProfile(user=user).save()
        ContactList(owner=user).save()
        Portfolio(owner=user).save()
        r = Resume(user=user)
        r.save()
        initialize_resume(r)
        #undefined_str = _('Update your Information')
        #name = user.get_full_name()
        #if not name:
        #    name = undefined_str
#        c.add_named_skill(name='Name', value=name, level=0, comment=None, deletable=False)
#        c.add_named_skill(name='Gender', value=undefined_str, level=0, comment=None, deletable=False)
#        c.add_named_skill(name='Birth Date', value=undefined_str, level=0, comment=None, deletable=False)
 #       s = Category(resume=r, title='Contact Information', parent_category=c, deletable=False)
 #       s.save()
 #       s.add_named_skill(name='Address', value=undefined_str, level=0, comment=None, deletable=False)
 #       s.add_named_skill(name='Telephone', value=undefined_str, level=0, comment=None, deletable=False)
 #       s.add_named_skill(name='Website', value=undefined_str, level=0, comment=None, deletable=False)
    except IntegrityError:
        pass
