from django.contrib import admin
from accounts.models import ReservedName
from models import UserProfile, Network,Request
from multimedia.models import AuthorRole
from django.contrib.auth.models import User

from django.contrib.auth.admin import UserAdmin

class RequestAdmin(admin.ModelAdmin):
    list_display = ('email','first_name', 'last_name')

class CustomUserAdmin(UserAdmin):
    list_display = ('username', 'email', 'first_name', 'last_name', 'is_staff', 'is_active','date_joined','last_login')
    search_fields = ['username', 'email', 'first_name', 'last_name']

class UserProfileAdmin(admin.ModelAdmin):
    list_display = ('user','featured','country')
    search_fields = ['user__username','country__name']
    
admin.site.unregister(User)
admin.site.register(User, CustomUserAdmin)
admin.site.register(Request, RequestAdmin)
admin.site.register(UserProfile,UserProfileAdmin)
admin.site.register(ReservedName)
admin.site.register(Network)
admin.site.register(AuthorRole)
