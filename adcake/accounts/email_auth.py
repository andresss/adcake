"""
Allow us to use an email address instead of the user id as the
primary id
Taken from a posting on the Django mailing list.
Thanks to Vasily Sulatskov for sending this to the list.
"""

from django.contrib.auth.backends import ModelBackend
from django.contrib.auth.models import User
from django.core.validators import email_re
            
class EmailBackend(ModelBackend):
    """Authenticate using email first, then username"""
    def authenticate(self, username=None, password=None, hashed=False):
        #If username is an email address, then try to pull it up
        if email_re.search(username):
            try:
                user = User.objects.get(email=username)
                if user.check_password(password):
                    return user
            except User.DoesNotExist:
                pass
        else:
            #We have a non-email address username we should try username
            try:
                user = User.objects.get(username=username)
                if user.check_password(password):
                    return user
            except User.DoesNotExist:
                return None
        if hashed:
            try:
                user = User.objects.get(username=username, password=password)
                return user
            except User.DoesNotExist:
                return None
        return None
