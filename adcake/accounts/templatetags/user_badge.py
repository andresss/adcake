from django.utils.translation import ugettext_lazy as _
from django import template
from django.contrib.auth.forms import AuthenticationForm
from registration.forms import RegistrationFormUniqueEmail

from django.conf import settings

register = template.Library()

@register.inclusion_tag('accounts/user_badge.html',
                        takes_context=True)
def badge(context):
    """
    Includes the user editable information as a badge.
    If the user is not logged it displays the log in/register forms

    Usage::

        {% load user_badge %}
        {% badge %}
    """
    user = context.get('user')
    if user.is_anonymous():
        return {
            'user': user,
            'form': AuthenticationForm(),
            'registration_form': RegistrationFormUniqueEmail(),
            }
    full_name = user.get_full_name()
    if not full_name:
        full_name = unicode(user).capitalize()
    return {'user': user, 'full_name' : full_name, 'MEDIA_URL': settings.MEDIA_URL}
