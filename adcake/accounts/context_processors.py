from datetime import datetime
from django.conf import settings
from django.core.cache import cache
import twitter


def latest_tweet( request ):
    tweets = cache.get( 'tweets' )
    api = twitter.Api(username='ctaloc', password='s4l4m4ndre')
    tweets = api.GetFriendsTimeline()
    cache.set( 'tweets', tweets, settings.TWITTER_TIMEOUT )
    return {"tweets": tweets}
