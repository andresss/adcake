from django.template import Library

from skills.models import Skill

register = Library()

@register.inclusion_tag('quick_skills_widget.html', takes_context=True)
def quick_skills(context):
    """
    Shows the skills quick form at the 'personal bin' section
    """
    return { 'skills': Skill.objects.get_for_user(context.get('user')) }
