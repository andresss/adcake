from django.conf.urls.defaults import *
from django.views.generic.list_detail import object_list
from django.views.generic.simple import redirect_to

from skills.models import Skill

skills_list = {
    'queryset': Skill.objects.all().order_by('name'),
    'template_object_name': 'skill',
    'paginate_by': 1,
    }

urlpatterns = patterns('skills.views',
#                       url(r'^$', redirect_to, {'url': '/skills/page1/'}),
#                       url(r'^page(?P<page>\d+)/$', object_list, skills_list, name='skills_index'),
                       url(r'^add/$', 'add_skill', name='skills_add'),
                       url(r'^remove/$', 'remove_skill', name='skills_remove'),
                       url(r'^update/$', 'update_skill', name='skills_update'),
                       url(r'^live_search/$', 'live_search', name='live_search'),
                       url(r'^(?P<skill_name>[a-z0-9-]+)/$', 'common_skills', name='skills_common'),
                       url(r'^(?P<skill_name>[a-z0-9-]+)/page(?P<page>[0-9]+)/$', 'common_skills', name='skills_common_page'),
)
