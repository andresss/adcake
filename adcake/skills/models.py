from django.db import models
from django.db.models import permalink
from django.contrib.auth.models import User
from django.utils.translation import ugettext_lazy as _
from django.conf import settings
from django.template.defaultfilters import slugify
class SkillManager(models.Manager):
    def add_skill(self, user, skill_name, skill_value=None,
                  category=None, level=3, comment=None, deletable=True):
        """
        Associate a given skill to an user and/or resume category with
        some experience level.
        This method also accepts comma-separated values.
        It returns the skill's name list that has been added
        """
        skill_names = [skill.strip() for skill in skill_name.split(',')]
        skills = []
        if len(skill_names) == 0:
            raise AttributeError(_('No skills were given: "%s".') % skill_name)
        for skill in skill_names:
            if skill:
                if settings.SKILLS_FORCE_LOWERCASE:
                    skill = skill.lower()
                skill_obj, created = self.get_or_create(name=skill)

                if user and not skill_value:    
                    try:
                        usk = UserSkill.objects.get(user=user, skill=skill_obj)
                        usk.level = level
                        usk.save()
                    except UserSkill.DoesNotExist:
                        usk = UserSkill(user=user, skill=skill_obj, level=level)
                        usk.save()
                        skills.append(usk.skill.name)

                if category:
                    from resume.models import CategorySkill
                    c = CategorySkill(category=category, skill=skill_obj,
                                      comment=comment, value=skill_value,
                                      level=level, deletable=deletable)
                    c.save()
                    if c.skill.name not in skills:
                        skills.append(c.id)
        return skills

    def remove_skill(self, user, skill_name):
        """
        Remove a given skill from an user.
        """
        if not skill_name:
            raise AttributeError(_('No skill was given: "%s".') % skill_name)
        if settings.SKILLS_FORCE_LOWERCASE:
            skill_name = skill_name.lower()
        try:
            usk = UserSkill.objects.get(user=user, skill__name=skill_name)
            usk.delete()
        except UserSkill.DoesNotExist:
            pass

    def update_skill_level(self, user, skill_name, level):
        usk = UserSkill.objects.get(skill__name=skill_name)
        usk.level = level
        usk.save()

    def get_for_skill_name(self, skill_name):
        """
        Return a list of users who have a skill.
        """
        try:
            skill = self.get(name=skill_name)
        except Skill.DoesNotExist:
            return []
        return [usk.user for usk in skill.users_related.all()]

    def get_for_user(self, user):
        """
        Return a queryset of skills for the given user.
        """
        return self.filter(users_related__user = user)

class Skill(models.Model):
    """
    A skill.

    >>> Skill.objects.all().delete()

    #Create some dummy users
    >>> from django.contrib.auth.models import User
    >>> daniel = User.objects.create_user('daniel', 'daniel@ac.labf.usb.ve', 'aaaa')
    >>> dummy = User.objects.create_user('dummy', 'xxxx@ac.labf.usb.ve', 'aaaa')

    #Add some skills
    >>> Skill.objects.add_skill(daniel, 'phoToshop, Mootools')
    ['photoshop', 'mootools']
    >>> Skill.objects.add_skill(daniel, 'mootools, Adobe Illustator')
    ['adobe illustator']

    #Retrieve skills for daniel
    >>> Skill.objects.get_for_user(daniel)
    [<Skill: photoshop>, <Skill: mootools>, <Skill: adobe illustator>]

    #Remove skills
    >>> Skill.objects.remove_skill(daniel, 'mootools')
    >>> Skill.objects.get_for_user(daniel)
    [<Skill: photoshop>, <Skill: adobe illustator>]

    #Removing a non-existent skill pass silently
    >>> Skill.objects.remove_skill(daniel, 'xxxxxx')
    >>> Skill.objects.get_for_user(daniel)
    [<Skill: photoshop>, <Skill: adobe illustator>]

    #Users can access their skills and levels with the related_name property
    >>> daniel.skills_related.all()
    [<UserSkill: daniel: photoshop(3)>, <UserSkill: daniel: adobe illustator(3)>]
    >>> [usk.level for usk in daniel.skills_related.all()]
    [3, 3]

    #Update a skill level given the user
    >>> Skill.objects.update_skill_level(daniel, 'photoshop', 4)
    >>> daniel.skills_related.all()
    [<UserSkill: daniel: photoshop(4)>, <UserSkill: daniel: adobe illustator(3)>]

    >>> Skill.objects.add_skill(dummy, 'photoshop, adobe flash, django, python')
    [u'photoshop', 'adobe flash', 'django', 'python']
    >>> dummy.skills_related.all()
    [<UserSkill: dummy: photoshop(3)>, <UserSkill: dummy: adobe flash(3)>, <UserSkill: dummy: django(3)>, <UserSkill: dummy: python(3)>]

    #Get users who have a specific skill
    >>> Skill.objects.get_for_skill_name('photoshop')
    [<User: daniel>, <User: dummy>]
    >>> Skill.objects.get_for_skill_name('adobe illustator')
    [<User: daniel>]
    >>> Skill.objects.get_for_skill_name('xxxxxx')
    []

    #Add a skill to both a Category and a User.
    >>> from resume.models import Resume, Category
    >>> r = Resume(user=daniel)
    >>> r.save()
    >>> c = r.add_category('Art')
    >>> Skill.objects.add_skill(daniel, 'Maya', category=c)
    ['maya']

    >>> daniel.skills_related.all()
    [<UserSkill: daniel: photoshop(4)>, <UserSkill: daniel: adobe illustator(3)>, <UserSkill: daniel: maya(3)>]
    >>> c.skill_set.all()
    [<CategorySkill: <daniel's Resume / Art>: maya(3)>]
    
    #Add a skill just to a Category 
    >>> Skill.objects.add_skill(None, 'Blender', category=c)
    ['blender']

    """
    name = models.CharField(_('Name'), max_length=50, unique=True)
    objects = SkillManager()
    
    def __unicode__(self):
        return self.name

    def _get_users(self):
        return [usk.user for usk in self.users_related.all()]
    users = property(_get_users)

    def _get_some_users(self):
        return [usk.user for usk in self.users_related.all()[:6]]
    some_users = property(_get_some_users)

    @permalink
    def get_absolute_url(self):
        return ('skills_common', [slugify(self.name)])

    class Meta:
        verbose_name = _('Skill')
        verbose_name_plural = _('Skills')

class UserSkill(models.Model):
    """
    Explicit ManyToMany relationship between an user and his skills.
    """
    user = models.ForeignKey(User, related_name='skills_related')
    skill = models.ForeignKey(Skill, related_name='users_related')
    level = models.IntegerField(max_length=5, default='0')
    
    def __unicode__(self):
        return '%s: %s(%s)' % (self.user, self.skill, self.level)

    class Meta:
        unique_together = ('user', 'skill',)
        verbose_name = _('User Skill')
        verbose_name_plural = _('Users Skills')
