from django.core.urlresolvers import reverse
from django.shortcuts import render_to_response
from django.template.context import RequestContext
from django.views.decorators.http import require_POST
from django.http import HttpResponseRedirect
from django.core.paginator import Paginator
from skills.models import Skill
from contrib.json import JsonResponse
from helpers.utils import clean_skill

@require_POST
def add_skill(request):
    skill_name = request.POST.get('skill','')
    skill_cleaned = clean_skill(skill_name)
    if skill_cleaned:
        try:
            response_dict = {
                'skills': Skill.objects.add_skill(request.user, skill_cleaned),
                'complete': True,
                }
        except AttributeError:
            response_dict = {'complete': False}
    else:
        response_dict = {'complete': False}
    if request.is_ajax():
        return JsonResponse(response_dict)
    return HttpResponseRedirect(request.META['HTTP_REFERER'])

@require_POST
def remove_skill(request):
    if not request.is_ajax():
        return HttpResponseRedirect(request.META['HTTP_REFERER'])
    response_dict = {'complete': True}
    try:
        Skill.objects.remove_skill(request.user, request.POST['skill'])
    except:
        response_dict['complete'] = False
    return JsonResponse(response_dict)

@require_POST
def update_skill(request):
    if not request.POST['level']:
        return HttpResponseRedirect(request.META['HTTP_REFERER'])
    try:
        Skill.objects.update_skill(request.user, skill_name,
                                   request.POST['level'])
        response_dict = {'msg': _('Complete!')}
    except:
        response_dict = {'msg': _('No skills were given')}
    if request.is_ajax():
        return JsonResponse(response_dict)
    return HttpResponseRedirect(request.META['HTTP_REFERER'])

def common_skills(request, skill_name,page=1,*args, **kwargs):
    skill_name_cleaned=skill_name.replace('-',' ')
    print skill_name_cleaned
    professionals_list = Skill.objects.get_for_skill_name(skill_name_cleaned)
    if not professionals_list:
        return HttpResponseRedirect(reverse('homepage'))
    paginator = Paginator(professionals_list, 10)
    professionals = paginator.page(page)
    extra_context = {
        'is_paginated': professionals.has_other_pages(),
        'results_per_page': paginator.per_page,
        'has_next': professionals.has_next(),
        'has_previous': professionals.has_previous(),
        'page': professionals.number,
        'next': professionals.next_page_number(),
        'previous': professionals.previous_page_number(),
        'first_on_page': professionals.start_index(),
        'last_on_page': professionals.end_index(),
        'pages': paginator.num_pages,
        'hits': paginator.count,
        'page_range': paginator.page_range,
        'page_obj': professionals,
        'paginator': paginator,
        'user_list' : professionals,
        'skill' : skill_name_cleaned,
    }
    return render_to_response('skills/users.html', extra_context ,context_instance=RequestContext(request))

@require_POST
def live_search(request):
    skills =  [skill.name
               for skill
               in Skill.objects.filter(name__istartswith=request.POST['skill'])]
    return JsonResponse(skills)
