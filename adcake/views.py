from django.contrib.auth.models import User
from django.http import Http404
from django.views.generic.list_detail import object_detail
from django.views.generic.simple import direct_to_template
from accounts.models import UserProfile, UserNotification
from directory.models import Company
from multimedia.models import Video,Image, AuthorRole
#from haystack.views import SearchView as HaystackSearchView
#from haystack.forms import ModelSearchForm
from itertools import chain
from invitation.models import InvitationKey
from portfolio.views import view_portfolio
#from django.views.decorators.cache import cache_page

remaining_invitations_for_user = InvitationKey.objects.remaining_invitations_for_user
import memcache

#@cache_page(60 * 15)
def homepage(request):
    mc = memcache.Client(['127.0.0.1:11211'], debug=0)
    new_media_list = mc.get("new_media_list")
    if not new_media_list:
        new_image_list = Image.objects.select_related('owner').all().order_by('-id')[:6]
        new_video_list=Video.objects.select_related('owner','region').all().order_by('-id').exclude(owner__username='mas').exclude(owner__username='camillebritor')[:6]
        new_media_list = list(chain(new_image_list,new_video_list))
        mc.set("new_media_list",new_media_list,86400)

    most_view_list = mc.get("most_view_list")
    if not most_view_list:
        most_view_image = Image.objects.select_related('owner').all().order_by('-view_count')[:6]
        most_view_video = Video.objects.select_related('owner','region').all().order_by('-view_count')[:6]
        most_view_list = list(chain(most_view_image,most_view_video))
        mc.set("most_view_list",most_view_list,86400)

    random_view_list = mc.get("random_view_list")
    if not random_view_list:
        random_view_image = Image.objects.select_related('owner').all().order_by('?').exclude(owner__username='reinaldofg').exclude(owner__username='melissabm')[:6]
        random_view_video = Video.objects.select_related('owner','region').all().order_by('?').exclude(owner__username='mas').exclude(owner__username='camillebritor')[:6]
        random_view_list = list(chain(random_view_image,random_view_video))
        mc.set("random_view_list",random_view_list,300)

    latest_companies = mc.get("latest_companies")
    if not latest_companies:
        latest_companies = Company.objects.all().order_by('-id')[:6]
        mc.set("latest_companies",latest_companies,86400)

    featured_profs = mc.get("featured_profs")
    if not featured_profs:
        featured_profs = UserProfile.objects.select_related('user').filter(featured=True).order_by('?')[:6]
        mc.set("featured_profs",featured_profs,300)

    featured_comps = mc.get("featured_comps")
    if not featured_comps:
        featured_comps = Company.objects.filter(featured=True).order_by('?')[:4]
        mc.set("featured_comps",featured_comps,300)

    if request.user.is_authenticated():
        remaining_invitations = remaining_invitations_for_user(request.user)
        up_image_list=Image.objects.filter(owner=request.user).order_by('-added')[:3]
        up_video_list=Video.objects.filter(owner=request.user).order_by('-added')[:3]
        upstream = list(chain(up_image_list,up_video_list))
    else:
        upstream = []
        remaining_invitations = 0

    pending_roles=AuthorRole.objects.pending_roles(request.user) if request.user.is_authenticated() else None
    extra = {
        #'news_list':  Article.objects.get_published().order_by('-pub_date')[:3],
        'new_media_list': new_media_list, ## filter owner is not request.user
        'most_view_list': most_view_list,
        'random_view_list': random_view_list,
        'featured_profs':featured_profs,
        'featured_comps':featured_comps,
        'upstream':   upstream,
        'remaining_invitations': remaining_invitations,
        'latest_companies':latest_companies,
        'pending_roles':pending_roles,
        }
    if request.GET:
        activated = request.GET.get('account','')
        extra.update({'account':activated})
    return direct_to_template(request, template='index.html',
                              extra_context=extra)

def view_port_for(request,username):
    if User.objects.filter(username=username,is_active=True):
        return view_portfolio(request,username=username)
    try:
        company = Company.objects.get(slug=username)
        return object_detail(request, queryset=Company.objects.all(), object_id=company.pk, template_object_name='company',template_name = 'directory/company_detail.html')
    except:
        pass
    raise Http404(('Specified user does not exist.'))

# class SearchView(HaystackSearchView):
#     def extra_context(self):
#         extra = super(SearchView, self).extra_context()
#         extra.update({
#             'advanced': not self.query,
#             'title': 'Search' if not self.query else \
#             'Search results for: %s' % self.request.GET['q'].replace('+', ''),
#             'search_query': self.request.GET.get('q','').replace('+', ''),
#         })
#         return extra
